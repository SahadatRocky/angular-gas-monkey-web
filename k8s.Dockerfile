FROM nginx:alpine
RUN rm -rf /usr/share/nginx/html/* && rm -rf /etc/nginx/nginx.conf
COPY ./deploy/gas-monkey.conf /etc/nginx/nginx.conf
COPY ./dist/gas-monkey-web /usr/share/nginx/html
# FROM nginx
# COPY ./dist/gas-monkey-web /usr/share/nginx/html
# COPY ./deploy/gas-monkey.conf /etc/nginx/conf.d