import { DatePipe } from "@angular/common";
import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { UtilService } from "../../shared/service/util.service";
import { StoreService } from "../../store-management/service/store.service";
import { ConfigService } from "../service/config.service";

@Component({
  selector: "app-basic-setup",
  templateUrl: "./basic-setup.component.html",
  styleUrls: ["./basic-setup.component.scss"],
})
export class BasicSetupComponent implements OnInit {
  configForm: FormGroup;
  isShowLoading = false;
  chargeList = [];
  payment: any = [
    { value: "v1", viewValue: "Cash On Delivery" },
    { value: "v2", viewValue: "Online Banking" },
    { value: "v3", viewValue: "Both" },
  ];

  time1: any = [
    { value: "v1", viewValue: "11.00 am" },
    { value: "v2", viewValue: "11.00 pm" },
    { value: "v3", viewValue: "12.00 pm" },
  ];

  time2: any = [
    { value: "v1", viewValue: "11.00 am" },
    { value: "v2", viewValue: "11.00 pm" },
    { value: "v3", viewValue: "12.00 pm" },
  ];

  constructor(
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router,
    private datePipe: DatePipe,
    private configService: ConfigService,
    private storeService: StoreService,
    private utilService: UtilService
  ) {}

  ngOnInit(): void {
    this.createFormGroup();
    this.getConfigData();
  }
  createFormGroup() {
    this.configForm = this.fb.group({
      vatRate: [0, [Validators.required]],
      serviceCharge: [0, [Validators.required]],
      regularServiceCharge: [0, [Validators.required]],
      expressServiceCharge: [0, [Validators.required]],
      partnerCommission: [0, [Validators.required]],
      liftingCharge: [0, [Validators.required]],
      enableTimeSlot: [false],
      openTime: ["", [Validators.required]],
      closeTime: ["", [Validators.required]],
      timeSlot: [0, [Validators.required]],
      rewardPoint: [0, [Validators.required]],
      floorNoCharge: [0, [Validators.required]],
    });
  }

  getConfigData() {
    this.configService.getConfigData().subscribe((res) => {
      // console.log('**',res);
      this.populateData(res);
    });
  }

  populateData(myData) {
    this.chargeList = myData.charges;
    console.log(
      "-------------------charge list--------------",
      this.chargeList
    );
    this.chargeList.forEach((element) => {
      if (element.serviceName == "REG_SERVICE_CHARGE") {
        this.configForm
          .get("regularServiceCharge")
          .setValue(element.serviceValue);
      } else if (element.serviceName == "SERVICE_CHARGE") {
        this.configForm.get("serviceCharge").setValue(element.serviceValue);
      } else if (element.serviceName == "PARTNER_COMMISSION") {
        this.configForm.get("partnerCommission").setValue(element.serviceValue);
      } else if (element.serviceName == "VAT_RATE") {
        this.configForm.get("vatRate").setValue(element.serviceValue);
      } else if (element.serviceName == "EXPRESS_SERVICE_CHARGE") {
        this.configForm
          .get("expressServiceCharge")
          .setValue(element.serviceValue);
      } else if (element.serviceName == "LIFTING_CHARGE") {
        this.configForm.get("liftingCharge").setValue(element.serviceValue);
      } else if (element.serviceName == "REWARD_POINT") {
        this.configForm.get("rewardPoint").setValue(element.serviceValue);
      } else if (element.serviceName == "FLOOR_NO_CHARGE") {
        this.configForm.get("floorNoCharge").setValue(element.serviceValue);
      }
    });

    this.configForm.get("enableTimeSlot").setValue(myData.scheduleBean.enabledTimeSlot);
    this.configForm.get("timeSlot").setValue(myData.scheduleBean.slotDurationInMin);
    this.configForm.get("openTime").setValue(this.getTimeData(myData.scheduleBean.startTime));
    this.configForm.get("closeTime").setValue(this.getTimeData(myData.scheduleBean.endTime));
  }

  checkDisable() {
    return !this.configForm.valid;
  }
  getTimeData(time: any) {
    let temp = time.split(":");
    //console.log(time)
    let date = new Date();
    date.setHours(temp[0]);
    date.setMinutes(temp[1]);
    return date;
  }

  onValueChange(event) {
    console.log(event);
  }

  onSubmit() {
    console.log("on-sumbit---");
    this.chargeList.forEach((element) => {
      // console.log(element.serviceName);
      if (element.serviceName == "REG_SERVICE_CHARGE") {
        element.serviceValue = this.configForm.value.regularServiceCharge;
      } else if (element.serviceName == "SERVICE_CHARGE") {
        element.serviceValue = this.configForm.value.serviceCharge;
      } else if (element.serviceName == "PARTNER_COMMISSION") {
        element.serviceValue = this.configForm.value.partnerCommission;
      } else if (element.serviceName == "VAT_RATE") {
        //console.log("vat=====",element.serviceName);
        element.serviceValue = this.configForm.value.vatRate;
      } else if (element.serviceName == "EXPRESS_SERVICE_CHARGE") {
        element.serviceValue = this.configForm.value.expressServiceCharge;
      } else if (element.serviceName == "LIFTING_CHARGE") {
        element.serviceValue = this.configForm.value.liftingCharge;
      } else if (element.serviceName == "REWARD_POINT") {
        //console.log("REWARD_POINT=====",element.serviceName);
        element.serviceValue = this.configForm.value.rewardPoint;
      } else if (element.serviceName == "FLOOR_NO_CHARGE") {
        element.serviceValue = this.configForm.value.floorNoCharge;
      }
    });

    let obj = {
      charges: this.chargeList,
      scheduleBean: {
        id: "141771b8-63bd-4a0c-b46e-4433324b47be",
        startTime: this.getTimeDataForSend(this.configForm.value.openTime),
        endTime: this.getTimeDataForSend(this.configForm.value.closeTime),
        slotDurationInMin: this.configForm.value.timeSlot,
        status: true,
        enabledTimeSlot: this.configForm.value.enableTimeSlot,
      },
    };
    //console.log(obj);
    this.configService.configCreate(obj).subscribe(
      (res) => {
        console.log("-------------Config-create-data-------------", res);
        this.utilService.showSnackBarMessage(
          " Config Created Successfully",
          this.utilService.TYPE_MESSAGE.SUCCESS_TYPE
        );
      },
      (error) => {
        this.utilService.showSnackBarMessage(
          error.message,
          this.utilService.TYPE_MESSAGE.ERROR_TYPE
        );
      }
    );
  }

  getTimeDataForSend(time: any) {
    let date = new Date(time);
    let hour = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
    let minute =
      date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
    return hour + ":" + minute + ":00";
  }

  onchnageStartTime(event) {
    console.log(event);
  }

  get vatRate() {
    return this.configForm.get("vatRate");
  }
  get serviceCharge() {
    return this.configForm.get("serviceCharge");
  }
  get regularServiceCharge() {
    return this.configForm.get("regularServiceCharge");
  }
  get expressServiceCharge() {
    return this.configForm.get("expressServiceCharge");
  }
  get partnerCommission() {
    return this.configForm.get("partnerCommission");
  }
  get liftingCharge() {
    return this.configForm.get("liftingCharge");
  }
  get timeSlot() {
    return this.configForm.get("timeSlot");
  }
  get rewardPoint() {
    return this.configForm.get("rewardPoint");
  }

  get floorNoCharge() {
    return this.configForm.get("floorNoCharge");
  }

  get openTime() {
    return this.configForm.get("openTime");
  }

  get closeTime() {
    return this.configForm.get("closeTime");
  }
}
