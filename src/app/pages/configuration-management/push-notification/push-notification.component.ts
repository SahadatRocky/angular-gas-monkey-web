import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomerService } from '../../customer-management/service/customer.service';
import { UtilService } from '../../shared/service/util.service';
import { ConfigService } from '../service/config.service';

@Component({
  selector: 'app-push-notification',
  templateUrl: './push-notification.component.html',
  styleUrls: ['./push-notification.component.scss']
})
export class PushNotificationComponent implements OnInit {

  customerTypeList: any;
  templateList: any;
  pushNotificationForm: FormGroup;
  isShowLoading = false;

  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private utilService: UtilService,
    private configService: ConfigService,
    private customerService: CustomerService
  ) { }

  ngOnInit(): void {
    this.createPushNotificationFormGroup();
    this.getCustomerTypeList();
    this.getTemplateDropDownList();
  }

  createPushNotificationFormGroup(){
    this.pushNotificationForm = this.fb.group({
      subject: ['', Validators.required],
      typeId: ['', Validators.required],
      templateId: ['', Validators.required],
      message: ['', Validators.required]
    } );

  }


  getCustomerTypeList() {
    this.customerService.getCustomerTypeList().subscribe((res) => {
      this.customerTypeList = res;
    });
  }

  getTemplateDropDownList(){
    this.configService.getTemplateDropDownList().subscribe((res) => {
      this.templateList = res;
    });
  }

  onSubmit(){
    let obj = {
      "subject": this.pushNotificationForm.value.subject,
      "typeId":  this.pushNotificationForm.value.typeId,
      "templateId":this.pushNotificationForm.value.templateId,
      "message": this.pushNotificationForm.value.message
    };
    console.log(obj);

    this.configService.createPushNotification(obj).subscribe(
      (res) => {
        this.utilService.showSnackBarMessage(
          "Push Notification Sent Successfully",
          this.utilService.TYPE_MESSAGE.SUCCESS_TYPE
        );
    this.pushNotificationForm.reset({
      typeId: "",
      templateId:""
     });
      },
      (error) => {
        this.utilService.showSnackBarMessage(
          error.message,
          this.utilService.TYPE_MESSAGE.ERROR_TYPE
        );
      }
    );
  }

  onSendTypeChange(event){}

  onChangeTemplate(event){}

  checkDisable(){
    return !this.pushNotificationForm.valid;
  }

  get subject() {
    return this.pushNotificationForm.get('subject');
  }
  get typeId() {
    return this.pushNotificationForm.get('typeId');
  }
  get templateId() {
    return this.pushNotificationForm.get('templateId');
  }
  get message() {
    return this.pushNotificationForm.get('message');
  }

}
