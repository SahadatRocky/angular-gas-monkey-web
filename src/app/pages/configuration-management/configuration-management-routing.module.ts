import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { BasicSetupComponent } from "./basic-setup/basic-setup.component";
import { CancelReasonEditComponent } from "./cancel-reason/cancel-reason-edit/cancel-reason-edit.component";
import { CancelReasonListComponent } from "./cancel-reason/cancel-reason-list/cancel-reason-list.component";
import { CancelReasonComponent } from "./cancel-reason/cancel-reason.component";
import { CuponEditComponent } from "./cupon/cupon-edit/cupon-edit.component";
import { CuponListComponent } from "./cupon/cupon-list/cupon-list.component";
import { CuponComponent } from "./cupon/cupon.component";
import { DiscountOfferComponent } from "./discount-offer/discount-offer.component";
import { DocumentEditComponent } from "./document/document-edit/document-edit.component";
import { DocumentListComponent } from "./document/document-list/document-list.component";
import { DocumentComponent } from "./document/document.component";
import { IssueTypeEditComponent } from "./issue-type/issue-type-edit/issue-type-edit.component";
import { IssueTypeListComponent } from "./issue-type/issue-type-list/issue-type-list.component";
import { IssueTypeComponent } from "./issue-type/issue-type.component";
import { MobileWalletEditComponent } from "./mobile-wallet/mobile-wallet-edit/mobile-wallet-edit.component";
import { MobileWalletListComponent } from "./mobile-wallet/mobile-wallet-list/mobile-wallet-list.component";
import { MobileWalletComponent } from "./mobile-wallet/mobile-wallet.component";
import { PushNotificationComponent } from "./push-notification/push-notification.component";
import { SliderImageComponent } from "./slider-image/slider-image.component";
import { TemplateEditComponent } from "./template/template-edit/template-edit.component";
import { TemplateListComponent } from "./template/template-list/template-list.component";
import { TemplateComponent } from "./template/template.component";

const routes: Routes = [
  {
    path: "",
    data: {
      title: "config-management",
    },
    children: [
      {
        path: "",
        redirectTo: "basic-setup",
      },
      {
        path: "basic-setup",
        component: BasicSetupComponent,
        data: {
          title: "basic-setup",
        },
      },
      {
        path: "slider-image",
        component: SliderImageComponent,
        data: {
          title: "slider-image",
        },
      },
      {
        path: "coupon",
        component: CuponComponent,
        data: {
          title: "coupon-list",
        },
      },

      {
        path: "coupon-list",
        component: CuponListComponent,
        data: {
          title: "coupon-list",
        },
      },
      {
        path: "coupon/:id",
        component: CuponEditComponent,
        data: {
          title: "coupon-edit",
        },
      },

      {
        path: "push-notification",
        component: PushNotificationComponent,
        data: {
          title: "push-notification",
        },
      },
      {
        path: "discount-offer",
        component: DiscountOfferComponent,
        data: {
          title: "discount-offer",
        },
      },
      {
        path: "document",
        component: DocumentComponent,
        data: {
          title: "document-type-list",
        },
      },
      {
        path: "document-list",
        component: DocumentListComponent,
        data: {
          title: "document-list",
        },
      },
      {
        path: "document/:id",
        component: DocumentEditComponent,
        data: {
          title: "document-edit",
        },
      },
      {
        path: "mobile-wallet",
        component: MobileWalletComponent,
        data: {
          title: "mobile-wallet",
        },
      },
      {
        path: "mobile-wallet-list",
        component: MobileWalletListComponent,
        data: {
          title: "mobile-wallet-list",
        },
      },
      {
        path: "mobile-wallet/:id",
        component: MobileWalletEditComponent,
        data: {
          title: "mobile-wallet-edit",
        },
      },
      {
        path: "issue-type",
        component: IssueTypeComponent,
        data: {
          title: "issue-type",
        },
      },
      {
        path: "issue-type-list",
        component: IssueTypeListComponent,
        data: {
          title: "issue-type-list",
        },
      },
      {
        path: "issue-type/:id",
        component: IssueTypeEditComponent,
        data: {
          title: "issue-type-edit",
        },
      },
      {
        path: "cancel-reason",
        component: CancelReasonComponent,
        data: {
          title: "cancel-reason",
        },
      },
      {
        path: "cancel-reason-list",
        component: CancelReasonListComponent,
        data: {
          title: "cancel-reason-list",
        },
      },
      {
        path: "cancel-reason/:id",
        component: CancelReasonEditComponent,
        data: {
          title: "cancel-reason-edit",
        },
      },
      {
        path: "template",
        component: TemplateComponent,
        data: {
          title: "template-list",
        },
      },
      {
        path: "template-list",
        component: TemplateListComponent,
        data: {
          title: "template-list",
        },
      },
      {
        path: "template/:id",
        component: TemplateEditComponent,
        data: {
          title: "template-edit",
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConfigurationManagementRoutingModule {}
