import { Component, OnInit } from '@angular/core';
import { OBJ_LIST, OBJ_EDIT, OBJ_NEW } from '../../../constants/gas-monkey-constants';
import { EmitterService } from '../../shared/service/emitter.service';

@Component({
  selector: 'app-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.scss']
})
export class DocumentComponent implements OnInit {
  mode: string;
  selectedDocumentObj: any;


  constructor(private emitterService: EmitterService) { }

  ngOnInit(): void {
    this.mode = OBJ_LIST;
    this.listenEmittedApiObj();
  }

  listenEmittedApiObj() {
    this.emitterService.selectedDocumentObj.subscribe(data => {
      if (data) {
        if (data.actionMode === OBJ_EDIT) {
          this.mode = OBJ_EDIT;
        } else if (data.actionMode === OBJ_NEW) {
          this.mode = OBJ_NEW;
        }  else {
          this.mode = OBJ_LIST;
        }
        this.selectedDocumentObj = data;
      } else {
        this.mode = OBJ_LIST;
      }
    });
  }

}
