import { DatePipe } from "@angular/common";
import { Component, Input, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { OBJ_EDIT, OBJ_NEW } from "../../../../constants/gas-monkey-constants";
import { UtilService } from "../../../shared/service/util.service";
import { ConfigService } from "../../service/config.service";

@Component({
  selector: "app-document-edit",
  templateUrl: "./document-edit.component.html",
  styleUrls: ["./document-edit.component.scss"],
})
export class DocumentEditComponent implements OnInit {
  documentForm: FormGroup;
  isShowLoading = false;
  buttonText: string;
  actionMode: string;
  @Input() cuponObj: any;
  attachmentId: any;

  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private utilService: UtilService,
    private configService: ConfigService,
    private datePipe: DatePipe
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      this.attachmentId = params.id;
      //console.log(this.brandId);
      this.buttonText = this.attachmentId != "0" ? "UPDATE" : "SAVE";
      this.actionMode = this.attachmentId != "0" ? OBJ_EDIT : OBJ_NEW;
    });

    this.createDocumentFormGroup();

    if (this.actionMode == OBJ_EDIT) {
      //this.getCouponById();
      // console.log('-----------------------------',this.getCouponById())
      //this.userForm.controls['email'].disable();
    } else {
      this.isShowLoading = false;
      // this.userForm.controls['userId'].enable();
    }
  }

  createDocumentFormGroup() {
    this.documentForm = this.fb.group({
      attachmentName: ["", Validators.required],
    });
  }

  checkDisable() {
    return !this.documentForm.valid;
  }

  onSubmit() {
      let obj = {
        "attachmentName": this.documentForm.value.attachmentName,
        "status": true,
      };
      console.log(obj);
      this.configService.createAttachment(obj).subscribe(
        (res) => {
          this.utilService.showSnackBarMessage(
            "Attachment Created Successfully",
            this.utilService.TYPE_MESSAGE.SUCCESS_TYPE
          );
          this.router.navigate(["/pages/configuration-management/document"]);
        },
        (error) => {
          this.utilService.showSnackBarMessage(
            error.message,
            this.utilService.TYPE_MESSAGE.ERROR_TYPE
          );
        }
      );
  }

  goToList() {
    this.router.navigate(["/pages/configuration-management/document"]);
  }

  get documentName() {
    return this.documentForm.get("attachmentName");
  }
}
