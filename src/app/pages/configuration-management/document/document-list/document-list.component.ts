import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ServiceListControlWrapper } from '../../../../containers/core/master-component/master-model/service-list-control-wrapper';
import { MasterListService } from '../../../../containers/core/master-component/master-service/master-list-service';
import { UserPermissionButtonService } from '../../../role-management/user-group-permission/service/user-permission-button-service';
import { UserPermissionListService } from '../../../role-management/user-group-permission/service/user-permission-list-service';
import { UtilService } from '../../../shared/service/util.service';
import DataUtils from '../../../shared/utils/data-utils';
import { ConfigService } from '../../service/config.service';
import { DocumentButtonService } from './service/document-button.service';
import { DocumentListService } from './service/document-list.service';

@Component({
  selector: 'app-document-list',
  templateUrl: './document-list.component.html',
  styleUrls: ['./document-list.component.scss']
})
export class DocumentListComponent implements OnInit {


  data: any;
  isShowLoading = false;
  dataSource = new MatTableDataSource();
  dataSize = 10;
  serviceListControlWrapper: ServiceListControlWrapper<
  DocumentListService,
  DocumentButtonService
 >;

  constructor( private utilService: UtilService,
    private documentListService: DocumentListService,
    private documentButtonService: DocumentButtonService,
    private configService: ConfigService,
    private router: Router,
    public dialog: MatDialog) { }

  ngOnInit(): void {
    this.loadList(0,this.dataSize);
  }

  loadList(offset,limit) {
    this.configService.getDocumentListTableData(offset, limit).subscribe((res) =>{
       console.log("------------Document-list-data------------",res);
      this.data = DataUtils.flattenData(res.content);
      this.serviceListControlWrapper = new ServiceListControlWrapper<
      DocumentListService,
      DocumentButtonService
        >(
          this.documentListService,
          this.data,
          this.documentButtonService,
          res.totalElements,
           { pageSize: res.size, pageIndex: res.number }
        );
     });
  }

  onPageChange(event) {
    this.loadList(event.pageInfo.offset, event.pageInfo.limit);
  }

  execute(event: { element: any; rawData?: any; method: string; service?: MasterListService; methodKey?: string }) {
    // console.log(event);
    switch (event.method) {
      case 'add':
        this.router.navigate(['/pages/configuration-management/document/0']);
        break;
      case 'edit':
        // console.log('edit',event);
        this.router.navigate(['/pages/configuration-management/document/1']);
        break;
      case 'toggle':
        console.log('attachment',event.element.status);
        let obj ={
          "id": event.element.id,
          "attachmentName": event.element.attachmentName,
           "status": event.element.status === 'Active'? false : true,
          //  "status": event.element.status
        }
          console.log(obj);
        this.configService.updateAttachment(event.element.id,obj).subscribe(res =>{
        this.utilService.showSnackBarMessage(' Document Update Successfully',this.utilService.TYPE_MESSAGE.SUCCESS_TYPE);
        this.loadList(0,this.dataSize);
        });
        break;
    }
  }

  
}
