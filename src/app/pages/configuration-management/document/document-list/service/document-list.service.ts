import { Injectable } from '@angular/core';
import { MasterListService } from '../../../../../containers/core/master-component/master-service/master-list-service';
import { TextData } from '../../../../shared/model/common/text-data';

@Injectable({
  providedIn: 'root'
})
export class DocumentListService extends MasterListService {
  constructor() {
    super();
    this.fieldInfo = {
      colWidth: 4,
      listTitleEn: 'Document',
      listTitleBn: ' তালিকা',
      fields: [
        new TextData({
          key: 'attachmentName',
          fieldWidth: 4,
          name: 'attachmentName',
          id: 'attachmentName',
          label: 'Document Name',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 1
        }),
        new TextData({
          key: 'status',
          fieldWidth: 4,
          name: 'status',
          id: 'status',
          label: 'Status',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 2
        }),
        new TextData({
          key: 'action',
          fieldWidth: 4,
          name: 'অ্যাকশন',
          id: 'action',
          label: 'Action',
          // type: 'text',
          canShow: true,
          canSort: true,
          order: 3
        })
      ]
    };
  }
}
