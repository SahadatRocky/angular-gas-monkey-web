import { ComponentFixture, TestBed } from '@angular/core/testing';
import { IssueTypeListComponent } from './issue-type-list.component';

describe('IssueTypeListComponent', () => {
  let component: IssueTypeListComponent;
  let fixture: ComponentFixture<IssueTypeListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IssueTypeListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IssueTypeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
