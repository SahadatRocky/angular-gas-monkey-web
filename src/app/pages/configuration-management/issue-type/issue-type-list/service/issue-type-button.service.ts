import { Injectable } from '@angular/core';
import { MasterButtonField } from '../../../../../containers/core/master-component/master-model/master-button-field';
import { MasterButtonService } from '../../../../../containers/core/master-component/master-service/master-button-service';

@Injectable({
  providedIn: 'root'
})
export class IssueTypeButtonService extends MasterButtonService {
  constructor() {
    super();
    this.buttonFields = [
      new MasterButtonField({
        controlType: 'add',
      }),
      new MasterButtonField({
        controlType: 'edit',
      }),
      // new MasterButtonField({
      //   controlType: 'toggle',
      // }),
    ];
  }
}
