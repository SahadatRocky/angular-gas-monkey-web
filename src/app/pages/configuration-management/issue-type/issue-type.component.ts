import { Component, OnInit } from '@angular/core';
import { OBJ_EDIT, OBJ_LIST, OBJ_NEW } from '../../../constants/gas-monkey-constants';
import { EmitterService } from '../../shared/service/emitter.service';

@Component({
  selector: 'app-issue-type',
  templateUrl: './issue-type.component.html',
  styleUrls: ['./issue-type.component.scss']
})
export class IssueTypeComponent implements OnInit {
  mode: string;
  selectedIssueTypetObj: any;

  constructor(
    private emitterService: EmitterService
  ) { }

  ngOnInit(): void {
    this.mode = OBJ_LIST;
    this.listenEmittedApiObj();
  }
  listenEmittedApiObj() {
    this.emitterService.selectedIssueTypetObj.subscribe(data => {
      if (data) {
        if (data.actionMode === OBJ_EDIT) {
          this.mode = OBJ_EDIT;
        } else if (data.actionMode === OBJ_NEW) {
          this.mode = OBJ_NEW;
        }  else {
          this.mode = OBJ_LIST;
        }
        this.selectedIssueTypetObj = data;
      } else {
        this.mode = OBJ_LIST;
      }
    });
  }

}
