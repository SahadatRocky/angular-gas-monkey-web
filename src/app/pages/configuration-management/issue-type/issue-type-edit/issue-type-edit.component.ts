import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { OBJ_EDIT, OBJ_NEW } from '../../../../constants/gas-monkey-constants';
import { UtilService } from '../../../shared/service/util.service';
import { ConfigService } from '../../service/config.service';

@Component({
  selector: 'app-issue-type-edit',
  templateUrl: './issue-type-edit.component.html',
  styleUrls: ['./issue-type-edit.component.scss']
})
export class IssueTypeEditComponent implements OnInit {
  issueTypeForm: FormGroup;
  isShowLoading = false;
  buttonText: string;
  actionMode: string;
  @Input() selectedMobileWalletObj: any;
  issueTypeId: any;

  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private utilService: UtilService,
    private configService: ConfigService,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      this.issueTypeId = params.id;
      //console.log(this.brandId);
      this.buttonText = this.issueTypeId != "0" ? "UPDATE" : "SAVE";
      this.actionMode = this.issueTypeId != "0" ? OBJ_EDIT : OBJ_NEW;
    });

    this.createIssueTypeFormGroup();

    if (this.actionMode == OBJ_EDIT) {
      this.getIssueTypeById();
      // console.log('-----------------------------',this.getCouponById())
      //this.userForm.controls['email'].disable();
    } else {
      this.isShowLoading = false;
      // this.userForm.controls['userId'].enable();
    }
  }

  createIssueTypeFormGroup() {
    this.issueTypeForm = this.fb.group({
      title: ["", Validators.required],
      description: ["",  Validators.required],
      status:[false]
    });
  }

  checkDisable() {
    return !this.issueTypeForm.valid;
  }

  getIssueTypeById() {
    //console.log("brand-id", this.brandId);
    //this.populateData(this.ELEMENT_DATA);
    this.isShowLoading = true;
    this.configService.getIssueTypeById(this.issueTypeId).subscribe((res) => {
      console.log('issue-type get by id-',res);
      this.isShowLoading = false;
      this.populateData(res);
    });
  }

  populateData(myData) {
    this.issueTypeForm.patchValue(myData);
  }


  onSubmit() {
    if (this.actionMode == OBJ_EDIT) {
      let obj = {
        "id": this.issueTypeId,
        "title": this.issueTypeForm.value.title,
        "description":this.issueTypeForm.value.description,
        "status": this.issueTypeForm.value.status,
      };
      // console.log(obj);
      this.configService.updateIssueType(this.issueTypeId, obj).subscribe(
        (res) => {
          //console.log(res);
          this.utilService.showSnackBarMessage(
            "Issue Type Updated Successfully",
            this.utilService.TYPE_MESSAGE.SUCCESS_TYPE
          );
          this.router.navigate(["pages/configuration-management/issue-type"]);
        },
        (error) => {
          this.utilService.showSnackBarMessage(
            error.message,
            this.utilService.TYPE_MESSAGE.ERROR_TYPE
          );
        }
      );

    }else{
      let obj = {
        "title": this.issueTypeForm.value.title,
        "description":this.issueTypeForm.value.description,
        "status": true,
      };
      this.configService.createIssueType(obj).subscribe(
        (res) => {
          this.utilService.showSnackBarMessage(
            "Issue Type Created Successfully",
            this.utilService.TYPE_MESSAGE.SUCCESS_TYPE
          );
          this.router.navigate(["pages/configuration-management/issue-type"]);
        },
        (error) => {
          this.utilService.showSnackBarMessage(
            error.message,
            this.utilService.TYPE_MESSAGE.ERROR_TYPE
          );
        }
      );
    }

  }

  goToList() {
    this.router.navigate(["/pages/configuration-management/issue-type"]);
  }

  get title() {
    return this.issueTypeForm.get("title");
  }
  get description() {
    return this.issueTypeForm.get("description");
  }

}
