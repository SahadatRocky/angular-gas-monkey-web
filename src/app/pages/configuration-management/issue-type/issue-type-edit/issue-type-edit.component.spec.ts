import { ComponentFixture, TestBed } from '@angular/core/testing';
import { IssueTypeEditComponent } from './issue-type-edit.component';

describe('IssueTypeEditComponent', () => {
  let component: IssueTypeEditComponent;
  let fixture: ComponentFixture<IssueTypeEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IssueTypeEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IssueTypeEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
