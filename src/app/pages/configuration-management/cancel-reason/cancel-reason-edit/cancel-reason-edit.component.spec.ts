import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CancelReasonEditComponent } from './cancel-reason-edit.component';

describe('CancelReasonEditComponent', () => {
  let component: CancelReasonEditComponent;
  let fixture: ComponentFixture<CancelReasonEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CancelReasonEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelReasonEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
