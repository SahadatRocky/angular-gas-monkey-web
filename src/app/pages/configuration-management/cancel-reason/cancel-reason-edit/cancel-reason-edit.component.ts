import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { OBJ_EDIT, OBJ_NEW } from '../../../../constants/gas-monkey-constants';
import { UtilService } from '../../../shared/service/util.service';
import { ConfigService } from '../../service/config.service';

@Component({
  selector: 'app-cancel-reason-edit',
  templateUrl: './cancel-reason-edit.component.html',
  styleUrls: ['./cancel-reason-edit.component.scss']
})
export class CancelReasonEditComponent implements OnInit {
  cancelReasonForm: FormGroup;
  isShowLoading = false;
  buttonText: string;
  actionMode: string;
  @Input() selectedMobileWalletObj: any;
  cancelReasonId: any;

  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private utilService: UtilService,
    private configService: ConfigService,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      this.cancelReasonId = params.id;
      //console.log(this.brandId);
      this.buttonText = this.cancelReasonId != "0" ? "UPDATE" : "SAVE";
      this.actionMode = this.cancelReasonId != "0" ? OBJ_EDIT : OBJ_NEW;
    });

    this.createCancelReasonFormGroup();

    if (this.actionMode == OBJ_EDIT) {
      this.getCancelReasonById();
      // console.log('-----------------------------',this.getCouponById())
      //this.userForm.controls['email'].disable();
    } else {
      this.isShowLoading = false;
      // this.userForm.controls['userId'].enable();
    }
  }

  createCancelReasonFormGroup() {
    this.cancelReasonForm = this.fb.group({
      title: ["", Validators.required],
      status:[false]
    });
  }

  checkDisable() {
    return !this.cancelReasonForm.valid;
  }

  getCancelReasonById() {
    //console.log("brand-id", this.brandId);
    //this.populateData(this.ELEMENT_DATA);
    this.isShowLoading = true;
    this.configService.getCancelReasonById(this.cancelReasonId).subscribe((res) => {
      console.log('cancel Reason get by id-',res);
      this.isShowLoading = false;
      this.populateData(res);
    });
  }

  populateData(myData) {
    this.cancelReasonForm.patchValue(myData);
  }

  onSubmit() {
      if (this.actionMode == OBJ_EDIT) {
        let obj = {
          "id": this.cancelReasonId,
          "title": this.cancelReasonForm.value.title,
          "status": this.cancelReasonForm.value.status
        };
        // console.log(obj);
        this.configService.updateCancelReason(this.cancelReasonId, obj).subscribe(
          (res) => {
            //console.log(res);
            this.utilService.showSnackBarMessage(
              "Issue Type Updated Successfully",
              this.utilService.TYPE_MESSAGE.SUCCESS_TYPE
            );
            this.router.navigate(["pages/configuration-management/cancel-reason"]);
          },
          (error) => {
            this.utilService.showSnackBarMessage(
              error.message,
              this.utilService.TYPE_MESSAGE.ERROR_TYPE
            );
          }
        );
      }else{
        let obj = {
          "title": this.cancelReasonForm.value.title,
          "status": true
        };
        this.configService.createCancelReason(obj).subscribe(
          (res) => {
            this.utilService.showSnackBarMessage(
              "Issue Type Created Successfully",
              this.utilService.TYPE_MESSAGE.SUCCESS_TYPE
            );
            this.router.navigate(["pages/configuration-management/cancel-reason"]);
          },
          (error) => {
            this.utilService.showSnackBarMessage(
              error.message,
              this.utilService.TYPE_MESSAGE.ERROR_TYPE
            );
          }
        );
      }
  }

  goToList() {
    this.router.navigate(["/pages/configuration-management/cancel-reason"]);
  }

  get title() {
    return this.cancelReasonForm.get("title");
  }

}
