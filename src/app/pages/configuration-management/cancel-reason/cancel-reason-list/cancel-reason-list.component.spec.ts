import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CancelReasonListComponent } from './cancel-reason-list.component';

describe('CancelReasonListComponent', () => {
  let component: CancelReasonListComponent;
  let fixture: ComponentFixture<CancelReasonListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CancelReasonListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelReasonListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
