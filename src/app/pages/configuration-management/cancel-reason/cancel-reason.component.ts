import { Component, OnInit } from '@angular/core';
import { OBJ_EDIT, OBJ_LIST, OBJ_NEW } from '../../../constants/gas-monkey-constants';
import { EmitterService } from '../../shared/service/emitter.service';

@Component({
  selector: 'app-cancel-reason',
  templateUrl: './cancel-reason.component.html',
  styleUrls: ['./cancel-reason.component.scss']
})
export class CancelReasonComponent implements OnInit {
  mode: string;
  selectedCancelReasonObj: any;

  constructor(
    private emitterService: EmitterService
  ) { }

  ngOnInit(): void {
    this.mode = OBJ_LIST;
    this.listenEmittedApiObj();
  }
  listenEmittedApiObj() {
    this.emitterService.selectedCancelReasonObj.subscribe(data => {
      if (data) {
        if (data.actionMode === OBJ_EDIT) {
          this.mode = OBJ_EDIT;
        } else if (data.actionMode === OBJ_NEW) {
          this.mode = OBJ_NEW;
        }  else {
          this.mode = OBJ_LIST;
        }
        this.selectedCancelReasonObj = data;
      } else {
        this.mode = OBJ_LIST;
      }
    });
  }

}
