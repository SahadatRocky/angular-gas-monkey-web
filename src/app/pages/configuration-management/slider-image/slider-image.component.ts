import { Component, EventEmitter, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FileUploader } from 'ng2-file-upload';
import { ProductService } from '../../product-management/service/product.service';
import { DialogImageViewerComponent } from '../../shared/components/dialog-image-viewer/dialog-image-viewer.component';
import { UtilService } from '../../shared/service/util.service';
import { ConfigService } from '../service/config.service';

@Component({
  selector: 'app-slider-image',
  templateUrl: './slider-image.component.html',
  styleUrls: ['./slider-image.component.scss']
})
export class SliderImageComponent implements OnInit {

  sliderImageList= [];
  isPrint = true;
  constructor(private configService: ConfigService,
    private productService:ProductService,
    private utilService: UtilService,
    public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getSliderImageListData();
  }

  getSliderImageListData(){
    this.configService.getSliderImageList().subscribe(res=>{
        this.sliderImageList = res.content;
    });
  }

  getSliderImageUpload(id:any , obj:any){
    this.configService.updateSliderImage(id,obj).subscribe(res =>{
      this.getSliderImageListData();
      this.utilService.showSnackBarMessage(' Slider Image Update Successfully',this.utilService.TYPE_MESSAGE.SUCCESS_TYPE);
    });
  }



  onchnage(item:any,checked:any){
    let obj = {
      imageLink: item.imageLink,
      imageLinkId: "",
      status: checked,
      title: item.title,
    };
    this.getSliderImageUpload(item.id, obj);
  }

  uploader: FileUploader = new FileUploader({
    //url: URL,
    disableMultipart: false,
    // autoUpload: true,
    method: "post",
    itemAlias: "attachment",
    allowedFileType: ["image", "video"],
  });

  onFileSelected(event: EventEmitter<File>) {
    const file: any = event;
    const title = file[0].name;
    const formData = new FormData();
    formData.append("file", file[0]);
    this.productService.uploadProductImage(formData).subscribe((res) => {
      if (res.status == 200) {
        let obj = {
          imageLink: res.body,
          imageLinkId: "",
          status: true,
          title: title,
        };
        console.log("=======================image-linsk==========", obj);
        this.configService.createSliderImage(obj).subscribe((res) => {
          console.log("slider image created", res);
          this.getSliderImageListData();
        });
      }
    });
  }

  onSliderImageDelete(item:any){
    this.configService.deleteSliderImage(item.id).subscribe(res=>{
      if(res){
        this.getSliderImageListData();
        this.utilService.showSnackBarMessage(' Slider Image Delete Successfully',this.utilService.TYPE_MESSAGE.SUCCESS_TYPE);
      }

    });
}

openImageDialog(event){
  console.log(event.srcElement.src);
  const dialogRef = this.dialog.open(DialogImageViewerComponent, {

    // disableClose: true,
    width: '550px',
    height: '550px',
    data: {
      title: 'Image',
      obj: event.srcElement.src
    },
  });

  dialogRef.afterClosed().subscribe(result => {
    console.log('image closed');
  });

}

}
