import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MobileWalletEditComponent } from './mobile-wallet-edit.component';

describe('MobileWalletEditComponent', () => {
  let component: MobileWalletEditComponent;
  let fixture: ComponentFixture<MobileWalletEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MobileWalletEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileWalletEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
