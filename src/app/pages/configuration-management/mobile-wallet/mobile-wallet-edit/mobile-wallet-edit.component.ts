import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { OBJ_EDIT, OBJ_NEW } from '../../../../constants/gas-monkey-constants';
import { UtilService } from '../../../shared/service/util.service';
import { ConfigService } from '../../service/config.service';

@Component({
  selector: 'app-mobile-wallet-edit',
  templateUrl: './mobile-wallet-edit.component.html',
  styleUrls: ['./mobile-wallet-edit.component.scss']
})
export class MobileWalletEditComponent implements OnInit {
  mobileWalletForm: FormGroup;
  isShowLoading = false;
  buttonText: string;
  actionMode: string;
  @Input() selectedMobileWalletObj: any;
  mobileWalletId: any;

  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private utilService: UtilService,
    private configService: ConfigService,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      this.mobileWalletId = params.id;
      //console.log(this.brandId);
      this.buttonText = this.mobileWalletId != "0" ? "UPDATE" : "SAVE";
      this.actionMode = this.mobileWalletId != "0" ? OBJ_EDIT : OBJ_NEW;
    });

    this.createMobileWalletFormGroup();

    if (this.actionMode == OBJ_EDIT) {
      this.getIMobileWalletById();
      // console.log('-----------------------------',this.getCouponById())
      //this.userForm.controls['email'].disable();
    } else {
      this.isShowLoading = false;
      // this.userForm.controls['userId'].enable();
    }
  }

  createMobileWalletFormGroup() {
    this.mobileWalletForm = this.fb.group({
      name: ["", Validators.required],
      status:[false]
    });
  }

  checkDisable() {
    return !this.mobileWalletForm.valid;
  }

  getIMobileWalletById() {
    //console.log("brand-id", this.brandId);
    //this.populateData(this.ELEMENT_DATA);
    this.isShowLoading = true;
    this.configService.getIMobileWalletById(this.mobileWalletId).subscribe((res) => {
      console.log('mobile-wallet  get by id-',res);
      this.isShowLoading = false;
      this.populateData(res);
    });
  }

  populateData(myData) {
    this.mobileWalletForm.patchValue(myData);
  }


  onSubmit() {

    if (this.actionMode == OBJ_EDIT) {
      let obj = {
        "id": this.mobileWalletId,
        "name": this.mobileWalletForm.value.name,
        "status": this.mobileWalletForm.value.status,
      };
       console.log(obj);
      this.configService.updateMobileWallet(this.mobileWalletId, obj).subscribe(
        (res) => {
          //console.log(res);
          this.utilService.showSnackBarMessage(
            "Mobile Wallet Updated Successfully",
            this.utilService.TYPE_MESSAGE.SUCCESS_TYPE
          );
          this.router.navigate(["pages/configuration-management/mobile-wallet"]);
        },
        (error) => {
          this.utilService.showSnackBarMessage(
            error.message,
            this.utilService.TYPE_MESSAGE.ERROR_TYPE
          );
        }
      );

    }else{
      let obj = {
        "name": this.mobileWalletForm.value.name,
        "status": true,
      };
      console.log(obj);
      this.configService.createMobileWallet(obj).subscribe(
        (res) => {
          this.utilService.showSnackBarMessage(
            "Mobile Wallet Created Successfully",
            this.utilService.TYPE_MESSAGE.SUCCESS_TYPE
          );
          this.router.navigate(["pages/configuration-management/mobile-wallet"]);
        },
        (error) => {
          this.utilService.showSnackBarMessage(
            error.message,
            this.utilService.TYPE_MESSAGE.ERROR_TYPE
          );
        }
      );

    }


      // let obj = {
      //   "name": this.mobileWalletForm.value.name,
      //   "status": true,
      // };
      // console.log(obj);
      // this.configService.createMobileWallet(obj).subscribe(
      //   (res) => {
      //     this.utilService.showSnackBarMessage(
      //       "Mobile Wallet Created Successfully",
      //       this.utilService.TYPE_MESSAGE.SUCCESS_TYPE
      //     );
      //     this.router.navigate(["pages/configuration-management/mobile-wallet"]);
      //   },
      //   (error) => {
      //     this.utilService.showSnackBarMessage(
      //       error.message,
      //       this.utilService.TYPE_MESSAGE.ERROR_TYPE
      //     );
      //   }
      // );
  }

  goToList() {
    this.router.navigate(["/pages/configuration-management/mobile-wallet"]);
  }

  get name() {
    return this.mobileWalletForm.get("name");
  }

}
