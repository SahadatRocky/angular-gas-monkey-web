import { Component, OnInit } from '@angular/core';
import { OBJ_EDIT, OBJ_LIST, OBJ_NEW } from '../../../constants/gas-monkey-constants';
import { EmitterService } from '../../shared/service/emitter.service';

@Component({
  selector: 'app-mobile-wallet',
  templateUrl: './mobile-wallet.component.html',
  styleUrls: ['./mobile-wallet.component.scss']
})
export class MobileWalletComponent implements OnInit {
  mode: string;
  selectedMobileWalletObj: any;

  constructor(
    private emitterService: EmitterService
  ) { }

  ngOnInit(): void {
    this.mode = OBJ_LIST;
    this.listenEmittedApiObj();
  }
  listenEmittedApiObj() {
    this.emitterService.selectedMobileWalletObj.subscribe(data => {
      if (data) {
        if (data.actionMode === OBJ_EDIT) {
          this.mode = OBJ_EDIT;
        } else if (data.actionMode === OBJ_NEW) {
          this.mode = OBJ_NEW;
        }  else {
          this.mode = OBJ_LIST;
        }
        this.selectedMobileWalletObj = data;
      } else {
        this.mode = OBJ_LIST;
      }
    });
  }

}
