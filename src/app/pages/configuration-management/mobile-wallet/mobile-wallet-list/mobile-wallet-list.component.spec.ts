import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MobileWalletListComponent } from './mobile-wallet-list.component';

describe('MobileWalletListComponent', () => {
  let component: MobileWalletListComponent;
  let fixture: ComponentFixture<MobileWalletListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MobileWalletListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileWalletListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
