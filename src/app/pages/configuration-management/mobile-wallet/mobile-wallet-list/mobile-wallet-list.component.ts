import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ServiceListControlWrapper } from '../../../../containers/core/master-component/master-model/service-list-control-wrapper';
import { MasterListService } from '../../../../containers/core/master-component/master-service/master-list-service';
import { UtilService } from '../../../shared/service/util.service';
import DataUtils from '../../../shared/utils/data-utils';
import { ConfigService } from '../../service/config.service';
import { MobileWalletButtonService } from './service/mobile-wallet-button.service';
import { MobileWalletListService } from './service/mobile-wallet-list.service';


@Component({
  selector: 'app-mobile-wallet-list',
  templateUrl: './mobile-wallet-list.component.html',
  styleUrls: ['./mobile-wallet-list.component.scss']
})
export class MobileWalletListComponent implements OnInit {

  // ELEMENT_DATA: any = [
  //   { mobileBankingName: 'Bikash', status: 'true'}
  // ];

  chkStatus:boolean;
  data: any;
  isShowLoading = false;
  dataSource = new MatTableDataSource();
  dataSize = 10;
  serviceListControlWrapper: ServiceListControlWrapper<
  MobileWalletListService,
  MobileWalletButtonService
 >;

  constructor(
    private utilService: UtilService,
    private mobileWalletListService: MobileWalletListService,
    private mobileWalletButtonService: MobileWalletButtonService,
    private configService: ConfigService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.loadList(0,this.dataSize);
  }

  loadList(offset,limit) {
    this.configService.getMobileWalletListTableData(offset, limit).subscribe((res) =>{
      // console.log("------------attachment-list-data------------",res);
      this.data = DataUtils.flattenData(res.content);
      this.serviceListControlWrapper = new ServiceListControlWrapper<
      MobileWalletListService,
      MobileWalletButtonService
        >(
          this.mobileWalletListService,
          this.data,
          this.mobileWalletButtonService,
          res.totalElements,
          { pageSize: res.size, pageIndex: res.number }
        );
     });
  }

  onPageChange(event) {
    this.loadList(event.pageInfo.offset, event.pageInfo.limit);
  }

  execute(event: { element: any; rawData?: any; method: string; service?: MasterListService; methodKey?: string }) {
    // console.log(event);
    switch (event.method) {
      case 'add':
        this.router.navigate(['/pages/configuration-management/mobile-wallet/0']);
        break;
      case 'edit':
        // console.log('edit',event);
        this.router.navigate(['/pages/configuration-management/mobile-wallet/'+ event.element.id]);
        break;
      // case 'toggle':
      //   console.log('attachment',event.element.status);
      //   let obj ={
      //     "id": event.element.id,
      //     "name": event.element.name,
      //     "status": event.element.status === 'Active'? false : true,
      //   }
      //    console.log(obj);
      //   this.configService.updateMobileWallet(event.element.id,obj).subscribe(res =>{
      //   this.utilService.showSnackBarMessage(' Mobile Wallet Update Successfully',this.utilService.TYPE_MESSAGE.SUCCESS_TYPE);
      //    this.loadList(0,this.dataSize);
      //   });

      //   break;
        // case 'delete':
        //   console.log(event.element.id);
        //   this.configService.deleteMobileWallet(event.element.id).subscribe(res=>{
        //     if(res){
        //       this.loadList(0,this.dataSize);
        //       this.utilService.showSnackBarMessage('Mobile Wallet Delete Successfully',this.utilService.TYPE_MESSAGE.SUCCESS_TYPE);
        //     }

        //   });

        // break;
    }
  }

}
