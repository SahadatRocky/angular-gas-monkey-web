import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/internal/operators/map';
import { ApiEndpoints } from '../../../api-endpoints';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  HTTPOptions:Object = {
    observe: 'response',
    responseType: 'text'
 }

 private apiEndpoints: ApiEndpoints = new ApiEndpoints();

  constructor(private http: HttpClient) { }

  configCreate(Obj: any): Observable<any>{
    let url = this.apiEndpoints.CONFIG_DATA_URL;
    return this.http.post<any>(url, Obj).pipe(map(value => value))
  }

  getConfigData(): Observable<any> {
    let url = this.apiEndpoints.CONFIG_DATA_URL;
    return this.http.get<any>(url).pipe(
      map(value => value)
      );
  }

  ///slider-image
  getSliderImageList(): Observable<any> {
    let url = this.apiEndpoints.SLIDER_IMAGE_LIST_URL;
    return this.http.get<any>(url).pipe(
      map(value => value)
      );
  }

  createSliderImage(obj:any): Observable<any> {
    let url = this.apiEndpoints.SLIDER_IMAGE_LIST_URL;
    return this.http.post<any>(url,obj).pipe(
      map(value => value)
      );
  }

  updateSliderImage(id:any, obj:any): Observable<any> {
    let url = this.apiEndpoints.SLIDER_IMAGE_LIST_URL+ '/' + id;
    return this.http.put<any>(url,obj).pipe(
      map(value => value)
      );
  }

  deleteSliderImage(id:any): Observable<any> {
    let url = this.apiEndpoints.SLIDER_IMAGE_LIST_URL+ '/' + id;
    return this.http.delete<any>(url,this.HTTPOptions).pipe(
      map(value => value)
      );
  }

  ////coupon
  getCouponList(page: number, size: number): Observable<any> {
    let url = this.apiEndpoints.COUPON_LIST_URL;
    const fullUrl = `${url}?page=${page}&size=${size}`;
    return this.http.get<any>(fullUrl).pipe(
      map(value => value)
      );
  }

  getCouponById(id:any): Observable<any> {
    let url = this.apiEndpoints.COUPON_LIST_URL + '/' + id;
    return this.http.get<any>(url).pipe(
      map(value => value)
      );
  }

  createCoupon(obj:any): Observable<any> {
    let url = this.apiEndpoints.COUPON_LIST_URL;
    return this.http.post<any>(url,obj).pipe(
      map(value => value)
      );
  }

  updateCoupon(id:any, obj:any): Observable<any> {
    let url = this.apiEndpoints.COUPON_LIST_URL+ '/' + id;
    return this.http.put<any>(url,obj).pipe(
      map(value => value)
      );
  }

  deleteCoupon(id:any): Observable<any> {
    let url = this.apiEndpoints.COUPON_LIST_URL+ '/' + id;
    return this.http.delete<any>(url,this.HTTPOptions).pipe(
      map(value => value)
      );
  }

  ///discount-offer
  discountDupdate(id:any,Obj: any): Observable<any>{
    let url = this.apiEndpoints.DISCOUNT_DATA_URL + '/' + id;
    return this.http.put<any>(url, Obj).pipe(map(value => value))
  }

  getDiscountOfferData(): Observable<any> {
    let url = this.apiEndpoints.DISCOUNT_DATA_URL;
    return this.http.get<any>(url).pipe(
      map(value => value)
      );
  }

  /// notification

  createPushNotification(obj:any): Observable<any> {
    let url = this.apiEndpoints.PUSH_NOTIFICATION_URL;
    return this.http.post<any>(url,obj).pipe(
      map(value => value)
      );
  }

  // Attachment

  getDocumentListTableData(page: number, size: number): Observable<any> {
    let url = this.apiEndpoints.ATTACHMENT_URL;
    const fullUrl = `${url}?page=${page}&size=${size}`;
    return this.http.get<any>(fullUrl).pipe(
      map(value => value)
      );
  }

  createAttachment(obj:any): Observable<any> {
    let url = this.apiEndpoints.ATTACHMENT_URL;
    return this.http.post<any>(url,obj).pipe(
      map(value => value)
      );
  }

  updateAttachment(id:any, obj:any): Observable<any> {
    let url = this.apiEndpoints.ATTACHMENT_URL+ '/' + id;
    return this.http.put<any>(url,obj).pipe(
      map(value => value)
      );
  }

   // Mobile wallet

   createMobileWallet(obj:any): Observable<any> {
    let url = this.apiEndpoints.MOBILE_BANKING_URL;
    return this.http.post<any>(url,obj).pipe(
      map(value => value)
      );
  }

  updateMobileWallet(id:any, obj:any): Observable<any> {
    let url = this.apiEndpoints.MOBILE_BANKING_URL+ '/' + id;
    return this.http.put<any>(url,obj).pipe(
      map(value => value)
      );
  }

   getMobileWalletListTableData(page: number, size: number): Observable<any> {
    let url = this.apiEndpoints.MOBILE_BANKING_URL;
    const fullUrl = `${url}?page=${page}&size=${size}`;
    return this.http.get<any>(fullUrl).pipe(
      map(value => value)
      );
  }

  getIMobileWalletById(id:any): Observable<any>{
    let url = this.apiEndpoints.MOBILE_BANKING_URL + '/' + id;
    return this.http.get<any>(url).pipe(
      map(value => value)
      );
  }

  deleteMobileWallet(id:any): Observable<any> {
    let url = this.apiEndpoints.MOBILE_BANKING_URL+ '/' + id;
    return this.http.delete<any>(url).pipe(
      map(value => value)
      );
  }



  // issue type
  getIssueTypeListTableData(page: number, size: number): Observable<any> {
    let url = this.apiEndpoints.ISSUE_TYPE_URL;
    const fullUrl = `${url}?page=${page}&size=${size}`;
    return this.http.get<any>(fullUrl).pipe(
      map(value => value)
      );
  }

  createIssueType(obj:any): Observable<any> {
    let url = this.apiEndpoints.ISSUE_TYPE_URL;
    return this.http.post<any>(url,obj).pipe(
      map(value => value)
      );
  }

  getIssueTypeById(id:any): Observable<any>{
    let url = this.apiEndpoints.ISSUE_TYPE_URL + '/' + id;
    return this.http.get<any>(url).pipe(
      map(value => value)
      );
  }

  updateIssueType(id:any, obj:any): Observable<any>{
    let url = this.apiEndpoints.ISSUE_TYPE_URL + '/' + id;
    return this.http.put<any>(url,obj).pipe(
      map(value => value)
    );
  }

  //cancel-reason
  getCancelReasonListTableData(page: number, size: number): Observable<any> {
    let url = this.apiEndpoints.CANCEL_REASON_URL;
    const fullUrl = `${url}?page=${page}&size=${size}`;
    return this.http.get<any>(fullUrl).pipe(
      map(value => value)
      );
  }

  createCancelReason(obj:any): Observable<any> {
    let url = this.apiEndpoints.CANCEL_REASON_URL;
    return this.http.post<any>(url,obj).pipe(
      map(value => value)
      );
  }

  getCancelReasonById(id:any): Observable<any>{
    let url = this.apiEndpoints.CANCEL_REASON_URL + '/' + id;
    return this.http.get<any>(url).pipe(
      map(value => value)
      );
  }

  updateCancelReason(id:any, obj:any): Observable<any>{
    let url = this.apiEndpoints.CANCEL_REASON_URL + '/' + id;
    return this.http.put<any>(url,obj).pipe(
      map(value => value)
    );
  }



  // template type
  getTemplateListTableData(page: number, size: number): Observable<any> {
    let url = this.apiEndpoints.TEMPLATE_URL;
    const fullUrl = `${url}?page=${page}&size=${size}`;
    return this.http.get<any>(fullUrl).pipe(
      map(value => value)
      );
  }

  createTemplate(obj:any): Observable<any> {
    let url = this.apiEndpoints.TEMPLATE_URL;
    return this.http.post<any>(url,obj).pipe(
      map(value => value)
      );
  }

  getTemplateById(id:any): Observable<any>{
    let url = this.apiEndpoints.TEMPLATE_URL + '/' + id;
    return this.http.get<any>(url).pipe(
      map(value => value)
      );
  }

  updateTemlate(id:any, obj:any): Observable<any>{
    let url = this.apiEndpoints.TEMPLATE_URL + '/' + id;
    return this.http.put<any>(url,obj).pipe(
      map(value => value)
    );
  }

  getTemplateDropDownList(){
    let url = this.apiEndpoints.TEMPLATE_URL + this.apiEndpoints.DROPDOWN_LIST_API_URL;
    return this.http.get<any>(url).pipe(
      map(value => value)
    );
  }

}
