import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { SharedModule } from "../shared/shared.module";

import { BasicSetupComponent } from "./basic-setup/basic-setup.component";
import { ConfigurationManagementRoutingModule } from "./configuration-management-routing.module";
import { MatTimepickerModule } from "mat-timepicker";
import { SliderImageComponent } from "./slider-image/slider-image.component";
import { CuponComponent } from "./cupon/cupon.component";
import { CuponListComponent } from "./cupon/cupon-list/cupon-list.component";
import { CuponEditComponent } from "./cupon/cupon-edit/cupon-edit.component";
import { DiscountOfferComponent } from "./discount-offer/discount-offer.component";
import { PushNotificationComponent } from './push-notification/push-notification.component';
import { MobileWalletComponent } from "./mobile-wallet/mobile-wallet.component";
import { MobileWalletEditComponent } from "./mobile-wallet/mobile-wallet-edit/mobile-wallet-edit.component";
import { MobileWalletListComponent } from "./mobile-wallet/mobile-wallet-list/mobile-wallet-list.component";
import { DocumentEditComponent } from "./document/document-edit/document-edit.component";
import { DocumentListComponent } from "./document/document-list/document-list.component";
import { DocumentComponent } from "./document/document.component";
import { IssueTypeComponent } from "./issue-type/issue-type.component";
import { IssueTypeEditComponent } from "./issue-type/issue-type-edit/issue-type-edit.component";
import { IssueTypeListComponent } from "./issue-type/issue-type-list/issue-type-list.component";
import { CancelReasonComponent } from "./cancel-reason/cancel-reason.component";
import { CancelReasonEditComponent } from "./cancel-reason/cancel-reason-edit/cancel-reason-edit.component";
import { CancelReasonListComponent } from "./cancel-reason/cancel-reason-list/cancel-reason-list.component";
import { TemplateComponent } from './template/template.component';
import { TemplateListComponent } from './template/template-list/template-list.component';
import { TemplateEditComponent } from './template/template-edit/template-edit.component';



@NgModule({
  declarations: [
    BasicSetupComponent,
    SliderImageComponent,
    CuponComponent,
    CuponListComponent,
    CuponEditComponent,
    DiscountOfferComponent,
    PushNotificationComponent,
    DocumentComponent,
    DocumentListComponent,
    DocumentEditComponent,
    MobileWalletComponent,
    MobileWalletListComponent,
    MobileWalletEditComponent,
    IssueTypeComponent,
    IssueTypeListComponent,
    IssueTypeEditComponent,
    CancelReasonComponent,
    CancelReasonListComponent,
    CancelReasonEditComponent,
    TemplateComponent,
    TemplateListComponent,
    TemplateEditComponent,

  ],
  imports: [
    CommonModule,
    ConfigurationManagementRoutingModule,
    MatTimepickerModule,
    SharedModule,
  ],
})
export class ConfigurationManagementModule {}
