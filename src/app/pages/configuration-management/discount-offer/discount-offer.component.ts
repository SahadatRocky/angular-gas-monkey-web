import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilService } from '../../shared/service/util.service';
import { StoreService } from '../../store-management/service/store.service';
import { ConfigService } from '../service/config.service';

@Component({
  selector: 'app-discount-offer',
  templateUrl: './discount-offer.component.html',
  styleUrls: ['./discount-offer.component.scss']
})
export class DiscountOfferComponent implements OnInit {

  discountList=[
    {id:'PERCENT', value:'PERCENT'},
    {id: 'AMOUNT', value:'AMOUNT'}
  ];
  discountOfferForm: FormGroup;
  isShowLoading = false;
  chargeList = [];
  discountId: any;
  constructor( private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router,
    private datePipe: DatePipe,
    private configService : ConfigService,
    private storeService: StoreService,
    private utilService: UtilService,) { }

  ngOnInit(): void {
    this.createFormGroup();
    this.getDiscountOfferData();

  }
  createFormGroup(){
    this.discountOfferForm = this.fb.group({
      discountType: ['',  [Validators.required]],
      discountStartDate: ['', [Validators.required]],
      discountEndDate: ['', [Validators.required]],
      discountValue: ['', [Validators.required]],
      discountEnable: [false],
      status:[false]

    }
    );
  }


  getDiscountOfferData(){
    this.configService.getDiscountOfferData().subscribe(res => {
      console.log('discount-offer',res);
      this.discountId = res[0].id;
      this.populateData(res[0]);
    })
  }

  populateData(myData) {
    this.discountOfferForm.patchValue(myData);
    // this.configForm.get('reviewRating').setValue(myData.scheduleBean.reviewRating);
  }

  checkDisable(){
    return !this.discountOfferForm.valid;
  }

  onDiscountTypeChange(event){

  }

  onSubmit(){
    let obj = {
      "id": this.discountId,
      "discountValue":this.discountOfferForm.value.discountValue,
      "discountType":this.discountOfferForm.value.discountType,
      "discountEnable": this.discountOfferForm.value.discountEnable,
      "status":this.discountOfferForm.value.status,
      "discountStartDate":this.datePipe.transform(this.discountOfferForm.value.discountStartDate,'yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\''),
      "discountEndDate":this.datePipe.transform(this.discountOfferForm.value.discountEndDate,'yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\''),
    };
    console.log(obj);
    this.configService.discountDupdate(this.discountId,obj).subscribe(res=>{
      console.log("-------------Discount-create-data-------------",res);
      this.utilService.showSnackBarMessage(' Discount offer Updated Successfully',this.utilService.TYPE_MESSAGE.SUCCESS_TYPE);
  },error => {
    this.utilService.showSnackBarMessage(error.message,this.utilService.TYPE_MESSAGE.ERROR_TYPE);
  });
  }

  onchangeDiscountEnable(event){
    this.discountOfferForm.get('discountEnable').setValue(event.checked);
}

  get discountType() {
    return this.discountOfferForm.get('discountType');
  }
  get discountStartDate() {
    return this.discountOfferForm.get('discountStartDate');
  }
  get discountEndDate() {
    return this.discountOfferForm.get('discountEndDate');
  }
  get discountValue() {
    return this.discountOfferForm.get('discountValue');
  }
  get discountEnable(){
    return this.discountOfferForm.get('discountEnable');
  }

}
