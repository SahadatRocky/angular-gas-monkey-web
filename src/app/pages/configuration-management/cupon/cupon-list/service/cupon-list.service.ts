import { Injectable } from '@angular/core';
import { MasterListService } from '../../../../../containers/core/master-component/master-service/master-list-service';
import { TextData } from '../../../../shared/model/common/text-data';

@Injectable({
  providedIn: 'root'
})
export class CuponListService extends MasterListService {
  constructor() {
    super();
    this.fieldInfo = {
      colWidth: 4,
      listTitleEn: 'Coupon',
      listTitleBn: 'কুপন তালিকা',
      fields: [

        new TextData({
          key: 'couponCode',
          fieldWidth: 4,
          name: 'couponCode',
          id: 'couponCode',
          label: ' Coupon Code',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 1
        }),

        new TextData({
          key: 'couponType',
          fieldWidth: 4,
          name: 'couponType',
          id: 'couponType',
          label: ' Coupon Type',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 2
        }),


        new TextData({
          key: 'couponUsageType',
          fieldWidth: 4,
          name: 'couponUsageType',
          id: 'couponUsageType',
          label: ' Coupon Usage Type',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 3
        }),

        new TextData({
          key: 'amount',
          fieldWidth: 4,
          name: 'amount',
          id: 'amount',
          label: 'Amount',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 4
        }),

        new TextData({
          key: 'usageLimit',
          fieldWidth: 4,
          name: 'usageLimit',
          id: 'usageLimit',
          label: 'Usage Limit',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 5
        }),

        new TextData({
          key: 'couponUsed',
          fieldWidth: 4,
          name: 'couponUsed',
          id: 'couponUsed',
          label: 'Coupon Used',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 6
        }),
        new TextData({
          key: 'startDate',
          fieldWidth: 4,
          name: 'startDate',
          id: 'startDate',
          label: 'Coupon Start Date',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 7
        }),
        new TextData({
          key: 'endDate',
          fieldWidth: 4,
          name: 'endDate',
          id: 'endDate',
          label: 'Coupon End Date',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 8
        }),

        new TextData({
          key: 'status',
          fieldWidth: 4,
          name: 'status',
          id: 'status',
          label: 'Status',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 4
        }),

        new TextData({
          key: 'action',
          fieldWidth: 4,
          name: 'অ্যাকশন',
          id: 'action',
          label: 'Action',
          // type: 'text',
          canShow: true,
          canSort: true,
          order: 9
        })
      ]
    };
  }
}
