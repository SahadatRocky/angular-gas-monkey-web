import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ServiceListControlWrapper } from '../../../../containers/core/master-component/master-model/service-list-control-wrapper';
import { MasterListService } from '../../../../containers/core/master-component/master-service/master-list-service';
import DataUtils from '../../../shared/utils/data-utils';
import { ConfigService } from '../../service/config.service';
import { CuponButtonService } from './service/cupon-button.service';
import { CuponListService } from './service/cupon-list.service';

@Component({
  selector: 'app-cupon-list',
  templateUrl: './cupon-list.component.html',
  styleUrls: ['./cupon-list.component.scss']
})
export class CuponListComponent implements OnInit {

  data: any;
  isShowLoading = false;
  dataSource = new MatTableDataSource();
  dataSize = 10;
  serviceListControlWrapper: ServiceListControlWrapper<
    CuponListService,
    CuponButtonService
    >;

  constructor(
    private router: Router,
    private  cuponButtonService: CuponButtonService,
    private configService: ConfigService,
    private cuponListService: CuponListService,
  ) { }

  ngOnInit(): void {
    this.loadList(0,this.dataSize);
  }

  loadList(offset, limit) {
    this.configService.getCouponList(offset, limit).subscribe((res) =>{

      console.log('++++',res);
      this.data = DataUtils.flattenData(res.content);
      this.serviceListControlWrapper = new ServiceListControlWrapper<
      CuponListService,
      CuponButtonService
        >(
          this.cuponListService,
          this.data,
          this.cuponButtonService,
          res.totalElements,
          { pageSize: res.size, pageIndex: res.number }
        );
    });
  }

  onPageChange(event) {
    this.loadList(event.pageInfo.offset, event.pageInfo.limit);
  }

  execute(event: { element: any; rawData?: any; method: string; service?: MasterListService; methodKey?: string }) {
    switch (event.method) {
      case 'add':
        this.router.navigate(['/pages/configuration-management/coupon/0']);
        break;
      case 'edit':
        console.log('edit',event);
        this.router.navigate(['/pages/configuration-management/coupon/'+ event.element.id ]);
        break;
      case 'view':
        break;
    }
  }

}
