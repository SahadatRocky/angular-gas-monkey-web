import { DatePipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { OBJ_EDIT, OBJ_NEW } from '../../../../constants/gas-monkey-constants';
import { UtilService } from '../../../shared/service/util.service';
import { ConfigService } from '../../service/config.service';

@Component({
  selector: 'app-cupon-edit',
  templateUrl: './cupon-edit.component.html',
  styleUrls: ['./cupon-edit.component.scss']
})
export class CuponEditComponent implements OnInit {

  couponForm: FormGroup;
  isShowLoading = false;
  buttonText: string;
  actionMode: string;
  @Input() cuponObj: any;
  cuponId: any;

  couponType=[
    {id:'PERCENT',value:'PERCENT'},
    {id:'AMOUNT',value:'AMOUNT'}
  ];

  couponUsedType=[
    {id:'SERVICE',value:'SERVICE'},
    {id:'ORDER',value:'ORDER'}
  ];


  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private utilService: UtilService,
    private configService: ConfigService,
    private datePipe: DatePipe
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      this.cuponId = params.id;
      //console.log(this.brandId);
      this.buttonText = this.cuponId != "0" ? "UPDATE" : "SAVE";
      this.actionMode = this.cuponId != "0" ? OBJ_EDIT : OBJ_NEW;
    });

    this.createCouponFormGroup();

    if (this.actionMode == OBJ_EDIT) {
      this.getCouponById();
     // console.log('-----------------------------',this.getCouponById())


      //this.userForm.controls['email'].disable();
    } else {
      this.isShowLoading = false;
      // this.userForm.controls['userId'].enable();
    }
  }

  createCouponFormGroup(){
    this.couponForm = this.fb.group({
      couponCode: ['', Validators.required],
      couponType: ['',Validators.required],
      amount: ['', Validators.required],
      status: [false],
      couponUsageType: ['',Validators.required],
      startDate:['',Validators.required],
      endDate:['',Validators.required],
      couponUsed: ['',Validators.required],
      usageLimit: ['',Validators.required],

    } );

  }

  checkDisable(){
    //console.log("check------");
    if(this.actionMode == 'edit'){
      return !this.couponForm.valid;
    }else{
      return !this.couponForm.valid;
    }
  }

  getCouponById(){
    this.isShowLoading = true;
    this.configService.getCouponById(this.cuponId).subscribe(res => {
      console.log(res);
      this.populateData(res);
      this.isShowLoading = false;
    })
  }


  populateData(myData) {
   this.couponForm.patchValue(myData);
  }

  onSubmit() {
    if (this.actionMode == OBJ_EDIT) {
      let obj = {
        "id": this.cuponId,
        "couponCode": this.couponForm.value.couponCode,
        "couponType": this.couponForm.value.couponType,
        "couponUsageType": this.couponForm.value.couponUsageType,
        "status": this.couponForm.value.status,
        "amount":this.couponForm.value.amount,
        "couponUsed":this.couponForm.value.couponUsed,
        "usageLimit": this.couponForm.value.usageLimit,
        "startDate":this.datePipe.transform(this.couponForm.value.startDate,'yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\''),
        "endDate":this.datePipe.transform(this.couponForm.value.endDate,'yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\''),

      };
      console.log(obj);
      this.configService.updateCoupon(this.cuponId, obj).subscribe(
        (res) => {
          //console.log(res);
          this.utilService.showSnackBarMessage(
            "Coupon Updated Successfully",
            this.utilService.TYPE_MESSAGE.SUCCESS_TYPE
          );
          this.router.navigate(["/pages/configuration-management/coupon"]);
        },
        (error) => {
          this.utilService.showSnackBarMessage(
            error.message,
            this.utilService.TYPE_MESSAGE.ERROR_TYPE
          );
        }
      );
    } else {
      let obj = {
        "couponCode": this.couponForm.value.couponCode,
        "couponType": this.couponForm.value.couponType,
        "couponUsageType": this.couponForm.value.couponUsageType,
        "status": false,
        "amount":this.couponForm.value.amount,
        "couponUsed":this.couponForm.value.couponUsed,
        "usageLimit": this.couponForm.value.usageLimit,
        "startDate":this.datePipe.transform(this.couponForm.value.startDate,'yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\''),
        "endDate":this.datePipe.transform(this.couponForm.value.endDate,'yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\''),
      };
      console.log(obj);
      this.configService.createCoupon(obj).subscribe(
        (res) => {
          this.utilService.showSnackBarMessage(
            "Coupon Created Successfully",
            this.utilService.TYPE_MESSAGE.SUCCESS_TYPE
          );
          this.router.navigate(["/pages/configuration-management/coupon"]);
        },
        (error) => {
          this.utilService.showSnackBarMessage(
            error.message,
            this.utilService.TYPE_MESSAGE.ERROR_TYPE
          );
        }
      );
    }
  }


  goToList() {
    this.router.navigate(["/pages/configuration-management/coupon"]);
  }

  onDiscountTypeChange(event){
    console.log(event);
  }

  get couponCode() {
    return this.couponForm.get('couponCode');
  }

  get amount() {
    return this.couponForm.get('amount');
  }
  get couponUsed() {
    return this.couponForm.get('couponUsed');
  }

  get couponStartDate() {
    return this.couponForm.get('couponStartDate');
  }
  get couponEndDate() {
    return this.couponForm.get('couponEndDate');
  }
  get couponTypeId() {
    return this.couponForm.get('couponType');
  }
  get usageLimit() {
    return this.couponForm.get('usageLimit');
  }
  get startDate() {
    return this.couponForm.get('startDate');
  }
  get endDate() {
    return this.couponForm.get('endDate');
  }
  get couponUsageType() {
    return this.couponForm.get('couponUsageType');
  }



}
