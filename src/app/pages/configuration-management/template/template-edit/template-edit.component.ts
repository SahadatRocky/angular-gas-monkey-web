import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { OBJ_EDIT, OBJ_NEW } from '../../../../constants/gas-monkey-constants';
import { UtilService } from '../../../shared/service/util.service';
import { ConfigService } from '../../service/config.service';

@Component({
  selector: 'app-template-edit',
  templateUrl: './template-edit.component.html',
  styleUrls: ['./template-edit.component.scss']
})
export class TemplateEditComponent implements OnInit {
  templateForm: FormGroup;
  isShowLoading = false;
  buttonText: string;
  actionMode: string;
  @Input() selectedTemplateObj: any;
  templateId: any;


  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private utilService: UtilService,
    private configService: ConfigService,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      this.templateId = params.id;
      //console.log(this.brandId);
      this.buttonText = this.templateId != "0" ? "UPDATE" : "SAVE";
      this.actionMode = this.templateId != "0" ? OBJ_EDIT : OBJ_NEW;
    });

    this.createTemplateFormGroup();

    if (this.actionMode == OBJ_EDIT) {
      this.getTemplateById();
      // console.log('-----------------------------',this.getCouponById())
      //this.userForm.controls['email'].disable();
    } else {
      this.isShowLoading = false;
      // this.userForm.controls['userId'].enable();
    }
  }

  createTemplateFormGroup() {
    this.templateForm = this.fb.group({
      title: ["", Validators.required],
      description: ["",  Validators.required],
      appToUser: ["",  Validators.required],
      status:[false]
    });
  }

  checkDisable() {
    return !this.templateForm.valid;
  }

  getTemplateById() {
    //console.log("brand-id", this.brandId);
    //this.populateData(this.ELEMENT_DATA);
    this.isShowLoading = true;
    this.configService.getTemplateById(this.templateId).subscribe((res) => {
      console.log('template get by id-',res);
      this.isShowLoading = false;
      this.populateData(res);
    });
  }

  populateData(myData) {
    this.templateForm.patchValue(myData);
  }


  onSubmit() {
    if (this.actionMode == OBJ_EDIT) {
      let obj = {
        "id": this.templateId,
        "title": this.templateForm.value.title,
        "description":this.templateForm.value.description,
        "appToUser":this.templateForm.value.appToUser,
        "status": this.templateForm.value.status,
      };
      console.log(obj);
      this.configService.updateTemlate(this.templateId, obj).subscribe(
        (res) => {
          //console.log(res);
          this.utilService.showSnackBarMessage(
            "Template Updated Successfully",
            this.utilService.TYPE_MESSAGE.SUCCESS_TYPE
          );
          this.router.navigate(["pages/configuration-management/template"]);
        },
        (error) => {
          this.utilService.showSnackBarMessage(
            error.message,
            this.utilService.TYPE_MESSAGE.ERROR_TYPE
          );
        }
      );

    }else{
      let obj = {
        "title": this.templateForm.value.title,
        "description":this.templateForm.value.description,
        "appToUser":this.templateForm.value.appToUser,
        "status": true,
      };
      this.configService.createTemplate(obj).subscribe(
        (res) => {
          this.utilService.showSnackBarMessage(
            "Template Created Successfully",
            this.utilService.TYPE_MESSAGE.SUCCESS_TYPE
          );
          this.router.navigate(["pages/configuration-management/template"]);
        },
        (error) => {
          this.utilService.showSnackBarMessage(
            error.message,
            this.utilService.TYPE_MESSAGE.ERROR_TYPE
          );
        }
      );
    }

  }

  goToList() {
    this.router.navigate(["/pages/configuration-management/template"]);
  }

  get title() {
    return this.templateForm.get("title");
  }
  get description() {
    return this.templateForm.get("description");
  }
  get appToUser() {
    return this.templateForm.get("appToUser");
  }

}
