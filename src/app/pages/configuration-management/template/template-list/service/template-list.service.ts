import { Injectable } from '@angular/core';
import { MasterListService } from '../../../../../containers/core/master-component/master-service/master-list-service';
import { TextData } from '../../../../shared/model/common/text-data';

@Injectable({
  providedIn: 'root'
})
export class TemplateListService extends MasterListService {
  constructor() {
    super();
    this.fieldInfo = {
      colWidth: 4,
      listTitleEn: 'Template',
      listTitleBn: ' তালিকা',
      fields: [
        new TextData({
          key: 'title',
          fieldWidth: 4,
          name: 'title',
          id: 'title',
          label: 'Title',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 1
        }),
        new TextData({
          key: 'description',
          fieldWidth: 4,
          name: 'description',
          id: 'description',
          label: 'Description',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 2
        }),
        new TextData({
          key: 'appToUser',
          fieldWidth: 4,
          name: 'appToUser',
          id: 'appToUser',
          label: 'ApptoUser',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 3
        }),
        new TextData({
          key: 'status',
          fieldWidth: 4,
          name: 'status',
          id: 'status',
          label: 'Status',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 4
        }),
        new TextData({
          key: 'action',
          fieldWidth: 4,
          name: 'অ্যাকশন',
          id: 'action',
          label: 'Action',
          //type: 'text',
          canShow: true,
          canSort: true,
          order: 5
        })
      ]
    };
  }
}

