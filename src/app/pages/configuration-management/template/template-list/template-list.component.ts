import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ServiceListControlWrapper } from '../../../../containers/core/master-component/master-model/service-list-control-wrapper';
import { MasterListService } from '../../../../containers/core/master-component/master-service/master-list-service';
import { UtilService } from '../../../shared/service/util.service';
import DataUtils from '../../../shared/utils/data-utils';
import { ConfigService } from '../../service/config.service';
import { TemplateButtonService } from './service/template-button.service';
import { TemplateListService } from './service/template-list.service';

@Component({
  selector: 'app-template-list',
  templateUrl: './template-list.component.html',
  styleUrls: ['./template-list.component.scss']
})
export class TemplateListComponent implements OnInit {
  chkStatus:boolean;
  data: any;
  isShowLoading = false;
  dataSource = new MatTableDataSource();
  dataSize = 10;
  serviceListControlWrapper: ServiceListControlWrapper<
  TemplateListService,
  TemplateButtonService
 >;


  constructor(
    private utilService: UtilService,
    private templateListService: TemplateListService,
    private templateButtonService: TemplateButtonService,
    private configService: ConfigService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.loadList(0,this.dataSize);
  }

  loadList(offset,limit) {
    this.configService.getTemplateListTableData(offset, limit).subscribe((res) =>{
      console.log("------------isueType-list-data------------",res);
      this.data = DataUtils.flattenData(res.content);
      this.serviceListControlWrapper = new ServiceListControlWrapper<
      TemplateListService,
      TemplateButtonService
        >(
          this.templateListService,
          this.data,
          this.templateButtonService,
          res.totalElements,
          { pageSize: res.size, pageIndex: res.number }
        );
     });
  }

  onPageChange(event) {
    this.loadList(event.pageInfo.offset, event.pageInfo.limit);
  }

  execute(event: { element: any; rawData?: any; method: string; service?: MasterListService; methodKey?: string }) {
    // console.log(event);
    switch (event.method) {
      case 'add':
        this.router.navigate(['/pages/configuration-management/template/0']);
        break;
      case 'edit':
        // console.log('issue-type-edit',event);
        this.router.navigate(['/pages/configuration-management/template/'+ event.element.id]);
        //+ event.element.id
        break;
      case 'toggle':
        // console.log('attachment',event.element.status);
        // let obj ={
        //   "id": event.element.id,
        //   "name": event.element.name,
        //   "status": event.element.status === 'Active'? false : true,
        // }
        //  console.log(obj);
        // this.configService.updateMobileWallet(event.element.id,obj).subscribe(res =>{
        // this.utilService.showSnackBarMessage(' Mobile Wallet Update Successfully',this.utilService.TYPE_MESSAGE.SUCCESS_TYPE);
        //  this.loadList(0,this.dataSize);
        // });

        break;
    }
  }


}
