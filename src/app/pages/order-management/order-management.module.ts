import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderManagementRoutingModule } from './order-management-routing.module';
import { OrderComponent } from './order/order.component';
import { OrderListComponent } from './order-list/order-list.component';
import { OrderEditComponent } from './order-edit/order-edit.component';
import { SharedModule } from '../shared/shared.module';
import { OrderViewComponent } from './order-view/order-view.component';
import { OrderMapComponent } from './order-map/order-map.component';
import { OrderForwardComponent } from './order-forward/order-forward.component';
@NgModule({
  declarations: [OrderComponent, OrderListComponent, OrderEditComponent, OrderViewComponent,OrderMapComponent, OrderForwardComponent],
  imports: [
    CommonModule,
    OrderManagementRoutingModule,
    SharedModule
  ],
  entryComponents: [
    OrderViewComponent,
    OrderMapComponent
    ],
})
export class OrderManagementModule { }
