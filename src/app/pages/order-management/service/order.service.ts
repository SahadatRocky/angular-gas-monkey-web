import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/internal/operators/map';
import { ApiEndpoints } from '../../../api-endpoints';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  HTTPOptions:Object = {
    observe: 'response',
    responseType: 'text'
 }

 private apiEndpoints: ApiEndpoints = new ApiEndpoints();

  constructor(private http: HttpClient) { }

  getOrderPageableTableData(page: number, size: number): Observable<any> {
    let url = this.apiEndpoints.ORDER_LIST_URL;
    const fullUrl = `${url}?page=${page}&size=${size}`;
    return this.http.get<any>(fullUrl).pipe(
      map(value => value)
      );
  }

  getOrderViewById(id:any): Observable<any>{
    let url = this.apiEndpoints.ORDER_GET_BY_ID_URL + '/' + id;
    return this.http.get<any>(url).pipe(
      map(value => value)
      );
  }

  // dorpdown list
  getCustomerDistrictDropList(): Observable<any> {
    let url = this.apiEndpoints.CUSTOMER_DISTRICT_DROPDOWN_LIST;
    return this.http.get<any>(url).pipe(
      map(value => value)
    );
  }

  getCustomerThanaDropList(districtId:any): Observable<any> {
    let url = this.apiEndpoints.CUSTOMER_THANA_DROPDOWN_LIST + '/' + districtId;
    return this.http.get<any>(url).pipe(
      map(value => value)
    );
  }

  getCustomerClusterDropList(thanaId:any): Observable<any> {
    let url = this.apiEndpoints.CUSTOMER_CLUSTER_DROPDOWN_LIST + '/' + thanaId;
    return this.http.get<any>(url).pipe(
      map(value => value)
    );
  }


  getPartnerDropListByAreaId(areaId:any): Observable<any> {
    let url = this.apiEndpoints.PARTNER_DROPDOWN_LIST_BY_AREA_ID + '/' + areaId;
    return this.http.get<any>(url).pipe(
      map(value => value)
    );
  }


  orderForward(Obj: any): Observable<any> {
    let url = this.apiEndpoints.ORDER_FORWARD_BASE_URL;
    return this.http.post<any>(url, Obj).pipe(map(value => value))
  }


  getAPIRouteDirection(apiKey:any,startLatLong:any, endLatLong:any){
    let url = this.apiEndpoints.API_ROUTE_DIRECTION_URL+'?api_key='+apiKey+'&start='+startLatLong+'&end='+endLatLong;
    return this.http.get<any>(url).pipe(
      map(value => value)
    );
  }

  getOrderTraceByOrderId(id:any){
    let url = this.apiEndpoints.ORDER_TRACE_URL + '/' + id;
    return this.http.get<any>(url).pipe(
      map(value => value)
    );

  }

}
