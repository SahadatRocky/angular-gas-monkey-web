import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderForwardComponent } from './order-forward.component';

describe('OrderForwardComponent', () => {
  let component: OrderForwardComponent;
  let fixture: ComponentFixture<OrderForwardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrderForwardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderForwardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
