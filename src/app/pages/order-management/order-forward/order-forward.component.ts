import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilService } from '../../shared/service/util.service';
import { OrderService } from '../service/order.service';

@Component({
  selector: 'app-order-forward',
  templateUrl: './order-forward.component.html',
  styleUrls: ['./order-forward.component.scss']
})
export class OrderForwardComponent implements OnInit {
  orderForwardForm: FormGroup;
  districtList:any;
  thanaList:any;
  areaList:any;
  partnerList:any;
  OrderId:any;
  selectedPartnerId:any;

  constructor(public dialogRef: MatDialogRef<OrderForwardComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private orderService : OrderService,
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private utilService: UtilService){}

  ngOnInit() {
    console.log('order-forward-',this.data.obj);
    this.OrderId = this.data.obj.element.id;
    this.createCouponFormGroup();
    this.getDistrictList();

  }

  createCouponFormGroup(){
    this.orderForwardForm = this.fb.group({
      districtId: ['', Validators.required],
      thanaId: ['', Validators.required],
      areaId: ['', Validators.required],
      partnerId: ['', Validators.required],

    } );

  }

  getDistrictList() {
    this.orderService.getCustomerDistrictDropList().subscribe((res) => {
      this.districtList = res;
      console.log('==================district', this.districtList);
    });
  }

  getThanaListByDistrictId(id: any) {
    this.orderService.getCustomerThanaDropList(id).subscribe((res) => {
      this.thanaList = res;
      console.log('==================thana', this.thanaList);
    });
  }

  getAreaListByThanaId(id: any) {
    this.orderService.getCustomerClusterDropList(id).subscribe((res) => {
      this.areaList = res;
      console.log('==================area', this.areaList);
    });
  }

  getPartnerListByAreaId(id: any) {
    this.orderService.getPartnerDropListByAreaId(id).subscribe((res) => {
      this.partnerList = res;
      console.log('==================partner', this.partnerList);
    });
  }



  onDistrictChange(event) {
    if(event.value == ""){
      this.orderForwardForm.get('thanaId').setValue("");
      this.orderForwardForm.get('areaId').setValue("");
      this.orderForwardForm.get('partnerId').setValue("");
    }else{
     this.getThanaListByDistrictId(event.value);
     this.orderForwardForm.get('areaId').setValue("");
     this.orderForwardForm.get('partnerId').setValue("");
    }
  }

  onThanaChange(event) {
    if(event.value == ""){
      this.orderForwardForm.get('areaId').setValue("");
      this.orderForwardForm.get('partnerId').setValue("");
    }else{
     this.getAreaListByThanaId(event.value);
     this.orderForwardForm.get('partnerId').setValue("");
    }
  }
  onAreaChange(event){
    if(event.value == ""){
      this.orderForwardForm.get('partnerId').setValue("");
    }else{
      this.getPartnerListByAreaId(event.value);
    }
  }

  onPartnerChange(event){
    //console.log(event.value);
    this.selectedPartnerId = event.value;
  }

  onSubmit(){
    let obj = {
      "orderId": this.OrderId,
      "partnerId": this.selectedPartnerId
    };

    this.orderService.orderForward(obj).subscribe(
      (res) => {
        this.utilService.showSnackBarMessage(
          "Order Forward Successfully",
          this.utilService.TYPE_MESSAGE.SUCCESS_TYPE
        );
        this.dialogRef.close();
        //this.router.navigate(["/pages/order-management/orders"]);
      },
      (error) => {
        this.utilService.showSnackBarMessage(
          error.message,
          this.utilService.TYPE_MESSAGE.ERROR_TYPE
        );
      }
    );


  }


   onNoClick(): void {
    this.dialogRef.close();
  }

  get districtId() {
    return this.orderForwardForm.get('districtId');
  }

  get thanaId() {
    return this.orderForwardForm.get('thanaId');
  }

  get areaId() {
    return this.orderForwardForm.get('areaId');
  }
  get partnerId() {
    return this.orderForwardForm.get('partnerId');
  }


}
