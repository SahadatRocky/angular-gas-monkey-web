import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { OBJ_EDIT, OBJ_NEW } from '../../../constants/gas-monkey-constants';

@Component({
  selector: 'app-order-edit',
  templateUrl: './order-edit.component.html',
  styleUrls: ['./order-edit.component.scss']
})
export class OrderEditComponent implements OnInit {
  buttonText: string;
  orderId:any;
  actionMode: string;
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  billingForm: FormGroup;
  shippingForm: FormGroup;
  customerId:any;

  constructor(private fb: FormBuilder, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(
      params => {
        this.orderId = params.id;
        // console.log(this.storeId);
        this.buttonText = this.orderId != '0' ? 'UPDATE' : 'SAVE';
        this.actionMode = this.orderId != '0' ? OBJ_EDIT : OBJ_NEW;
      });

      if(this.actionMode == OBJ_EDIT){
        // this.getDataStoreById();

        //this.userForm.controls['email'].disable();
      }else{
        // this.isShowLoading = false;
        // this.userForm.controls['userId'].enable();

      }
      this.createBillingFormGroup();


  }
  createBillingFormGroup(){
    this.billingForm = this.fb.group({
      id: [''],
      firstName: ['',  [Validators.required]],


    } );

  }

  get firstName() {
    return this.billingForm.get('firstName');
  }

  onSubmit(){}
  onChange($event){}

}
