import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { OrderService } from '../service/order.service';
@Component({
  selector: 'app-order-view',
  templateUrl: './order-view.component.html',
  styleUrls: ['./order-view.component.scss']
})
export class OrderViewComponent implements OnInit {

  orderViewData:any;

  constructor(public dialogRef: MatDialogRef<OrderViewComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private orderService : OrderService){}
  ngOnInit() {

    console.log(this.data.obj);
    this.oderViewData(this.data.obj.id);


  }

  oderViewData(id:any){
     this.orderService.getOrderViewById(id).subscribe(res=>{
         console.log('order-view-',res);
         this.orderViewData = res;
     });
  }


  onNoClick(): void {
    this.dialogRef.close();
  }
}
