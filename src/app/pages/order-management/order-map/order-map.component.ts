import {Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { icon, latLng, Map, marker, point, polyline, tileLayer } from 'leaflet';
import { OrderService } from '../service/order.service';
@Component({
  selector: 'app-order-map',
  templateUrl: './order-map.component.html',
  styleUrls: ['./order-map.component.scss']
})
export class OrderMapComponent implements OnInit {
  map: any;
  route:any;
  options:any;
  manOneMarker:any;
  manTwoMarker:any;
  orderId:any;
  // initialLatLong:any = [
  //   23.823201, 90.3779113
  //   // 23.86615060342202, 90.39221187870722
  // ];
  // destinationLatLong:any = [
  //   23.8226716,90.3933636
  //   // 23.875541390521143, 90.39609099608218
  // ];
  // routeDirection:any[];
//   responseRouteData=[
//     [23.823201,90.3779113],
//     [23.8231389,90.3780191],
//     [23.8231234,90.3781268],
//     [23.8232241,90.3786972],
//     [23.8233161,90.3791036],
//     [23.823418,90.3794402]
//     // [23.86615060342202, 90.39221187870722],
//     // [23.864603540478768, 90.39229856383349],
//     // [23.864593728914343, 90.39391861797263],
//     // [23.867325808182517, 90.39381715765255],
//     // [23.869519040784112, 90.3937475463942],
//     // [23.86952019079725, 90.39088769012366],
//     // [23.874442583509495, 90.39085532442716],
//     // [23.87452107003394, 90.39729262564236],
//     // [23.876012304956582, 90.39742137166667],
//     // [23.8759730621524, 90.39647723415509],
//     // [23.875541390521143, 90.39609099608218]
// ];

  streetMaps = tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    detectRetina: true,
    // attribution: '&amp;copy; &lt;a href="https://www.openstreetmap.org/copyright"&gt;OpenStreetMap&lt;/a&gt; contributors'
  });

  constructor( public dialogRef: MatDialogRef<OrderMapComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private orderService : OrderService){

      console.log('order-map-info--',this.data);
    }

  ngOnInit() {
    //this.routeDirection = this.responseRouteData;
    this.getManOneMarker(this.data.dest);
    this.getManTwoMarker(this.data.init);
    this.route = polyline(this.data.res);
    this.options = {
      layers: [ this.streetMaps, this.route, this.manTwoMarker, this.manOneMarker ],
      zoom: 14,
      center: latLng(this.data.init)
    };
 }

  // partner
  getManTwoMarker(initialLatLong){
    this.manTwoMarker = marker(initialLatLong, {
      icon: icon({
        iconSize: [ 25, 41 ],
        iconAnchor: [ 13, 41 ],
        iconUrl: 'assets/img/images/icon-maker-2.png',
        shadowUrl: 'leaflet/marker-shadow.png'
      })
    });
  }

  //house
  getManOneMarker(destinationLatLong){
    this.manOneMarker = marker(destinationLatLong, {
      icon: icon({
        iconSize: [ 25, 41 ],
        iconAnchor: [ 13, 41 ],
        iconUrl: 'assets/img/images/icon-maker-1.png',
        shadowUrl: 'leaflet/marker-shadow.png'
      })
    });
  }



  onMapReady(map: Map) {
    map.fitBounds(this.route.getBounds(), {
      padding: point(24, 24),
      maxZoom: 25,
      animate: true
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
