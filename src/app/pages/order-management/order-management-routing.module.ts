import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OrderEditComponent } from './order-edit/order-edit.component';
import { OrderListComponent } from './order-list/order-list.component';
import { OrderViewComponent } from './order-view/order-view.component';
import { OrderComponent } from './order/order.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'order-management'
    },
    children: [
      {
        path: '',
        redirectTo: 'orders'
      },
      {
        path: 'orders',
        component:OrderComponent ,
        data: {
          title: 'order-list'
        },
      },
      {
        path: 'order-list',
        component: OrderListComponent,
        data: {
          title: 'order-list'
        },
      },
      {
        path: 'order-edit/:id',
        component: OrderEditComponent,
        data: {
          title: 'order-create-or-edit'
        },
      },
      {
        path: 'order-view/:id',
        component: OrderViewComponent,
        data: {
          title: 'order-view'
        },
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderManagementRoutingModule { }
