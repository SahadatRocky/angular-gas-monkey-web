import { Injectable } from '@angular/core';
import { MasterButtonField } from '../../../../containers/core/master-component/master-model/master-button-field';
import { MasterButtonService } from '../../../../containers/core/master-component/master-service/master-button-service';

@Injectable({
  providedIn: 'root'
})
export class OrderButtonService extends MasterButtonService {
  constructor() {
    super();
    this.buttonFields = [
      new MasterButtonField({
        controlType: 'view',
      }),
      new MasterButtonField({
        controlType: 'map',
      }),
      new MasterButtonField({
        controlType: 'forward',
      }),
    ];
  }
}
