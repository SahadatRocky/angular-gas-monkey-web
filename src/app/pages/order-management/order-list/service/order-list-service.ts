import {Injectable} from '@angular/core';
import { MasterListService } from '../../../../containers/core/master-component/master-service/master-list-service';
import { TextData } from '../../../shared/model/common/text-data';

@Injectable({
  providedIn: 'root'
})
export class OrderListService extends MasterListService {
  constructor() {
    super();
    this.fieldInfo = {
      colWidth: 4,
      listTitleEn: 'Order',
      listTitleBn: 'তালিকা',
      fields: [
        new TextData({
          key: 'orderNumber',
          fieldWidth: 4,
          name: 'orderNumber',
          id: 'orderNumber',
          label: 'Order Number',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 1
        }),
        new TextData({
          key: 'customerName',
          fieldWidth: 4,
          name: 'customerName',
          id: 'customerName',
          label: 'Customer Name',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 3
        }),
        new TextData({
          key: 'partnerName',
          fieldWidth: 4,
          name: 'partnerName',
          id: 'partnerName',
          label: 'Partner Name',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 4
        }),
        new TextData({
          key: 'dealerName',
          fieldWidth: 4,
          name: 'dealerName',
          id: 'dealerName',
          label: 'Dealer Name',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 5
        }),
        new TextData({
          key: 'customerPhoneNo',
          fieldWidth: 4,
          name: 'customerPhoneNo',
          id: 'customerPhoneNo',
          label: 'Customer Mobile',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 6
        }),
        new TextData({
          key: 'productPhoto',
          fieldWidth: 4,
          name: 'productPhoto',
          id: 'productPhoto',
          label: 'Product Photo',
          type: 'img',
          canShow: true,
          canSort: true,
          order: 6
        }),
        new TextData({
          key: 'brandName',
          fieldWidth: 4,
          name: 'brandName',
          id: 'brandName',
          label: 'Brand Name',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 8
        }),
        new TextData({
          key: 'orderQuantity',
          fieldWidth: 4,
          name: 'orderQuantity',
          id: 'orderQuantity',
          label: 'Order Quantity',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 9
        }),
        new TextData({
          key: 'productSize',
          fieldWidth: 4,
          name: 'productSize',
          id: 'productSize',
          label: 'Product Size',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 10
        }),
        new TextData({
          key: 'price',
          fieldWidth: 4,
          name: 'price',
          id: 'price',
          label: 'Price',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 11
        }),

        new TextData({
          key: 'valveSize',
          fieldWidth: 4,
          name: 'valveSize',
          id: 'valveSize',
          label: 'Valve Size',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 12
        }),

        new TextData({
          key: 'orderDate',
          fieldWidth: 4,
          name: 'orderDate',
          id: 'orderDate',
          label: 'Order Date',
          type: 'date',
          canShow: true,
          canSort: true,
          order: 13
        }),

        new TextData({
          key: 'orderStatus',
          fieldWidth: 4,
          name: 'orderStatus',
          id: 'orderStatus',
          label: 'Order Status',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 14
        }),
        new TextData({
          key: 'action',
          fieldWidth: 4,
          name: 'অ্যাকশন',
          id: 'action',
          label: 'Action',
          // type: 'text',
          canShow: true,
          canSort: true,
          order: 15
        })
      ]
    };
  }
}
