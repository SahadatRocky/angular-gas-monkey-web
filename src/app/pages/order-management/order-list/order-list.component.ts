import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ServiceListControlWrapper } from '../../../containers/core/master-component/master-model/service-list-control-wrapper';
import { MasterListService } from '../../../containers/core/master-component/master-service/master-list-service';
import DataUtils from '../../shared/utils/data-utils';
import { OrderMapComponent } from '../order-map/order-map.component';
import { OrderViewComponent } from '../order-view/order-view.component';
import { OrderButtonService } from './service/order-button-service';
import { OrderListService } from './service/order-list-service';
import { OrderService } from '../service/order.service';
import { OrderForwardComponent } from '../order-forward/order-forward.component';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss']
})
export class OrderListComponent implements OnInit {
  api_key = "5b3ce3597851110001cf6248a61ad9a86175454099a5a92b4c0c0475";
  data: any;
  dataSize = 10;
  isShowLoading = false;
  dataSource = new MatTableDataSource();
  destinationLatLong = [];
  initialLatLong = [];
  responseRouteData = [];
  handleInterval:any;
  serviceListControlWrapper: ServiceListControlWrapper<
    OrderListService,
    OrderButtonService
 >;
  constructor(private router: Router,
    private orderService : OrderService,
    private orderListService: OrderListService,
    private orderButtonService: OrderButtonService,
    public dialog: MatDialog) { }

    ngOnInit(): void {
      this.loadList(0,this.dataSize);
     }


     loadList(offset, limit) {
        this.orderService.getOrderPageableTableData(offset,limit).subscribe((res) =>{
          console.log("------------order-list-data------------",res.content);
         this.data = DataUtils.flattenData(res.content);
         this.serviceListControlWrapper = new ServiceListControlWrapper<
           OrderListService,
           OrderButtonService
           >(
             this.orderListService,
             this.data,
             this.orderButtonService,
             res.totalElements,
            { pageSize: res.size, pageIndex: res.number }
           );
        });
     }

     onPageChange(event) {
      this.loadList(event.pageInfo.offset, event.pageInfo.limit);
    }

    execute(event: { element: any; rawData?: any; method: string; service?: MasterListService; methodKey?: string }) {
     // console.log(event);
     switch (event.method) {
       case 'add':
         //this.router.navigate(['/pages/role-management/user-create']);
         break;
       case 'map':
          this.getOrderTraceByOrderId(event.element.id);
          this.handleInterval = setInterval(()=>{
            console.log("call after in 1 minute");
            this.getOrderTraceByOrderId(event.element.id);
          },60000);
         break;
       case 'view':
        this.sendRowDataForViewOrder(event.element);
         break;
       case 'forward':
         this.sendRowDataForOrderForward(event);
         break;
     }
   }

   getOrderTraceByOrderId(orderId){
    this.orderService.getOrderTraceByOrderId(orderId).subscribe(res=> {
        console.log('traceBy ------',res);
        this.destinationLatLong = [];
        this.initialLatLong = [];
        this.destinationLatLong.push(Number(res.customer.latitude));
        this.destinationLatLong.push(Number(res.customer.longitude));
        this.initialLatLong.push(Number(res.partner.latitude));
        this.initialLatLong.push(Number(res.partner.longitude));
        console.log('des ------',this.destinationLatLong);
        console.log('init ------',this.initialLatLong);
        let startLatLong = res.customer.longitude+","+res.customer.latitude;
        let endLatLong = res.partner.longitude+","+res.partner.latitude;
        this.getAPIRouteDirection(startLatLong,endLatLong);
    });
 }

 getAPIRouteDirection(startLatLong:any, endLatLong:any){
  let rev = [];
  this.orderService.getAPIRouteDirection(this.api_key,startLatLong,endLatLong).subscribe(res=>{
      this.responseRouteData = [];
      console.log("=======>>>>before-add>>>",this.responseRouteData);
      res.features[0].geometry.coordinates.forEach(element => {
        this.responseRouteData.push([element[1],element[0]]);
      });
      console.log("=======>>>>after-add>>>",this.responseRouteData);
      this.sendRowDataForMapOrder(this.initialLatLong,this.destinationLatLong,this.responseRouteData);
   });


}


 sendRowDataForMapOrder(initialLatLong,destinationLatLong,responseRouteData) {
  //console.log(data);
  const dialogRef = this.dialog.open(OrderMapComponent, {
    // disableClose: true,
    width: '1000px',
    height: '500px',
    data: {
      title: 'Map',
      init: initialLatLong,
      dest : destinationLatLong,
      res: responseRouteData
    },
  });

  dialogRef.afterClosed().subscribe(result => {
    clearInterval(this.handleInterval);
    console.log('interval-close');
    console.log('map closed');
  });
}

sendRowDataForViewOrder(data){
  console.log(data);
  const dialogRef = this.dialog.open(OrderViewComponent, {
    disableClose: true,
      width: '700px',
      height: '700px',
    data: {
      title: 'View',
      obj: data
    },
  });
  dialogRef.afterClosed().subscribe(result => {
    console.log('view closed');
  });
}

sendRowDataForOrderForward(data){
  console.log(data);
  const dialogRef = this.dialog.open(OrderForwardComponent, {
    disableClose: true,
      width: '700px',
      height: '700px',
    data: {
      title: 'View',
      obj: data
    },
  });
  dialogRef.afterClosed().subscribe(result => {
    //console.log('view closed');
    this.loadList(0,this.dataSize);
  });
}

}
