import {EventEmitter, Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EmitterService {

  selectedUserObj: EventEmitter<any> = new EventEmitter<any>();
  selectedCustomerObj: EventEmitter<any> = new EventEmitter<any>();
  selectedPartnerObj:EventEmitter<any> = new EventEmitter<any>();
  selectedDealerObj:EventEmitter<any> = new EventEmitter<any>();
  brandObj: EventEmitter<any> = new EventEmitter<any>();
  selectedStoreObj: EventEmitter<any> = new EventEmitter<any>();
  selectedBrandObj: EventEmitter<any> = new EventEmitter<any>();
  selectedProductObj: EventEmitter<any> = new EventEmitter<any>();
  selectedProductTypeObj: EventEmitter<any> = new EventEmitter<any>();
  selectedProductPriceObj: EventEmitter<any> = new EventEmitter<any>();
  selectedPermissionObj: EventEmitter<any> = new EventEmitter<any>();
  selectedUserGroupPermissionObj: EventEmitter<any> = new EventEmitter<any>();
  selectedOrderObj: EventEmitter<any> = new EventEmitter<any>();
  selectedProductImageObj: EventEmitter<any> = new EventEmitter<any>();
  selectedCuponObj: EventEmitter<any> = new EventEmitter<any>();
  selectedDocumentObj: EventEmitter<any> = new EventEmitter<any>();
  selectedMobileWalletObj: EventEmitter<any> = new EventEmitter<any>();
  selectedIssueTypetObj: EventEmitter<any> = new EventEmitter<any>();
  selectedCancelReasonObj: EventEmitter<any> = new EventEmitter<any>();
  selectedTemplateObj: EventEmitter<any> = new EventEmitter<any>();
  selectedDepartmentObj: EventEmitter<any> = new EventEmitter<any>();
  selectedDesignationObj: EventEmitter<any> = new EventEmitter<any>();
  constructor() {
  }

  emitSelectedUserObj(selectedObj: any) {
    this.selectedUserObj.emit(selectedObj);
  }

  emitSelectedCustomerObj(selectedObj: any) {
    this.selectedCustomerObj.emit(selectedObj);
  }

  emitSelectedStoreObj(selectedObj: any) {
    this.selectedStoreObj.emit(selectedObj);
  }

  emitSelectedBrandObj(selectedObj: any) {
    this.selectedBrandObj.emit(selectedObj);
  }

  emitSelectedProductObj(selectedObj: any) {
    this.selectedProductObj.emit(selectedObj);
  }
  emitselectedProductTypeObj(selectedObj: any) {
    this.selectedProductTypeObj.emit(selectedObj);
  }
  emitselectedProductPriceObj(selectedObj: any) {
    this.selectedProductPriceObj.emit(selectedObj);
  }

  emitselectedPermissionObj(selectedObj: any) {
    this.selectedPermissionObj.emit(selectedObj);
  }
  emitselectedUserGroupPermissionObj(selectedObj: any) {
    this.selectedUserGroupPermissionObj.emit(selectedObj);
  }
  emitselectedOrderObj(selectedObj: any) {
    this.selectedCuponObj.emit(selectedObj);
  }

  emitselectedProductImageObj(selectedObj: any) {
    this.selectedProductImageObj.emit(selectedObj);
  }

  emitselectedPartnerObj(selectedObj: any) {
    this.selectedPartnerObj.emit(selectedObj);
  }

  emitSelectedCuponObj(selectedObj: any) {
    this.selectedCuponObj.emit(selectedObj);
  }

  emitSelectedDocumentObj(selectedObj: any) {
    this.selectedDocumentObj.emit(selectedObj);
  }

  emitselectedDealerObj(selectedObj: any) {
    this.selectedDealerObj.emit(selectedObj);
  }

  emitselectedMobileWalletObj(selectedObj: any) {
    this.selectedMobileWalletObj.emit(selectedObj);
  }

  emitselectedIssueTypetObj(selectedObj: any) {
    this.selectedIssueTypetObj.emit(selectedObj);
  }


  emitselectedCancelReasonObj(selectedObj: any) {
    this.selectedCancelReasonObj.emit(selectedObj);
  }
  emitselectedTemplateObj(selectedObj: any) {
    this.selectedTemplateObj.emit(selectedObj);
  }

  emitselectedDepartmentObj(selectedObj: any) {
    this.selectedDepartmentObj.emit(selectedObj);
  }

  emitselectedDesignationObj(selectedObj: any) {
    this.selectedDesignationObj.emit(selectedObj);
  }
}
