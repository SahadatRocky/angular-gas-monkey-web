import {Sort} from '@angular/material';

export interface IGrpTable {
  buildColumns();

  sortColumns();

  sortData(params: Sort);

  onScroll();
}
