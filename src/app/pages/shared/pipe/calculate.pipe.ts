import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  pure: true,
  name: 'sumOf'
})
export class CalculatePipe implements PipeTransform {

  transform(items: any[], propName: string): any {
    if (!items || !propName) {
      return items;
    }
    return this.calculateTotal(items, propName);
  }

  private calculateTotal(items: any[], columnName: string) {
    let total = 0;
    if (items) {
      if (items.length > 0) {
        for (let index = 0; index < items.length; index++) {
          const element = items[index];
          if (element[columnName]) {
            total += parseFloat(element[columnName]);
          }
        }
      }
    }
    return total;
  }

}
