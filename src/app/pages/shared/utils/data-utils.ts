import * as flatter from 'flat';
import * as _ from 'lodash';
import { PROFILE_IMAGE_TAG, SIGNATURE_TAG } from '../../../constants/gas-monkey-constants';

export default class DataUtils {
  static flattenData(data: any[]): any[] {
    const flatData = [];
    data.forEach(x => {
      flatData.push(flatter.flatten(x, {safe: true}));
    });
    return flatData;
  }

  static unFlattenData(data: any[]): any[] {
    const unFlatData = [];
    data.forEach(x => {
      unFlatData.push(flatter.unflatten(x, {safe: true}));
    });
    return unFlatData;
  }

  static autoCompleteShowValue(data: any, valueToShow: string) {
    const values = valueToShow.split(', ');
    if (values.length > 1) {
      const firstOne = _.get(data, values[0]);
      let result = '';
      if (firstOne) {
        result += firstOne + ' (';
      }
      let isValuePresent = false;
      values.splice(1, values.length).forEach(val => {
        const presentValue = _.get(data, val);
        if (presentValue) {
          isValuePresent = true;
          result += presentValue + ', ';
        }
      });
      result = result.length > 0 ? result.slice(0, result.length - 2) : '';
      if (isValuePresent) {
        result += ')';
      }
      return result;
    } else {
      return _.get(data, valueToShow);
    }
  }
}
