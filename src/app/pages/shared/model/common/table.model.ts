import { TextData } from "./text-data";

export class TableModel {
  columns: TextData[] = [];

  addColumn(column: TextData) {
    this.columns = [...this.columns, column];
  }
}
