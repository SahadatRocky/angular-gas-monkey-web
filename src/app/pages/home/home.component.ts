import { Component, OnInit, ViewChild } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets, RadialChartOptions } from 'chart.js';
import { Color, Label, MultiDataSet, SingleDataSet } from 'ng2-charts';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  // --------------------
// lineChart1
public lineChart1Data: Array<any> = [
  {
    data: [65, 59, 84, 84, 51, 55, 40],
    label: 'Partner'
  }
];
public lineChart1Labels: Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
public lineChart1Options: any = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips
  },
  maintainAspectRatio: false,
  scales: {
    xAxes: [{
      gridLines: {
        color: 'transparent',
        zeroLineColor: 'transparent'
      },
      ticks: {
        fontSize: 2,
        fontColor: 'transparent',
      }

    }],
    yAxes: [{
      display: false,
      ticks: {
        display: false,
        min: 40 - 5,
        max: 84 + 5,
      }
    }],
  },
  elements: {
    line: {
      borderWidth: 1
    },
    point: {
      radius: 4,
      hitRadius: 10,
      hoverRadius: 4,
    },
  },
  legend: {
    display: false
  }
};
public lineChart1Colours: Array<any> = [
  {
    backgroundColor: getStyle('--primary'),
    borderColor: 'rgba(255,255,255,.55)'
  }
];
public lineChart1Legend = false;
public lineChart1Type = 'line';

// lineChart2
public lineChart2Data: Array<any> = [
  {
    data: [1, 18, 9, 17, 34, 22, 11],
    label: 'Customer'
  }
];
public lineChart2Labels: Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
public lineChart2Options: any = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips
  },
  maintainAspectRatio: false,
  scales: {
    xAxes: [{
      gridLines: {
        color: 'transparent',
        zeroLineColor: 'transparent'
      },
      ticks: {
        fontSize: 2,
        fontColor: 'transparent',
      }

    }],
    yAxes: [{
      display: false,
      ticks: {
        display: false,
        min: 1 - 5,
        max: 34 + 5,
      }
    }],
  },
  elements: {
    line: {
      tension: 0.00001,
      borderWidth: 1
    },
    point: {
      radius: 4,
      hitRadius: 10,
      hoverRadius: 4,
    },
  },
  legend: {
    display: false
  }
};
public lineChart2Colours: Array<any> = [
  { // grey
    backgroundColor: getStyle('--info'),
    borderColor: 'rgba(255,255,255,.55)'
  }
];
public lineChart2Legend = false;
public lineChart2Type = 'line';


// lineChart3
public lineChart3Data: Array<any> = [
  {
    data: [78, 81, 80, 45, 34, 12, 40],
    label: 'Dealer'
  }
];
public lineChart3Labels: Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
public lineChart3Options: any = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips
  },
  maintainAspectRatio: false,
  scales: {
    xAxes: [{
      display: false
    }],
    yAxes: [{
      display: false
    }]
  },
  elements: {
    line: {
      borderWidth: 2
    },
    point: {
      radius: 0,
      hitRadius: 10,
      hoverRadius: 4,
    },
  },
  legend: {
    display: false
  }
};
public lineChart3Colours: Array<any> = [
  {
    backgroundColor: 'rgba(255,255,255,.2)',
    borderColor: 'rgba(255,255,255,.55)',
  }
];
public lineChart3Legend = false;
public lineChart3Type = 'line';


// barChart1
public barChart1Data: Array<any> = [
  {
    data: [78, 81, 80, 45, 34, 12, 40, 78, 81, 80, 45, 34, 12, 40, 12, 40],
    label: 'Income',
    barPercentage: 0.6,
  }
];
public barChart1Labels: Array<any> = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16'];
public barChart1Options: any = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips
  },
  maintainAspectRatio: false,
  scales: {
    xAxes: [{
      display: false,
    }],
    yAxes: [{
      display: false
    }]
  },
  legend: {
    display: false
  }
};
public barChart1Colours: Array<any> = [
  {
    backgroundColor: 'rgba(255,255,255,.3)',
    borderWidth: 0
  }
];
public barChart1Legend = false;
public barChart1Type = 'bar';

  // -------------------------
  public barChartOptions: ChartOptions = {
    responsive: true,
  };
  public barChartLabels: Label[] = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [];

  public barChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Last Week' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Current Week' }
  ];

  // Pie
  public pieChartOptions: ChartOptions = {
    responsive: true,
  };
  public pieChartLabels: Label[] = [['Beximco'], ['Jamuna'], 'Bashundhara'];
  public pieChartData: SingleDataSet = [300, 500, 100];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];

  //line
  public lineChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'This Week BDT ' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Last Week BDT ' }
  ];
  public lineChartLabels: Label[] = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  public lineChartOptions: ChartOptions = {
    responsive: true,
  };
  public lineChartColors: Color[] = [
    {
      borderColor: 'black',
      backgroundColor: 'rgba(255,0,0,0.3)',
    },
  ];
  public lineChartLegend = true;
  public lineChartType = 'line';
  public lineChartPlugins = [];

  ///Data table
  tableDATA: any[] = [
    {orderId: '#12567', product: 'Jamuna 12KG', customer: "mostafizur R.", status: 'Pending'},
    {orderId: '#3267', product: 'Bashundhara 12KG', customer: "Moudud", status: 'Deliver'},
    {orderId: '#12745', product: 'Jamuna 24KG', customer: "Zamir", status: 'Decline'}
  ];
  minDate:any;
  maxDate:any;
  public pageSizeOptions = [10,20,100,500];
  displayedColumns: string[];
  paginatorLength: number;
  pageEvent: PageEvent;
  dataSize = 10;
  isShowLoading = false;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  dataSource = new MatTableDataSource();

  // scatter
  /*
  public scatterChartOptions: ChartOptions = {
    responsive: true,
  };

  public scatterChartData: ChartDataSets[] = [
    {
      data: [
        { x: 1, y: 1 },
        { x: 2, y: 3 },
        { x: 3, y: -2 },
        { x: 4, y: 4 },
        { x: 5, y: -3, r: 20 },
      ],
      label: 'Series A',
      pointRadius: 10,
    },
  ];
  public scatterChartType: ChartType = 'scatter';
*/
  // Radar
  /*
  public radarChartOptions: RadialChartOptions = {
    responsive: true,
  };
  public radarChartLabels: Label[] = ['Eating', 'Drinking', 'Sleeping', 'Designing', 'Coding', 'Cycling', 'Running'];

  public radarChartData: ChartDataSets[] = [
    { data: [65, 59, 90, 81, 56, 55, 40], label: 'Series A' },
    { data: [28, 48, 40, 19, 96, 27, 100], label: 'Series B' }
  ];
  public radarChartType: ChartType = 'radar';
  */
  // Doughnut
  /*
  public doughnutChartLabels: Label[] = ['Download Sales', 'In-Store Sales', 'Mail-Order Sales'];
  public doughnutChartData: MultiDataSet = [
    [350, 450, 100],
    [50, 150, 120],
    [250, 130, 70],
  ];
  public doughnutChartType: ChartType = 'doughnut';
  */
  constructor() {}
  ngOnInit(): void {
    this.setApiColumns();
    this.getProductTypeList(0, this.dataSize);
  }

  setApiColumns() {
    this.displayedColumns =  ['orderId', 'product', 'customer', 'status'];
  }

  getProductTypeList(page: number, size: number) {
    this.setTableData(this.tableDATA);
    // this.isShowLoading = true;
    // this.productService.getProductTypeTableData(page,size).subscribe((res) =>{
    //   this.isShowLoading = false;
    //   console.log("----",res);
    //   this.setTableData(res.content);
    //   this.setPagination(res.totalPages, res.totalElements);
    // });
  }

  lastYear(){

  }

  lastMonth(){

  }

  lastWeek(){

  }

  setTableData(list: any) {
    this.dataSource.data = list;
  }

  setPagination(totalPages: number, totalElements: number) {
    this.paginatorLength = totalElements;
  }

  onPaginateChange(pageEvent: PageEvent) {
    this.getProductTypeList(pageEvent.pageIndex, pageEvent.pageSize);
  }

  orderDetailsStartDateChange(event){
     console.log('start date-',event.target.value);
     this.minDate = event.target.value;
  }

  orderDetailsEndDateChange(event){
    console.log('end date-',event.target.value);
    this.maxDate = event.target.value;
  }

  ratioStartDateChange(event){
    console.log('ratio start date-',event.target.value);
    this.minDate = event.target.value;
 }

 RatioEndDateChange(event){
   console.log('ratio end date-',event.target.value);
   this.maxDate = event.target.value;
 }


}
