import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { OBJ_EDIT, OBJ_NEW } from '../../../../constants/gas-monkey-constants';


const RADIO_LIST_FROM_DATABASE = [
  { name: 'MATH', topicId: "aa", checked: false, noOfQuestions:  "" },
  { name: 'ENGLISH', topicId: "aa",checked: false , noOfQuestions: ""},
  { name: 'IT', topicId: "aa" , checked: false, noOfQuestions: ""},
  { name: 'Bangla', topicId: "aa" , checked: false,noOfQuestions: ""}
];

@Component({
  selector: 'app-permission-edit',
  templateUrl: './permission-edit.component.html',
  styleUrls: ['./permission-edit.component.scss']
})
export class PermissionEditComponent implements OnInit {
  language: any = [
    {value: 'Bangla', viewValue: 'Bangla '},
    {value: 'English', viewValue: 'English'}

  ];
  group: any = [
    {value: 'Bangla', viewValue: 'Bangla '},
    {value: 'English', viewValue: 'English'}

  ];
  merchant: any = [
    {value: 'Bangla', viewValue: 'Bangla '},
    {value: 'English', viewValue: 'English'}

  ];

  @Input() selectedPermissionObj: any;
  userId:any;
  buttonText: string;
  actionMode: string;
  isShowLoading = false;
  permisisonForm: FormGroup;
  permissionList:any;
  fruits:any;

  constructor(private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(
      params => {
        this.userId = params.id;
        console.log(this.userId);
        this.buttonText = this.userId != '0' ? 'UPDATE' : 'SAVE';
        this.actionMode = this.userId != '0' ? OBJ_EDIT : OBJ_NEW;
      });

      if(this.actionMode == OBJ_EDIT){
       // this.getDataUserById();
        //this.userForm.controls['email'].disable();
      }else{
        // this.isShowLoading = false;
        // this.userForm.controls['userId'].enable();
      }

    this.createProductTypeFormGroup();
    this.getPermissonList();

  }

  createProductTypeFormGroup(){
    this.permisisonForm = this.fb.group({
        permissionName:[''],
        status:[false]

    } );
  }

  onSubmit(){
    console.log(this.permisisonForm.value);
  }

  getPermissonList() {
    // this.isShowLoading = true;
    // this.subjectService.getSubjectDropdownList().subscribe((res) =>{
    //   this.isShowLoading = false;
    //   console.log("subject-dropdownlist----",res);
    //   this.topicList = res;
    //   this._patchValues();
    // });

    this.permissionList = RADIO_LIST_FROM_DATABASE;
    console.log('****',this.permissionList);

  }

  goToList(){
    this.router.navigate(['/pages/role-management/permission']);
  }

  get permissionName() {
    return this.permisisonForm.get('permissionName');
  }
}
