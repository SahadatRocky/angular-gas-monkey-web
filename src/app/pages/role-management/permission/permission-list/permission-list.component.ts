import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ServiceListControlWrapper } from '../../../../containers/core/master-component/master-model/service-list-control-wrapper';
import { MasterListService } from '../../../../containers/core/master-component/master-service/master-list-service';
import { UtilService } from '../../../shared/service/util.service';
import DataUtils from '../../../shared/utils/data-utils';
import { PermissionButtonService } from './service/permission-button-service';
import { PermissionListService } from './service/permission-list-service';

@Component({
  selector: 'app-permission-list',
  templateUrl: './permission-list.component.html',
  styleUrls: ['./permission-list.component.scss']
})
export class PermissionListComponent implements OnInit {

  ELEMENT_DATA: any = [
    { permissionName: 'User Create', status: 'active' },
    { permissionName: 'Product Create', status: 'active' },
    { permissionName: 'Customer Create', status: 'active' },
    { permissionName: 'View Tracking Order', status: 'active' }
  ];

  data: any;
  dataSize =10;
  isShowLoading = false;
  dataSource = new MatTableDataSource();
  serviceListControlWrapper: ServiceListControlWrapper<
  PermissionListService,
  PermissionButtonService
 >;
  constructor(
    private utilService: UtilService,
    private permissionListService: PermissionListService,
    private permissionButtonService: PermissionButtonService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.loadList(0,this.dataSize);
   }

   loadList(offset, limit) {
     // this.userService.getUsersTableData(page,size).subscribe((res) =>{
       this.data = DataUtils.flattenData(this.ELEMENT_DATA);
       this.serviceListControlWrapper = new ServiceListControlWrapper<
       PermissionListService,
       PermissionButtonService
         >(
           this.permissionListService,
           this.data,
           this.permissionButtonService,
           20,
           {pageSize: 0, pageIndex: 0}
         );
     // });
   }

   onPageChange(event) {
    this.loadList(event.pageInfo.offset, event.pageInfo.limit);
  }

  execute(event: { element: any; rawData?: any; method: string; service?: MasterListService; methodKey?: string }) {
   // console.log(event);
   switch (event.method) {
     case 'add':
       break;
     case 'edit':
       // console.log('edit',event);
       break;
     case 'view':
       break;
   }
 }
}
