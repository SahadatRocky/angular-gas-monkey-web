import {Injectable} from '@angular/core';
import { MasterListService } from '../../../../../containers/core/master-component/master-service/master-list-service';
import { TextData } from '../../../../shared/model/common/text-data';

@Injectable({
  providedIn: 'root'
})
export class PermissionListService extends MasterListService {
  constructor() {
    super();
    this.fieldInfo = {
      colWidth: 4,
      listTitleEn: 'Permission',
      listTitleBn: 'উজারের তালিকা',
      fields: [
        new TextData({
          key: 'permissionName',
          fieldWidth: 4,
          name: 'permissionName',
          id: 'permissionName',
          label: 'Permission Name',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 1
        }),
        new TextData({
          key: 'status',
          fieldWidth: 4,
          name: 'status',
          id: 'status',
          label: 'Status',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 5
        })
      ]
    };
  }
}
