
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ServiceListControlWrapper } from '../../../containers/core/master-component/master-model/service-list-control-wrapper';
import { MasterListService } from '../../../containers/core/master-component/master-service/master-list-service';
import { UtilService } from '../../shared/service/util.service';
import DataUtils from '../../shared/utils/data-utils';
import { UserService } from '../service/user.service';
import { UserResetPasswordEditComponent } from '../user-reset-password-edit/user-reset-password-edit.component';
import { UserResetPasswordButtonService } from './service/user-reset-password-button-service';
import { UserResetPasswordListService } from './service/user-reset-password-list-service';

@Component({
  selector: 'app-user-reset-password-list',
  templateUrl: './user-reset-password-list.component.html',
  styleUrls: ['./user-reset-password-list.component.scss']
})
export class UserResetPasswordListComponent implements OnInit {

  data: any;
  isShowLoading = false;
  dataSource = new MatTableDataSource();
  dataSize = 10;
  serviceListControlWrapper: ServiceListControlWrapper<
  UserResetPasswordListService,
  UserResetPasswordButtonService
    >;
  constructor(private userService: UserService,
    private utilService: UtilService,
    private userResetPasswordListService: UserResetPasswordListService,
    private userResetPasswordButtonService: UserResetPasswordButtonService,
    public dialog: MatDialog,
    private router: Router
    ) { }


    ngOnInit(): void {
      this.loadList(0,this.dataSize);
     }

     loadList(offset, limit) {
      this.userService.getUsersTableData(offset,limit).subscribe((res) =>{
        console.log('***',res.content);
         this.data = DataUtils.flattenData(res.content);
         this.serviceListControlWrapper = new ServiceListControlWrapper<
         UserResetPasswordListService,
         UserResetPasswordButtonService
           >(
             this.userResetPasswordListService,
             this.data,
             this.userResetPasswordButtonService,
             res.totalElements,
            { pageSize: res.size, pageIndex: res.number }
           );
       });
     }

    onPageChange(event) {
      this.loadList(event.pageInfo.offset, event.pageInfo.limit);
    }

    execute(event: { element: any; rawData?: any; method: string; service?: MasterListService; methodKey?: string }) {
     // console.log(event);
     switch (event.method) {
       case 'add':
        this.resetPasswordDialog(event);
        //this.router.navigate(['/pages/role-management/user-create']);
         break;
       case 'reset-password':
        this.resetPasswordDialog(event.element);
         break;
     }
   }


  resetPasswordDialog(data) {
    console.log(data);
    const dialogRef = this.dialog.open(UserResetPasswordEditComponent, {
      width: '800px',
      height: '400px',
      data: {
         title:'user-reset-password',
         obj: data
      },
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
}
