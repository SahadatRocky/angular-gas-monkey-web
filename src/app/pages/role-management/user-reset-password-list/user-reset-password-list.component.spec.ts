import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UserResetPasswordListComponent } from './user-reset-password-list.component';

describe('UserResetPasswordListComponent', () => {
  let component: UserResetPasswordListComponent;
  let fixture: ComponentFixture<UserResetPasswordListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserResetPasswordListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserResetPasswordListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
