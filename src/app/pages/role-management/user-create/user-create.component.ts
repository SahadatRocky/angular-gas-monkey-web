import { Component, Input, OnInit } from "@angular/core";
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { OBJ_EDIT, OBJ_NEW } from "../../../constants/gas-monkey-constants";
import { MustMatch } from "../../shared/validation/must-match.validator";
import { UserService } from "../service/user.service";
import { v4 as uuidv4 } from "uuid";
import { UtilService } from "../../shared/service/util.service";
import { StoreService } from "../../store-management/service/store.service";
// import * as bcrypt from '../../../../../node_modules/bcryptjs';
// const saltRounds = 10;
// const myPlaintextPassword = 's0/\/\P4$$w0rD';

@Component({
  selector: "app-user-create",
  templateUrl: "./user-create.component.html",
  styleUrls: ["./user-create.component.scss"],
})
export class UserCreateComponent implements OnInit {
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$";
  salt: any;
  hash: any;

  @Input() selectedUserObj: any;
  userForm: FormGroup;
  useroId: any;
  buttonText: string;
  actionMode: string;
  isShowLoading = false;
  languageList: any;
  groupsList: any;
  departmentList: any;
  designationList: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router,
    private userService: UserService,
    private storeService: StoreService,
    private utilService: UtilService
  ) {}

  ngOnInit(): void {
    this.createFormGroup();
    this.getGroupDropdownListData();
    this.getDesignationDropdownListData();
    this.getDepartmentDropdownListData();

    // this.salt = bcrypt.genSaltSync(saltRounds);
    // this.hash = bcrypt.hashSync(myPlaintextPassword, this.salt);
    // console.log("hash-password", this.hash);
  }

  createFormGroup() {
    this.userForm = this.fb.group(
      {
        name: ["", [Validators.required]],
        email: [
          "",
          [Validators.required, Validators.pattern(this.emailPattern)],
        ],
        password: ["", [Validators.required, Validators.minLength(8)]],
        confirmPassword: ["", [Validators.required]],
        mobile: ["", [Validators.required, Validators.pattern("[0-9 ]{11}")]],
        groupId: ["",[Validators.required]],
        designationId: ["",[Validators.required]],
        departmentId: ["",[Validators.required]],
      },
      {
        validator: MustMatch("password", "confirmPassword"),
      }
    );
  }

  getGroupDropdownListData() {
    this.userService.getGroupDropdownList().subscribe((response) => {
      this.groupsList = response;
    });
  }

  getDesignationDropdownListData() {
    this.userService.getDesignationDropdownList().subscribe((response) => {
      this.designationList = response;
    });
  }

  getDepartmentDropdownListData() {
    this.userService.getDepartmentDropdownList().subscribe((response) => {
      this.departmentList = response;
    });
  }

  onSubmit(): void {

    let obj = {
      "userFullName": this.userForm.value.name,
      "password": this.userForm.value.password,
      "confirmPassword": this.userForm.value.confirmPassword,
      "mobile": this.userForm.value.mobile,
      "groupId": this.userForm.value.groupId,
      "email": this.userForm.value.email,
      "departmentId": this.userForm.value.departmentId,
      "designationId": this.userForm.value.designationId,
      "status": true
    };
    this.userService.UserRegistration(obj).subscribe(
      (res) => {
        this.utilService.showSnackBarMessage(
          "User Registration Successfully",
          this.utilService.TYPE_MESSAGE.SUCCESS_TYPE
        );
        this.router.navigate(["/pages/role-management/users"]);
      },
      (error) => {
        this.utilService.showSnackBarMessage(
          error.message,
          this.utilService.TYPE_MESSAGE.ERROR_TYPE
        );
      }
    );
  }

  goToList() {
    this.router.navigate(["/pages/role-management/users"]);
  }
  goToCheckAvailableEmail() {
    //todo
  }

  get name() {
    return this.userForm.get("name");
  }

  get email() {
    return this.userForm.get("email");
  }

  get password() {
    return this.userForm.get("password");
  }

  get mobile() {
    return this.userForm.get("mobile");
  }

  get confirmPassword() {
    return this.userForm.get("confirmPassword");
  }

  get groupId() {
    return this.userForm.get("groupId");
  }
  get designationId() {
    return this.userForm.get("designationId");
  }

  get departmentId() {
    return this.userForm.get("departmentId");
  }

}
