import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './user/user.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserEditComponent } from './user-edit/user-edit.component';
import { SharedModule } from '../shared/shared.module';

import { ChangePasswordComponent } from './change-password/change-password.component';
import { ProfileComponent } from './profile/profile.component';
import { UserCreateComponent } from './user-create/user-create.component';
import { PermissionComponent } from './permission/permission.component';
import { PermissionListComponent } from './permission/permission-list/permission-list.component';
import { PermissionEditComponent } from './permission/permission-edit/permission-edit.component';
import { UserGroupPermissionComponent } from './user-group-permission/user-group-permission.component';
import { UserGroupPermissionListComponent } from './user-group-permission/user-group-permission-list/user-group-permission-list.component';
import { UserGroupPermissionEditComponent } from './user-group-permission/user-group-permission-edit/user-group-permission-edit.component';
import { RoleManagementRoutingModule } from './role-management-routing.module';
import { UserResetPasswordEditComponent } from './user-reset-password-edit/user-reset-password-edit.component';
import { UserResetPasswordListComponent } from './user-reset-password-list/user-reset-password-list.component';
import { DepartmentComponent } from './department/department.component';
import { DepartmentListComponent } from './department/department-list/department-list.component';
import { DepartmentEditComponent } from './department/department-edit/department-edit.component';
import { DesignationComponent } from './designation/designation.component';
import { DesignationEditComponent } from './designation/designation-edit/designation-edit.component';
import { DesignationListComponent } from './designation/designation-list/designation-list.component';
@NgModule({
  declarations: [
    UserComponent,
    UserCreateComponent,
    UserListComponent,
    UserEditComponent,
    ProfileComponent,
    ChangePasswordComponent,
    PermissionComponent,
    PermissionListComponent,
    PermissionEditComponent,
    UserGroupPermissionComponent,
    UserGroupPermissionListComponent,
    UserGroupPermissionEditComponent,
    UserResetPasswordListComponent,
    UserResetPasswordEditComponent,
    DepartmentComponent,
    DepartmentListComponent,
    DepartmentEditComponent,
    DesignationComponent,
    DesignationListComponent,
    DesignationEditComponent
  ],
  imports: [
    CommonModule,
    RoleManagementRoutingModule,
    SharedModule
  ],
  entryComponents: [
    UserResetPasswordEditComponent,
    ChangePasswordComponent
    ]
})
export class RoleManagementModule { }
