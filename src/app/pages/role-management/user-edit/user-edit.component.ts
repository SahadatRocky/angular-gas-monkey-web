import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { OBJ_EDIT, OBJ_NEW } from '../../../constants/gas-monkey-constants';
import { UtilService } from '../../shared/service/util.service';
import { StoreService } from '../../store-management/service/store.service';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent implements OnInit {
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  @Input() selectedUserObj: any;
  userForm: FormGroup;
  userId:any;
  userData:any;
  buttonText: string;
  actionMode: string;
  isShowLoading = false;
  languageList:any;
  languageChange:any;
  groupChange:any;
  groupsList:any;
  designationList:any;
  departmentList:any;

  // groupsList: any = [
  //   {id: '1', value: 'Admin'},
  //   {id: '2', value: 'Report Viewer'}

  // ];

  // designationList: any = [
  //   {id: '1', value: 'Customer'},

  // ];

  // departmentList: any = [
  //   {id: '1', value: 'Sales'},

  // ];

  // userListData =
  //   { firstNameEng: 'Zamir', firstNameBan:'জমির',lastNameEng: 'ahmed', lastNameBan:'আহমেদ' , email: 'zamir@batworld.com',mobile:'01711212180', type: 'Admin' , active: true};

  // merchantStore: any = [
  //   {value: 'merchant1', viewValue: 'merchant 1'},
  //   {value: 'merchant2', viewValue: 'merchant 2'},
  //   {value: 'merchant3', viewValue: 'merchant 3'},
  // ];

  constructor(
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router,
    private userService: UserService,
    private storeService: StoreService,
    private utilService: UtilService) {
      // this.controlArray[0].setValue(false);
     }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(
      params => {
        this.userId = params.id;
        console.log(this.userId);
        this.buttonText = this.userId != '0' ? 'UPDATE' : 'SAVE';
        this.actionMode = this.userId != '0' ? OBJ_EDIT : OBJ_NEW;
      });

      this.createFormGroup();
      this.getGroupDropdownListData();
     this.getDesignationDropdownListData();
     this.getDepartmentDropdownListData();

      if(this.actionMode == OBJ_EDIT){
        this.getDataUserById();
        this.userForm.controls['email'].disable();
      }else{
        // this.isShowLoading = false;
        // this.userForm.controls['userId'].enable();
      }


  }

  createFormGroup(){
    this.userForm = this.fb.group({
      userFullName: ['',  [Validators.required]],
      email: ['',[Validators.required, Validators.pattern(this.emailPattern)]],
      mobile:['', [Validators.required,Validators.pattern("[0-9 ]{11}")]],
      groupId: ['',[Validators.required]],
      designationId: ['',[Validators.required]],
      departmentId: ['',[Validators.required]],
      status:[false]
    }
    );
  }

  getDataUserById(){
   // this.populateData(this.userListData);
    this.isShowLoading = true;
    this.userService.getUserById(this.userId).subscribe(res => {
      console.log(res);
      this.userData = res;
      this.isShowLoading = false;
      this.populateData(res);
    })
  }


  getGroupDropdownListData(){
    this.userService.getGroupDropdownList().subscribe(response =>{
      this.groupsList = response;
    });
}

getDesignationDropdownListData(){
 this.userService.getDesignationDropdownList().subscribe(response =>{
   this.designationList = response;
 });
}

getDepartmentDropdownListData(){
 this.userService.getDepartmentDropdownList().subscribe(response =>{
   this.departmentList = response;
 });
}

  populateData(myData) {
    console.log(myData);
    this.userForm.patchValue(myData);
    this.userForm.get('groupId').setValue(myData.groupInfoBean.id);
    this.userForm.get('designationId').setValue(myData.designation.id);
    this.userForm.get('departmentId').setValue(myData.department.id);
    this.userForm.controls['email'].disable();
  }

  onSubmit(): void {

    let obj = {
      "id": this.userId,
      "userFullName": this.userForm.value.userFullName,
      "mobile": this.userForm.value.mobile,
      "status": this.userForm.value.status,
      "groupId": this.userForm.value.groupId,
      "email": this.userForm.get('email').value,
      "departmentId": this.userForm.value.departmentId,
      "designationId": this.userForm.value.designationId
  };
  console.log('***',obj);
  this.userService.userUpdate(this.userId,obj).subscribe(res =>{
    console.log(res);
    this.utilService.showSnackBarMessage('User Updated Successfully',this.utilService.TYPE_MESSAGE.SUCCESS_TYPE);
    this.router.navigate(['/pages/role-management/users']);
  },error => {
    this.utilService.showSnackBarMessage(error.message,this.utilService.TYPE_MESSAGE.ERROR_TYPE);
  });
}

goToList(){
  this.router.navigate(['/pages/role-management/users']);
}

get email() {
  return this.userForm.get('email');
}

get name() {
  return this.userForm.get("userFullName");
}

get password(){
  return this.userForm.get('password');
}

get mobile(){
  return this.userForm.get('mobile');
}

get confirmPassword(){
  return this.userForm.get('confirmPassword');
}

get groupId(){
  return this.userForm.get('groupId');
}
get departmentId(){
  return this.userForm.get('departmentId');
}

get designationId(){
  return this.userForm.get('designationId');
}

}
