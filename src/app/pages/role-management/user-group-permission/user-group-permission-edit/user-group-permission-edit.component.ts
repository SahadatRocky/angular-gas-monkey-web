import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { OBJ_EDIT, OBJ_NEW } from '../../../../constants/gas-monkey-constants';
import { UtilService } from '../../../shared/service/util.service';
import { UserService } from '../../service/user.service';


@Component({
  selector: 'app-user-group-permission-edit',
  templateUrl: './user-group-permission-edit.component.html',
  styleUrls: ['./user-group-permission-edit.component.scss']
})
export class UserGroupPermissionEditComponent implements OnInit {

  @Input() selectedUserGroupPermissionObj: any;
  userId:any;
  buttonText: string;
  actionMode: string;
  isShowLoading = false;
  userGroupPermisisonForm: FormGroup;
  groupsList:any;
  menuList:any;
  selectedMenu = [];
  ids = [];
  usergrpId:any;

  constructor(private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private utilService: UtilService
    ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(
      params => {
        this.userId = params.id;
        console.log(this.userId);
        this.buttonText =  this.userId != '0' ? 'UPDATE'  : 'SAVE';
        this.actionMode = this.userId != '0' ? OBJ_EDIT : OBJ_NEW;
      });

    this.createUserGroupPermissionFormGroup();
    this.getGroupDropdownListData();
    //menu list
    this.getPermissonList();

      if(this.actionMode == OBJ_EDIT){
      }
      else{}
  }

  createUserGroupPermissionFormGroup(){
    this.userGroupPermisisonForm = this.fb.group({
      userGroupId:[''],
      menuBeans: new FormArray([])
    } );
  }

  getGroupDropdownListData() {
    this.userService.getGroupDropdownList().subscribe((response) => {
      console.log("user-permission-",response);
      this.groupsList = response;
    });
  }

  populateData(myData: any) {

    myData.forEach(
      el=> this.ids.push(el.menuId)
    );
    this.menuList.forEach(el =>{
          el.status = this.ids.includes(el.id);
    })
    this._patchValues();
    this.userGroupPermisisonForm.get('userGroupId').setValue(this.usergrpId);

  }

  //manu list
  getPermissonList() {
    this.userService.getAppMenuList().subscribe((res) =>{
      this.menuList = res;
      this.menuList.forEach(element =>{
        element.status = false;
      })
      this._patchValues();
    });
  }

  private _patchValues(): void {
    const formArray = new FormArray([]);
    this.menuList.forEach((menu) => {
      formArray.push(
        new FormGroup({
          menuId: new FormControl(menu.id),
          status: new FormControl(menu.status),
        })
      );
    });
    console.log(formArray)
    this.userGroupPermisisonForm = this.fb.group({
      userGroupId:[''],
      menuBeans: formArray
    } );
  }

  changeUseGroup(event){

    this.ids = [];
    this.usergrpId = event.value;
    this.userService.getAssignRoleMenuByUserGroupId(event.value).subscribe(res=>{
        this.populateData(res.menuBeans);
      });
  }

  onSubmit(){

    const { value } = this.userGroupPermisisonForm;
    let usergroupId = this.userGroupPermisisonForm.value.userGroupId;
    const selectedFruit =
      value?.menuBeans?.filter((f: any) => f.status) || [];

    this.selectedMenu = [];
    selectedFruit.forEach(element => {
       let obj = {
          "menuId": element.menuId
        }
        this.selectedMenu.push(obj);
    });

    this.userService.AssignRoleMenuPatchRequest(usergroupId,this.selectedMenu).subscribe(
      (res) => {
        this.utilService.showSnackBarMessage(
          "User Permission Created Successfully",
          this.utilService.TYPE_MESSAGE.SUCCESS_TYPE
        );
      },
      (error) => {
        this.utilService.showSnackBarMessage(
          error.message,
          this.utilService.TYPE_MESSAGE.ERROR_TYPE
        );
      }
    );
  }
}
