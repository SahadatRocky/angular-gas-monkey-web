import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UserGroupPermissionEditComponent } from './user-group-permission-edit.component';

describe('UserGroupPermissionEditComponent', () => {
  let component: UserGroupPermissionEditComponent;
  let fixture: ComponentFixture<UserGroupPermissionEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserGroupPermissionEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserGroupPermissionEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
