import { Component, OnInit } from '@angular/core';
import { OBJ_EDIT, OBJ_LIST, OBJ_NEW } from '../../../constants/gas-monkey-constants';
import { EmitterService } from '../../shared/service/emitter.service';

@Component({
  selector: 'app-user-group-permission',
  templateUrl: './user-group-permission.component.html',
  styleUrls: ['./user-group-permission.component.scss']
})
export class UserGroupPermissionComponent implements OnInit {

  mode: string;
  selectedUserGroupPermissionObj: any;

  constructor(private emitterService: EmitterService) { }

  ngOnInit(): void {
    this.mode = OBJ_LIST;
    this.listenEmittedApiObj();
  }
  listenEmittedApiObj() {
    this.emitterService.selectedUserGroupPermissionObj.subscribe(data => {
      if (data) {
        if (data.actionMode === OBJ_EDIT) {
          this.mode = OBJ_EDIT;
        } else if (data.actionMode === OBJ_NEW) {
          this.mode = OBJ_NEW;
        }  else {
          this.mode = OBJ_LIST;
        }
        this.selectedUserGroupPermissionObj = data;
      } else {
        this.mode = OBJ_LIST;
      }
    });
  }


}
