import {Injectable} from '@angular/core';
import { MasterListService } from '../../../../containers/core/master-component/master-service/master-list-service';
import { TextData } from '../../../shared/model/common/text-data';

@Injectable({
  providedIn: 'root'
})
export class UserPermissionListService extends MasterListService {
  constructor() {
    super();
    this.fieldInfo = {
      colWidth: 4,
      listTitleEn: 'User Permission',
      listTitleBn: 'উজারের তালিকা',
      fields: [
        new TextData({
          key: 'user',
          fieldWidth: 4,
          name: 'user',
          id: 'user',
          label: 'User',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 1
        }),
        new TextData({
          key: 'permission',
          fieldWidth: 4,
          name: 'permission',
          id: 'permission',
          label: 'Permission',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 2
        }),
        new TextData({
          key: 'action',
          fieldWidth: 4,
          name: 'অ্যাকশন',
          id: 'action',
          label: 'Action',
          // type: 'text',
          canShow: true,
          canSort: true,
          order: 6
        })
      ]
    };
  }
}
