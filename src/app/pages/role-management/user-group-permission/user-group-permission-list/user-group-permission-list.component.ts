import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ServiceListControlWrapper } from '../../../../containers/core/master-component/master-model/service-list-control-wrapper';
import { MasterListService } from '../../../../containers/core/master-component/master-service/master-list-service';
import { UtilService } from '../../../shared/service/util.service';
import DataUtils from '../../../shared/utils/data-utils';
import { UserPermissionButtonService } from '../service/user-permission-button-service';
import { UserPermissionListService } from '../service/user-permission-list-service';

@Component({
  selector: 'app-user-group-permission-list',
  templateUrl: './user-group-permission-list.component.html',
  styleUrls: ['./user-group-permission-list.component.scss']
})
export class UserGroupPermissionListComponent implements OnInit {

  ELEMENT_DATA: any = [
    { user: 'Sahadat', permission: 'User Create, Product Create' }
  ];

  data: any;
  isShowLoading = false;
  dataSource = new MatTableDataSource();
  dataSize = 10;
  serviceListControlWrapper: ServiceListControlWrapper<
  UserPermissionListService,
  UserPermissionButtonService
 >;

  constructor(
    private utilService: UtilService,
    private userPermissionListService: UserPermissionListService,
    private userPermissionButtonService: UserPermissionButtonService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.loadList(0,this.dataSize);
   }

   loadList(offset,limit) {
     // this.userService.getUsersTableData(page,size).subscribe((res) =>{
       this.data = DataUtils.flattenData(this.ELEMENT_DATA);
       this.serviceListControlWrapper = new ServiceListControlWrapper<
       UserPermissionListService,
       UserPermissionButtonService
         >(
           this.userPermissionListService,
           this.data,
           this.userPermissionButtonService,
           20,
           {pageSize: 0, pageIndex: 0}
          //  res.totalElements,
          //   { pageSize: res.size, pageIndex: res.number }
         );
     // });
   }



   onPageChange(event) {
    this.loadList(event.pageInfo.offset, event.pageInfo.limit);
  }


}
