import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ServiceListControlWrapper } from '../../../../containers/core/master-component/master-model/service-list-control-wrapper';
import { MasterListService } from '../../../../containers/core/master-component/master-service/master-list-service';
import { UtilService } from '../../../shared/service/util.service';
import DataUtils from '../../../shared/utils/data-utils';
import { UserService } from '../../service/user.service';
import { DesignationButtonService } from './service/designation-button.service';
import { DesignationListService } from './service/designation-list.service';


@Component({
  selector: 'app-designation-list',
  templateUrl: './designation-list.component.html',
  styleUrls: ['./designation-list.component.scss']
})
export class DesignationListComponent implements OnInit {

  chkStatus:boolean;
  data: any;
  isShowLoading = false;
  dataSource = new MatTableDataSource();
  dataSize = 10;
  serviceListControlWrapper: ServiceListControlWrapper<
  DesignationListService,
  DesignationButtonService
 >;

  constructor(
    private utilService: UtilService,
    private designationListService: DesignationListService,
    private designationButtonService: DesignationButtonService,
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.loadList(0,this.dataSize);
  }

  loadList(offset,limit) {
    this.userService.getDesignationListTableData(offset, limit).subscribe((res) =>{
      console.log("------------designation-list-data------------",res);
      this.data = DataUtils.flattenData(res.content);
      this.serviceListControlWrapper = new ServiceListControlWrapper<
      DesignationListService,
      DesignationButtonService
        >(
          this.designationListService,
          this.data,
          this.designationButtonService,
          res.totalElements,
          { pageSize: res.size, pageIndex: res.number }
        );
     });
  }

  onPageChange(event) {
    this.loadList(event.pageInfo.offset, event.pageInfo.limit);
  }

  execute(event: { element: any; rawData?: any; method: string; service?: MasterListService; methodKey?: string }) {
    // console.log(event);
    switch (event.method) {
      case 'add':
        this.router.navigate(['/pages/role-management/designation/0']);
        break;
      case 'edit':
        // console.log('edit',event);
        this.router.navigate(['/pages/role-management/designation/'+ event.element.id]);
        break;
    }
  }

}
