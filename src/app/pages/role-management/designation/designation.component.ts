import { Component, OnInit } from '@angular/core';
import { OBJ_EDIT, OBJ_LIST, OBJ_NEW } from '../../../constants/gas-monkey-constants';
import { EmitterService } from '../../shared/service/emitter.service';

@Component({
  selector: 'app-designation',
  templateUrl: './designation.component.html',
  styleUrls: ['./designation.component.scss']
})
export class DesignationComponent implements OnInit {
  mode: string;
  selectedDesignationObj: any;

  constructor(
    private emitterService: EmitterService
  ) { }

  ngOnInit(): void {
    this.mode = OBJ_LIST;
    this.listenEmittedApiObj();
  }
  listenEmittedApiObj() {
    this.emitterService.selectedDesignationObj.subscribe(data => {
      if (data) {
        if (data.actionMode === OBJ_EDIT) {
          this.mode = OBJ_EDIT;
        } else if (data.actionMode === OBJ_NEW) {
          this.mode = OBJ_NEW;
        }  else {
          this.mode = OBJ_LIST;
        }
        this.selectedDesignationObj = data;
      } else {
        this.mode = OBJ_LIST;
      }
    });
  }

}
