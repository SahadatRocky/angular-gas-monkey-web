import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { OBJ_EDIT, OBJ_NEW } from '../../../../constants/gas-monkey-constants';
import { UtilService } from '../../../shared/service/util.service';
import { UserService } from '../../service/user.service';

@Component({
  selector: 'app-designation-edit',
  templateUrl: './designation-edit.component.html',
  styleUrls: ['./designation-edit.component.scss']
})
export class DesignationEditComponent implements OnInit {
  designationForm: FormGroup;
  isShowLoading = false;
  buttonText: string;
  actionMode: string;
  @Input() selectedDesignationObj: any;
  designationId: any;

  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private utilService: UtilService,
    private userService: UserService,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      this.designationId = params.id;
      //console.log(this.brandId);
      this.buttonText = this.designationId != "0" ? "UPDATE" : "SAVE";
      this.actionMode = this.designationId != "0" ? OBJ_EDIT : OBJ_NEW;
    });

    this.createDesignationFormGroup();
    if (this.actionMode == OBJ_EDIT) {
      this.getDesignationById();
    } else {
      this.isShowLoading = false;
    }
  }

  createDesignationFormGroup() {
    this.designationForm = this.fb.group({
      name: ["", Validators.required],
      status:[false]
    });
  }

  checkDisable() {
    return !this.designationForm.valid;
  }

  getDesignationById() {
    this.isShowLoading = true;
    this.userService.getDesignationById(this.designationId).subscribe((res) => {
      console.log('designationId get by id-',res);
      this.isShowLoading = false;
      this.populateData(res);
    });
  }

  populateData(myData) {
    this.designationForm.patchValue(myData);
  }

  onSubmit() {
    if (this.actionMode == OBJ_EDIT) {
      let obj = {
        "id": this.designationId,
        "name": this.designationForm.value.name,
        "status": this.designationForm.value.status
      };
      console.log(obj);
      this.userService.updateDesignation(this.designationId, obj).subscribe(
        (res) => {

          this.utilService.showSnackBarMessage(
            "Designation Updated Successfully",
            this.utilService.TYPE_MESSAGE.SUCCESS_TYPE
          );
          this.router.navigate(["pages/role-management/designation"]);
        },
        (error) => {
          this.utilService.showSnackBarMessage(
            error.message,
            this.utilService.TYPE_MESSAGE.ERROR_TYPE
          );
        }
      );
    }else{
      let obj = {
        "name": this.designationForm.value.name,
        "status": true
      };
      console.log(obj);
      this.userService.createDesignation(obj).subscribe(
        (res) => {
          this.utilService.showSnackBarMessage(
            "Designation Created Successfully",
            this.utilService.TYPE_MESSAGE.SUCCESS_TYPE
          );
          this.router.navigate(["pages/role-management/designation"]);
        },
        (error) => {
          this.utilService.showSnackBarMessage(
            error.message,
            this.utilService.TYPE_MESSAGE.ERROR_TYPE
          );
        }
      );
    }
  }

  goToList() {
    this.router.navigate(["/pages/role-management/designation"]);
  }

  get name(){
    return this.designationForm.get("name");
  }

}
