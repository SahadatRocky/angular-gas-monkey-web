import { Component, Inject, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../auth/service/auth.service';
import { UtilService } from '../../shared/service/util.service';
import { MustMatch } from '../../shared/validation/must-match.validator';
import { UserService } from '../service/user.service';


@Component({
  selector: 'app-user-reset-password-edit',
  templateUrl: './user-reset-password-edit.component.html',
  styleUrls: ['./user-reset-password-edit.component.scss']
})
export class UserResetPasswordEditComponent implements OnInit {
  userResetPasswordForm: FormGroup;
  userId:any;

  constructor(private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router,
    private userService: UserService,
    private authService: AuthService,
    public dialogRef: MatDialogRef<UserResetPasswordEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private utilService: UtilService
    ) { }

  ngOnInit(): void {
    this.createFormGroup();
    console.log(this.data.obj);
    this.userId = this.data.obj.id;
  }
  createFormGroup(){
    this.userResetPasswordForm = this.fb.group({
      password: ["", [Validators.required, Validators.minLength(8)]],
      confirmPassword: ["", [Validators.required]],
    },
    {
      validator: MustMatch("password", "confirmPassword"),
    }
    );
  }

  onNoClick(){
    this.dialogRef.close();
  }

  onOptionsSelected(event:any) {
    console.log(event);
 }


  onChange(event:any){
    console.log(event.value);
    if(event.value==='profile'){
     this.router.navigateByUrl('/pages/user-management/profile');
    }else{
     this.router.navigateByUrl('/pages/user-management/change-password');
    }
    // this.router.navigateByUrl('/pages/user-management/change-password');

 }
 onSubmit(){

  let obj = {
    "userId": this.userId,
    "password": this.userResetPasswordForm.value.password,
    "confirmPassword" : this.userResetPasswordForm.value.confirmPassword
}
console.log(obj);
  this.userService.UserResetPassword(obj).subscribe(
    (res) => {
      this.utilService.showSnackBarMessage(
        "User Reset password Successfully",
        this.utilService.TYPE_MESSAGE.SUCCESS_TYPE
      );
      this.dialogRef.close();
      this.authService.logout();
     // this.router.navigate(["/pages/role-management/user-reset-password-list"]);
    },
    (error) => {
      this.utilService.showSnackBarMessage(
        error.message,
        this.utilService.TYPE_MESSAGE.ERROR_TYPE
      );
    }
  );
 }

 goToList(){
  this.router.navigate(['/pages/role-management/user-reset-password-list']);
}


get password() {
  return this.userResetPasswordForm.get("password");
}

get confirmPassword() {
  return this.userResetPasswordForm.get("confirmPassword");
}


}
