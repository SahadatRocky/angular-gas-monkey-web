import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ServiceListControlWrapper } from '../../../../containers/core/master-component/master-model/service-list-control-wrapper';
import { MasterListService } from '../../../../containers/core/master-component/master-service/master-list-service';
import { UtilService } from '../../../shared/service/util.service';
import DataUtils from '../../../shared/utils/data-utils';
import { UserService } from '../../service/user.service';
import { DepartmentButtonService } from './service/department-button.service';
import { DepartmentListService } from './service/department-list.service';


@Component({
  selector: 'app-department-list',
  templateUrl: './department-list.component.html',
  styleUrls: ['./department-list.component.scss']
})
export class DepartmentListComponent implements OnInit {

  chkStatus:boolean;
  data: any;
  isShowLoading = false;
  dataSource = new MatTableDataSource();
  dataSize = 10;
  serviceListControlWrapper: ServiceListControlWrapper<
  DepartmentListService,
  DepartmentButtonService
 >;

  constructor(
    private utilService: UtilService,
    private departmentListService: DepartmentListService,
    private departmentButtonService: DepartmentButtonService,
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.loadList(0,this.dataSize);
  }

  loadList(offset,limit) {
    this.userService.getDepartmentListTableData(offset, limit).subscribe((res) =>{
      console.log("------------Department-list-data------------",res);
      this.data = DataUtils.flattenData(res.content);
      this.serviceListControlWrapper = new ServiceListControlWrapper<
      DepartmentListService,
      DepartmentButtonService
        >(
          this.departmentListService,
          this.data,
          this.departmentButtonService,
          res.totalElements,
          { pageSize: res.size, pageIndex: res.number }
        );
     });
  }

  onPageChange(event) {
    this.loadList(event.pageInfo.offset, event.pageInfo.limit);
  }

  execute(event: { element: any; rawData?: any; method: string; service?: MasterListService; methodKey?: string }) {
    // console.log(event);
    switch (event.method) {
      case 'add':
        this.router.navigate(['/pages/role-management/department/0']);
        break;
      case 'edit':
        // console.log('edit',event);
        this.router.navigate(['/pages/role-management/department/'+ event.element.id]);
        break;
    }
  }

}
