import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { OBJ_EDIT, OBJ_NEW } from '../../../../constants/gas-monkey-constants';
import { UtilService } from '../../../shared/service/util.service';
import { UserService } from '../../service/user.service';

@Component({
  selector: 'app-department-edit',
  templateUrl: './department-edit.component.html',
  styleUrls: ['./department-edit.component.scss']
})
export class DepartmentEditComponent implements OnInit {
  departmentForm: FormGroup;
  isShowLoading = false;
  buttonText: string;
  actionMode: string;
  @Input() selectedDepartmentObj: any;
  departmentId: any;

  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private utilService: UtilService,
    private userService: UserService,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      this.departmentId = params.id;
      this.buttonText = this.departmentId != "0" ? "UPDATE" : "SAVE";
      this.actionMode = this.departmentId != "0" ? OBJ_EDIT : OBJ_NEW;
    });

    this.createDepartmentFormGroup();

    if (this.actionMode == OBJ_EDIT) {
      this.getDepartmentById();
    } else {
      this.isShowLoading = false;
    }
  }

  createDepartmentFormGroup() {
    this.departmentForm = this.fb.group({
      name: ["", Validators.required],
      status:[false]
    });
  }


  checkDisable() {
    return !this.departmentForm.valid;
  }

  getDepartmentById() {
    this.isShowLoading = true;
    this.userService.getDepartmentById(this.departmentId).subscribe((res) => {
      console.log('department get by id-',res);
      this.isShowLoading = false;
      this.populateData(res);
    });
  }

  populateData(myData) {
    this.departmentForm.patchValue(myData);
  }

  onSubmit() {
      if (this.actionMode == OBJ_EDIT) {
        let obj = {
          "id": this.departmentId,
          "name": this.departmentForm.value.name,
          "status": this.departmentForm.value.status
        };
        console.log(obj);
        this.userService.updateDepartment(this.departmentId, obj).subscribe(
          (res) => {

            this.utilService.showSnackBarMessage(
              "Department Updated Successfully",
              this.utilService.TYPE_MESSAGE.SUCCESS_TYPE
            );
            this.router.navigate(["pages/role-management/department"]);
          },
          (error) => {
            this.utilService.showSnackBarMessage(
              error.message,
              this.utilService.TYPE_MESSAGE.ERROR_TYPE
            );
          }
        );
      }else{
        let obj = {
          "name": this.departmentForm.value.name,
          "status": true
        };
        console.log(obj);
        this.userService.createDepartment(obj).subscribe(
          (res) => {
            this.utilService.showSnackBarMessage(
              "Department Created Successfully",
              this.utilService.TYPE_MESSAGE.SUCCESS_TYPE
            );
            this.router.navigate(["pages/role-management/department"]);
          },
          (error) => {
            this.utilService.showSnackBarMessage(
              error.message,
              this.utilService.TYPE_MESSAGE.ERROR_TYPE
            );
          }
        );
      }
  }

  goToList() {
    this.router.navigate(["/pages/role-management/department"]);
  }

  get name(){
    return this.departmentForm.get("name");
  }
}
