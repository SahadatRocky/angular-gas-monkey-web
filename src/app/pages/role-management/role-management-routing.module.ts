import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { DepartmentEditComponent } from './department/department-edit/department-edit.component';
import { DepartmentListComponent } from './department/department-list/department-list.component';
import { DepartmentComponent } from './department/department.component';
import { DesignationEditComponent } from './designation/designation-edit/designation-edit.component';
import { DesignationListComponent } from './designation/designation-list/designation-list.component';
import { DesignationComponent } from './designation/designation.component';
import { PermissionEditComponent } from './permission/permission-edit/permission-edit.component';
import { PermissionListComponent } from './permission/permission-list/permission-list.component';
import { PermissionComponent } from './permission/permission.component';
import { ProfileComponent } from './profile/profile.component';
import { UserCreateComponent } from './user-create/user-create.component';
import { UserEditComponent } from './user-edit/user-edit.component';
import { UserGroupPermissionEditComponent } from './user-group-permission/user-group-permission-edit/user-group-permission-edit.component';
import { UserGroupPermissionListComponent } from './user-group-permission/user-group-permission-list/user-group-permission-list.component';
import { UserGroupPermissionComponent } from './user-group-permission/user-group-permission.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserResetPasswordEditComponent } from './user-reset-password-edit/user-reset-password-edit.component';
import { UserResetPasswordListComponent } from './user-reset-password-list/user-reset-password-list.component';
import { UserComponent } from './user/user.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'role-management'
    },
    children: [
      {
        path: '',
        redirectTo: 'users',
        data: {
          title: 'user-list'
        },
      },
      {
        path: 'users',
        component: UserComponent,
        data: {
          title: 'user-list'
        },
      },
      {
        path: 'user-create',
        component: UserCreateComponent,
        data: {
          title: 'user-create'
        },
      },
      {
        path: 'user-list',
        component: UserListComponent,
        data: {
          title: 'user-list'
        },
      },
      {
        path: 'user-edit/:id',
        component: UserEditComponent,
        data: {
          title: 'user-edit'
        },
      },
      {
        path: 'profile',
        component: ProfileComponent,
        data: {
          title: 'profile'
        },
      },
      // {
      //   path: 'change-password',
      //   component: ChangePasswordComponent,
      //   data: {
      //     title: 'reset-password'
      //   },
      // },
      {
        path: 'permission',
        component: PermissionComponent,
        data: {
          title: 'permission-list'
        },
      },
      {
        path: 'permission-list',
        component: PermissionListComponent,
        data: {
          title: 'permission-list'
        },
      },
      {
        path: 'permission/:id',
        component: PermissionEditComponent,
        data: {
          title: 'permission-create-or-edit'
        },
      },
      // {
      //   path: 'user-group-permission',
      //   component: UserGroupPermissionComponent,
      //   data: {
      //     title: 'user-permission-list'
      //   },
      // },
      // {
      //   path: 'user-group-permission-list',
      //   component: UserGroupPermissionListComponent,
      //   data: {
      //     title: 'user-permission-list'
      //   },
      // },
      {
        path: 'user-group-permission/:id',
        component: UserGroupPermissionEditComponent,
        data: {
          title: 'user-permission-create-or-edit'
        },
      },
      {
        path: 'user-reset-password-list',
        component: UserResetPasswordListComponent,
        data: {
          title: 'user-reset-password-list'
        },
      },
      {
        path: "department",
        component: DepartmentComponent,
        data: {
          title: "department",
        },
      },
      {
        path: "department-list",
        component: DepartmentListComponent,
        data: {
          title: "department-list",
        },
      },
      {
        path: "department/:id",
        component: DepartmentEditComponent,
        data: {
          title: "department-edit",
        },
      },
      {
        path: "designation",
        component: DesignationComponent,
        data: {
          title: "designation",
        },
      },
      {
        path: "designation-list",
        component: DesignationListComponent,
        data: {
          title: "designation-list",
        },
      },
      {
        path: "designation/:id",
        component: DesignationEditComponent,
        data: {
          title: "designation-edit",
        },
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoleManagementRoutingModule { }
