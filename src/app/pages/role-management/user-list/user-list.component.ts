
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ServiceListControlWrapper } from '../../../containers/core/master-component/master-model/service-list-control-wrapper';
import { MasterListService } from '../../../containers/core/master-component/master-service/master-list-service';
import { UtilService } from '../../shared/service/util.service';
import DataUtils from '../../shared/utils/data-utils';
import { UserService } from '../service/user.service';
import {  UserButtonService } from './service/user-button-service';
import {  UserListService } from './service/user-list-service';
@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  // ELEMENT_DATA: any = [
  //   { firstName: 'Zamir', firstNameBn:'জমির',lastName: 'Ahmed', lastNameBn:'আহমেদ' , email: 'zamir@batworld.com',mobile:'01711212180', type: 'Admin' , status: 'active'}
  // ];
  data: any;
  dataSize = 10;
  isShowLoading = false;
  dataSource = new MatTableDataSource();

  serviceListControlWrapper: ServiceListControlWrapper<
    UserListService,
    UserButtonService
    >;

  constructor(
    private utilService: UtilService,
    private  userService: UserService,
    private  userButtonService: UserButtonService,
    private userListService: UserListService,
    protected dialog: MatDialog,
    private router: Router
    ) {
     }

  ngOnInit(): void {
   this.loadList(0,this.dataSize);
  }

  loadList(offset, limit) {
     this.userService.getUsersTableData(offset, limit).subscribe((res) =>{
      console.log('user-list:::',res.content);
      this.data = DataUtils.flattenData(res.content);
      this.serviceListControlWrapper = new ServiceListControlWrapper<
        UserListService,
        UserButtonService
        >(
          this.userListService,
          this.data,
          this.userButtonService,
          res.totalElements,
          { pageSize: res.size, pageIndex: res.number }
        );
     });
  }



  // getFirstNameAndLastName(element?: any) {
  //   element.forEach((el,ind)=>{
  //     if (el) {
  //       this.data[ind]['name'] = el.firstName + " " + el.lastName;
  //     }
  //   });
  //   return this.data;
  // }

  onPageChange(event) {
    this.loadList(event.pageInfo.offset, event.pageInfo.limit);
  }

 execute(event: { element: any; rawData?: any; method: string; service?: MasterListService; methodKey?: string }) {
  // console.log(event);
  switch (event.method) {
    case 'add':
      this.router.navigate(['/pages/role-management/user-create']);
      break;
    case 'edit':
       console.log('edit',event);
      this.router.navigate(['/pages/role-management/user-edit/'+ event.element.id]);
      break;
    case 'view':
      break;
  }
}

}


