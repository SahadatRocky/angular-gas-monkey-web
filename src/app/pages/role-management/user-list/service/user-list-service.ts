import {Injectable} from '@angular/core';
import { MasterListService } from '../../../../containers/core/master-component/master-service/master-list-service';
import { TextData } from '../../../shared/model/common/text-data';

@Injectable({
  providedIn: 'root'
})
export class UserListService extends MasterListService {
  constructor() {
    super();
    this.fieldInfo = {
      colWidth: 4,
      listTitleEn: 'User',
      listTitleBn: 'উজারের তালিকা',
      fields: [
        new TextData({
          key: 'userFullName',
          fieldWidth: 4,
          name: 'name',
          id: 'name',
          label: 'Name',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 1
        }),
        new TextData({
          key: 'email',
          fieldWidth: 4,
          name: 'email',
          id: 'email',
          label: 'Email',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 2
        }),
        new TextData({
          key: 'mobile',
          fieldWidth: 4,
          name: 'mobile',
          id: 'mobile',
          label: 'Mobile',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 3
        }),
        new TextData({
          key: 'userType',
          fieldWidth: 4,
          name: 'userType',
          id: 'userType',
          label: 'User Type',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 4
        }),
        new TextData({
          key: 'designation',
          fieldWidth: 4,
          name: 'designation',
          id: 'designation',
          label: 'Designation',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 4
        }),
        new TextData({
          key: 'department',
          fieldWidth: 4,
          name: 'department',
          id: 'department',
          label: 'Department',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 4
        }),
        new TextData({
          key: 'status',
          fieldWidth: 4,
          name: 'status',
          id: 'status',
          label: 'Status',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 5
        }),
        new TextData({
          key: 'action',
          fieldWidth: 4,
          name: 'অ্যাকশন',
          id: 'action',
          label: 'Action',
          // type: 'text',
          canShow: true,
          canSort: true,
          order: 6
        })
      ]
    };
  }
}
