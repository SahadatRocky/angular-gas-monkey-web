import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ServiceListControlWrapper } from '../../../containers/core/master-component/master-model/service-list-control-wrapper';
import { MasterListService } from '../../../containers/core/master-component/master-service/master-list-service';
import { UtilService } from '../../shared/service/util.service';
import DataUtils from '../../shared/utils/data-utils';
import { TransectionHistoryButtonService } from './service/transection-history-button.service';
import { TransectionHistoryListService } from './service/transection-history-list.service';

@Component({
  selector: 'app-transection-history-list',
  templateUrl: './transection-history-list.component.html',
  styleUrls: ['./transection-history-list.component.scss']
})
export class TransectionHistoryListComponent implements OnInit {

  ELEMENT_DATA: any = [
    {  paymentId: 'pay-001', partnerId: 'par-002', customerId: 'cus-004',paymentDate: '2022-7-23', amount : 1400, paymentType: 'Bikas'}
  ];

  data: any;
  isShowLoading = false;
  dataSource = new MatTableDataSource();
  dataSize = 10;
  serviceListControlWrapper: ServiceListControlWrapper<
  TransectionHistoryListService,
  TransectionHistoryButtonService
    >;

  constructor(
    private utilService: UtilService,
    private transectionHistoryListService: TransectionHistoryListService,
    private transectionHistoryButtonService: TransectionHistoryButtonService,
    public dialog: MatDialog,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.loadList(0,this.dataSize);
   }

   loadList(offset, limit) {
    // this.userService.getUsersTableData(offset,limit).subscribe((res) =>{
      //console.log('***',res.content);
       this.data = DataUtils.flattenData(this.ELEMENT_DATA);
       this.serviceListControlWrapper = new ServiceListControlWrapper<
       TransectionHistoryListService,
       TransectionHistoryButtonService
         >(
           this.transectionHistoryListService,
           this.data,
           this.transectionHistoryButtonService,
           10,
           {pageSize: 0,pageIndex: 0}
          //  res.totalElements,
          // { pageSize: res.size, pageIndex: res.number }
         );
    //  });
   }

  onPageChange(event) {
    this.loadList(event.pageInfo.offset, event.pageInfo.limit);
  }

  execute(event: { element: any; rawData?: any; method: string; service?: MasterListService; methodKey?: string }) {
   // console.log(event);
   switch (event.method) {
    //  case 'replay':
    //   this.replayDialog(event.element);
    //    break;
   }
 }


//  replayDialog(data) {
//   console.log(data);
//   const dialogRef = this.dialog.open(ReplayComponent, {
//     width: '800px',
//     height: '400px',
//     data: {
//        title:'replay',
//        obj: data
//     },
//   });
//   dialogRef.afterClosed().subscribe(result => {
//     console.log('The dialog was closed');
//   });
// }


}
