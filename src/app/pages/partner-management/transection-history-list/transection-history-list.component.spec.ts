import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TransectionHistoryListComponent } from './transection-history-list.component';

describe('TransectionHistoryListComponent', () => {
  let component: TransectionHistoryListComponent;
  let fixture: ComponentFixture<TransectionHistoryListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TransectionHistoryListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TransectionHistoryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
