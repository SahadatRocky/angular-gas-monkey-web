import { Injectable } from '@angular/core';
import { MasterListService } from '../../../../containers/core/master-component/master-service/master-list-service';
import { TextData } from '../../../shared/model/common/text-data';

@Injectable({
  providedIn: 'root'
})
export class TransectionHistoryListService extends MasterListService {
  constructor() {
    super();
    this.fieldInfo = {
      colWidth: 4,
      listTitleEn: 'Transection History List',
      listTitleBn: 'উজারের তালিকা',
      fields: [


        new TextData({
          key: 'paymentId',
          fieldWidth: 4,
          name: 'paymentId',
          id: 'paymentId',
          label: 'Payment Id',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 1
        }),
        new TextData({
          key: 'partnerId',
          fieldWidth: 4,
          name: 'partnerId',
          id: 'partnerId',
          label: 'Partner Id',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 2
        }),
        new TextData({
          key: 'customerId',
          fieldWidth: 4,
          name: 'customerId',
          id: 'customerId',
          label: 'Customer Id',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 3
        }),
        new TextData({
          key: 'paymentDate',
          fieldWidth: 4,
          name: 'paymentDate',
          id: 'paymentDate',
          label: 'Payment Date',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 4
        }),
        new TextData({
          key: 'amount',
          fieldWidth: 4,
          name: 'amount',
          id: 'amount',
          label: 'Amount',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 5
        }),
        new TextData({
          key: 'paymentType',
          fieldWidth: 4,
          name: 'paymentType',
          id: 'paymentType',
          label: 'Payment Type',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 6
        })
      ]
    };
  }
}
