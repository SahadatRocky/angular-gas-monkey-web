import { Injectable } from '@angular/core';
import { MasterButtonService } from '../../../../containers/core/master-component/master-service/master-button-service';

@Injectable({
  providedIn: 'root'
})
export class TransectionHistoryButtonService extends MasterButtonService {
  constructor() {
    super();
    this.buttonFields = [
      // new MasterButtonField({
      //   controlType: 'add',
      // })
    ];
  }
}
