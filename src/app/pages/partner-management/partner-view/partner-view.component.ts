import { Component, EventEmitter, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilService } from '../../shared/service/util.service';
import { PartnerService } from '../service/partner.service';
import { FileUploader } from 'ng2-file-upload';
import { ProductService } from '../../product-management/service/product.service';
import { DialogImageViewerComponent } from '../../shared/components/dialog-image-viewer/dialog-image-viewer.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-partner-view',
  templateUrl: './partner-view.component.html',
  styleUrls: ['./partner-view.component.scss']
})
export class PartnerViewComponent implements OnInit {
  partnerId:any;
  orderViewData:any;
  buttonText: string;
  partnerViewData:any;
  isApproved:boolean;
  verificationData:any;
  DocumentImageList = [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private partnerService: PartnerService,
    private productService: ProductService,
    private utilService: UtilService,
    public dialog: MatDialog

  ){}
  ngOnInit() {
    this.activatedRoute.params.subscribe(
      params => {
        this.partnerId = params.id;
        // console.log(this.storeId);
        this.buttonText = 'Approved';
      });

      this.getPartnerById();
  }

  getPartnerById(){
    //this.isShowLoading = true;
    this.partnerService.getPartnerById(this.partnerId).subscribe(res => {
      this.partnerViewData = res;
      console.log('=========================partner-view',res);
      this.isApproved = this.partnerViewData.customerInfoBean.approved;
      console.log('partner-approved-status====>>>>>',this.isApproved);
      if(this.isApproved){
        return this.verificationData='Verified';
      }else{
        return this.verificationData='Not Verified';
      }
    })
  }

  uploader: FileUploader = new FileUploader({
    //url: URL,
    disableMultipart: false,
    // autoUpload: true,
    method: "post",
    itemAlias: "attachment",
    allowedFileType: ["image", "video"],
  });

  onFileSelected(event: EventEmitter<File>) {
    const file: any = event;
    const title = file[0].name;
    const formData = new FormData();
    formData.append("file", file[0]);
    //console.log(formData.get("file"));
    this.productService.uploadProductImage(formData).subscribe((res) => {
      //console.log("----------------------img-response-------------", res);
      if (res.status == 200) {
        //console.log(title);
        let obj = {
          imageLink: res.body,
          imageLinkId: "",
          status: true,
          // brandId: this.brandId,
          title: title,
        };
        // this.productService.createBrandImage(obj).subscribe((res) => {
        //   console.log("Brand image created", res);
        //  // this.getBrandById();
        // });
      }
    });
  }

  onSubmit(){
    let obj = {
      // "id": this.brandId,
    };

    console.log(this.partnerId);
    this.partnerService.partnerApproved(this.partnerId, obj).subscribe(
      (res) => {
        //console.log(res);
        this.utilService.showSnackBarMessage(
          "Partner Approved Successfully",
          this.utilService.TYPE_MESSAGE.SUCCESS_TYPE
        );
        this.router.navigate(["/pages/partner-management/partners"]);
      },
      (error) => {
        this.utilService.showSnackBarMessage(
          error.message,
          this.utilService.TYPE_MESSAGE.ERROR_TYPE
        );
      }
    );
  }

  openImageDialog(event){
    console.log('=======================partner-view',event)
    const dialogRef = this.dialog.open(DialogImageViewerComponent, {

      // disableClose: true,
      width: '550px',
      height: '550px',
      data: {
        title: 'Image',
       // obj: event.attachmentLink
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('image closed');
    });

  }


  goToList(){
    this.router.navigate(['/pages/partner-management/partners']);
  }

}
