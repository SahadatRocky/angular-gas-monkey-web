import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UtilService } from '../../shared/service/util.service';
import { PartnerService } from '../service/partner.service';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {
  paymentInformationForm: FormGroup;
  partnerId:any;
  duePayment:any;
  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<PaymentComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private partnerService: PartnerService,
    private utilService: UtilService,
  ) { }

  ngOnInit(): void {
    console.log('****',this.data.obj['customerInfoBean.id']);
    this.partnerId = this.data.obj['customerInfoBean.id'];
    this.createPaymentInformationFormGroup();
    this.getDuePayment();
  }

  createPaymentInformationFormGroup(){
    this.paymentInformationForm = this.fb.group({
      note: ["", [Validators.required]],
      amount: ["", [Validators.required]]
    });
  }

  getDuePayment(){
      this.partnerService.getDuePayment(this.partnerId).subscribe(res=>{
        console.log(res.totalPayable);
        this.duePayment = res.totalPayable;
      });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit(){

    let obj = {
    "customerId": this.partnerId,
    "note":this.paymentInformationForm.value.note,
    "amount": this.paymentInformationForm.value.amount,
    };

    console.log(obj);

  this.partnerService.createPayment(obj).subscribe(
    (res) => {
      this.utilService.showSnackBarMessage(
        "Payment Created Successfully",
        this.utilService.TYPE_MESSAGE.SUCCESS_TYPE
      );
      this.dialogRef.close();
    },
    (error) => {
      this.utilService.showSnackBarMessage(
        error.message,
        this.utilService.TYPE_MESSAGE.ERROR_TYPE
      );
    }
  );
  }

  get note() {
    return this.paymentInformationForm.get("note");
  }
  get amount() {
    return this.paymentInformationForm.get("amount");
  }

}
