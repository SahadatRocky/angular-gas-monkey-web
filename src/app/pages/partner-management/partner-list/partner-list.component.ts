import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ServiceListControlWrapper } from '../../../containers/core/master-component/master-model/service-list-control-wrapper';
import { MasterListService } from '../../../containers/core/master-component/master-service/master-list-service';
import DataUtils from '../../shared/utils/data-utils';
import { PaymentComponent } from '../payment/payment.component';
import { PartnerService } from '../service/partner.service';
import { PartnerButtonService } from './service/partner-button-service';
import { PartnerListService } from './service/partner-list-service';

@Component({
  selector: 'app-partner-list',
  templateUrl: './partner-list.component.html',
  styleUrls: ['./partner-list.component.scss']
})

export class PartnerListComponent implements OnInit {

  // ELEMENT_DATA: any = [
  //   { name: 'Sahadat', locationName: 'Uttara', walletName: 'Bikas', mobile:'01711213148',depositBalance:15000, status: 'verified'}
  // ];

  data: any;
  isShowLoading = false;
  dataSource = new MatTableDataSource();
  dataSize = 10;
  serviceListControlWrapper: ServiceListControlWrapper<
    PartnerListService,
    PartnerButtonService
 >;
  constructor(private router: Router,
    private partnerListService: PartnerListService,
    private partnerButtonService: PartnerButtonService,
    private partnerService: PartnerService,
    public dialog: MatDialog) { }


    ngOnInit(): void {
      this.loadList(0,this.dataSize);
     }

     loadList(offset, limit) {
       this.partnerService.getPartnerListTableData(offset,limit).subscribe((res) =>{
        console.log(res.content);
        this.data = DataUtils.flattenData(res.content);

         this.serviceListControlWrapper = new ServiceListControlWrapper<
           PartnerListService,
           PartnerButtonService
           >(
             this.partnerListService,
             this.data,
             this.partnerButtonService,
             res.totalElements,
             { pageSize: res.size, pageIndex: res.number }
           );
       });
     }

     onPageChange(event) {
      this.loadList(event.pageInfo.offset, event.pageInfo.limit);
    }

    execute(event: { element: any; rawData?: any; method: string; service?: MasterListService; methodKey?: string }) {
    // console.log(event.element);
     switch (event.method) {
       case 'add':
         this.router.navigate(['/pages/partner-management/partner-edit/0']);
         break;
       case 'edit':
         break;
       case 'view':
         this.router.navigate(['/pages/partner-management/partner-view/'+ event.element['customerInfoBean.id']]);
         break;
       case "payment":
         this.paymentDialog(event.element);
         break;
     }
   }


   paymentDialog(data) {
    console.log(data);
    const dialogRef = this.dialog.open(PaymentComponent, {
      width: '700px',
      height: '400px',
      data: {
         title:'customer-payment-data',
         obj: data
      },
    });
    dialogRef.afterClosed().subscribe(result => {
      this.loadList(0,this.dataSize);
      console.log('The dialog was closed');
    });
  }
}
