import {Injectable} from '@angular/core';
import { MasterListService } from '../../../../containers/core/master-component/master-service/master-list-service';
import { TextData } from '../../../shared/model/common/text-data';

@Injectable({
  providedIn: 'root'
})
export class PartnerListService extends MasterListService {
  constructor() {
    super();
    this.fieldInfo = {
      colWidth: 4,
      listTitleEn: 'Partner',
      listTitleBn: 'তালিকা',
      fields: [
        new TextData({
          key: 'customerInfoBean.customerName',
          fieldWidth: 4,
          name: 'customerName',
          id: 'customerName',
          label: 'Name',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 1
        }),
        new TextData({
          key: 'customerInfoBean.photoLink',
          fieldWidth: 4,
          name: 'photoLink',
          id: 'photoLink',
          label: 'Photo',
          type: 'img',
          canShow: true,
          canSort: true,
          order: 2
        }),
        new TextData({
          key: 'workingZone',
          fieldWidth: 4,
          name: 'locationName',
          id: 'locationName',
          label: 'Location Name',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 3
        }),
        new TextData({
          key: 'mobileBankName',
          fieldWidth: 4,
          name: 'walletName',
          id: 'walletName',
          label: 'Wallet Name',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 4
        }),
        new TextData({
          key: 'customerInfoBean.phoneNo',
          fieldWidth: 4,
          name: 'phoneNo',
          id: 'phoneNo',
          label: 'Mobile',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 5
        }),
        new TextData({
          key: 'depositBalance',
          fieldWidth: 4,
          name: 'depositBalance',
          id: 'depositBalance',
          label: 'Deposit Balance',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 6
        }),
        new TextData({
          key: 'approved',
          fieldWidth: 4,
          name: 'approved',
          id: 'approved',
          label: 'Approval Status',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 7
        }),
        new TextData({
          key: 'action',
          fieldWidth: 4,
          name: 'অ্যাকশন',
          id: 'action',
          label: 'Action',
          // type: 'text',
          canShow: true,
          canSort: true,
          order: 8
        })
      ]
    };
  }
}
