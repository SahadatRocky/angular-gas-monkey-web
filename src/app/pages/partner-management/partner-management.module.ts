import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PartnerManagementRoutingModule } from './partner-management-routing.module';
import { PartnerComponent } from './partner/partner.component';
import { PartnerListComponent } from './partner-list/partner-list.component';
import { SharedModule } from '../shared/shared.module';
import { PartnerEditComponent } from './partner-edit/partner-edit.component';
import { PartnerViewComponent } from './partner-view/partner-view.component';
import { TransectionHistoryListComponent } from './transection-history-list/transection-history-list.component';
import { PaymentComponent } from './payment/payment.component';


@NgModule({
  declarations: [
    PartnerComponent,
    PartnerListComponent,
    PartnerEditComponent,
    PartnerViewComponent,
    TransectionHistoryListComponent,
    PaymentComponent
  ],
  imports: [
    CommonModule,
    PartnerManagementRoutingModule,
    SharedModule
  ],
  entryComponents: [
    PaymentComponent
    ]
})
export class PartnerManagementModule { }
