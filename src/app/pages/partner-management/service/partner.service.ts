import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { ApiEndpoints } from "../../../api-endpoints";


@Injectable({
  providedIn: 'root'
})
export class PartnerService {

  HTTPOptions:Object = {
    observe: 'response',
    responseType: 'text'
 }

  private apiEndpoints: ApiEndpoints = new ApiEndpoints();
  constructor(private http: HttpClient) { }

  getPartnerListTableData(page: number, size: number): Observable<any> {
    let url = this.apiEndpoints.PARTNER_URL;
    const fullUrl = `${url}?page=${page}&size=${size}`;
    return this.http.get<any>(fullUrl).pipe(
      map(value => value)
      );
  }

  getPartnerById(id:any): Observable<any>{
    let url = this.apiEndpoints.PARTNER_URL + '/' + id;
    return this.http.get<any>(url).pipe(
      map(value => value)
      );
  }

  partnerApproved(id:any, obj:any): Observable<any>{
    let url = this.apiEndpoints.PARTNER_APPROVED_BASE_URL + '/' + id;
    return this.http.put<any>(url,obj).pipe(
      map(value => value)
    );
  }


  getMobileWalletListDropdown(): Observable<any> {
    let url = this.apiEndpoints.MOBILE_BANKING_DROPDOWN_URL;
    return this.http.get<any>(url).pipe(
      map(value => value)
      );
  }


  createParner(Obj: any): Observable<any>{
    let url = this.apiEndpoints.PARTNER_URL;
    return this.http.post<any>(url, Obj).pipe(map(value => value))
  }


  getDuePayment(id:any): Observable<any>{
    let url = this.apiEndpoints.PARTNER_PAYMENT_URL + '/' + id;
    return this.http.get<any>(url).pipe(
      map(value => value)
      );
  }

  createPayment(Obj: any): Observable<any>{
    let url = this.apiEndpoints.TRANSECTION_PAYMENT_URL;
    return this.http.post<any>(url, Obj).pipe(map(value => value))
  }

}
