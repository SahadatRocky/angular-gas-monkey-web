import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PartnerEditComponent } from './partner-edit/partner-edit.component';
import { PartnerListComponent } from './partner-list/partner-list.component';
import { PartnerViewComponent } from './partner-view/partner-view.component';
import { PartnerComponent } from './partner/partner.component';
import { TransectionHistoryListComponent } from './transection-history-list/transection-history-list.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'partner-management'
    },
    children: [
      {
        path: '',
        redirectTo: 'partners'
      },
      {
        path: 'partners',
        component: PartnerComponent,
      },
      {
        path: 'partner-list',
        component: PartnerListComponent,
      },
      {
        path: 'partner-edit/:id',
        component: PartnerEditComponent
      },
      {
        path: 'partner-view/:id',
        component: PartnerViewComponent
      },
      {
        path: 'transection-history-list',
        component: TransectionHistoryListComponent,
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PartnerManagementRoutingModule { }
