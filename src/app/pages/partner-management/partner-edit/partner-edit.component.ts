import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { OBJ_EDIT, OBJ_NEW } from '../../../constants/gas-monkey-constants';
import { FileUploader } from 'ng2-file-upload';
import { UtilService } from '../../shared/service/util.service';
import { PartnerService } from '../service/partner.service';
import { OrderService } from '../../order-management/service/order.service';
import { DealerService } from '../../dealer-management/service/dealer.service';

@Component({
  selector: 'app-partner-edit',
  templateUrl: './partner-edit.component.html',
  styleUrls: ['./partner-edit.component.scss']
})
export class PartnerEditComponent implements OnInit {

  @Input() selectedPartnerObj: any;
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$";
  partnerForm: FormGroup;
  customerAddressForm: FormGroup;
  walletInformationForm: FormGroup;
  nomineInformationForm: FormGroup;
  documentInformationForm: FormGroup;
  buttonText: string;
  dealerId: any;
  actionMode: string;
  file: File[];
  selectedFiles: any;
  districtList: any;
  thanaList: any;
  clusterList: any;
  BrandList:any;
  attachmentList:any;
  fileUploadUrl:any;
  mobileWalletList:any;

  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private dealerService: DealerService,
    private utilService: UtilService,
    private formBuilder: FormBuilder,
    private partnerService: PartnerService
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      this.dealerId = params.id;
      this.buttonText = this.dealerId != "0" ? "Approved" : "SAVE";
      this.actionMode = this.dealerId != "0" ? OBJ_EDIT : OBJ_NEW;
    });
    this.createPartnerFormGroup();
    this.customerAddressGroup();
    this.walletInformationGroup();
    this.nomineInformationGroup();
    this.documentInformationGroup();
    this.getCustomerDistrictList();
    this.generateBrandDropdownListData();
    this.getAttachmentTypeDropdownListData();
    this.getMobileWalletListDropdown();
    if (this.actionMode == OBJ_EDIT) {

    } else {

    }
  }
  // [Validators.required, Validators.pattern(this.emailPattern)]
  createPartnerFormGroup() {
    this.partnerForm = this.fb.group({
      customerName: ["", [Validators.required]],
      phoneNo: ["",  [Validators.required, Validators.pattern("[0-9 ]{11}")]],
      emailAddress: ["",[Validators.required, Validators.pattern(this.emailPattern)]],
      status: [false],
    });
  }

  customerAddressGroup() {
    this.customerAddressForm = this.fb.group({
      districtId: [""],
      thanaId: [""],
      clusterId: [""],
      area: ["", [Validators.required]],
      latitude: ["", [Validators.required]],
      longitude: ["", [Validators.required]],
      workingZone:["", [Validators.required]],
      address: ["", [Validators.required]],
    });
  }

  walletInformationGroup() {
    this.walletInformationForm = this.fb.group({
      mobileBankId: ["", [Validators.required]],
      mobileWalletNo: ["",  [Validators.required]]
    });
  }

  nomineInformationGroup() {
    this.nomineInformationForm = this.fb.group({
      nomineeName: ["",[Validators.required]],
      nomineePhone: ["",[Validators.required]],
      nomineeRelationShip: ["",[Validators.required]],
    });
  }

  documentInformationGroup() {
    this.documentInformationForm = this.fb.group({
      documentTypeId: [""],
    });
  }

  uploader: FileUploader = new FileUploader({
    disableMultipart: false,
    method: "post",
    itemAlias: "attachment",
    allowedFileType: ["image", "video"],
  });

  onFileSelected(event: EventEmitter<File[]>) {
    const file: any = event;
    const formData = new FormData();
    formData.append('file', file[0]);

    this.dealerService.fileUpload(formData).subscribe(res=>{
      console.log('----------------------file-upload-------------',res);
      this.fileUploadUrl = res.path;
      console.log('---------------------this.fileUploadUrl-------------',this.fileUploadUrl);
      this.utilService.showSnackBarMessage('File Uploaded Successfully',this.utilService.TYPE_MESSAGE.SUCCESS_TYPE);
      });


  }

  getCustomerDistrictList() {
    this.dealerService.getCustomerDistrictDropList().subscribe((res) => {
      this.districtList = res;
    });
  }

  getCustomerThanaListByDistrictId(id: any) {
    this.dealerService.getCustomerThanaDropList(id).subscribe((res) => {
      this.thanaList = res;
    });
  }

  getCustomerClusterListByThanaId(id: any) {
    this.dealerService.getCustomerClusterDropList(id).subscribe((res) => {
      this.clusterList = res;
    });
  }


  getMobileWalletListDropdown(){
    this.partnerService.getMobileWalletListDropdown().subscribe(res=>{
      console.log('res---',res);
        this.mobileWalletList = res;
    });
  }

  onDistrictChange(event) {
    if (event.value == "") {
      this.customerAddressForm.get("customerThanaId").setValue("");
      this.customerAddressForm.get("customerClusterId").setValue("");
    } else {
      this.getCustomerThanaListByDistrictId(event.value);
    }
  }

  onThanaChange(event) {
    if (event.value == "") {
      this.customerAddressForm.get("customerThanaId").setValue("");
    } else {
      this.getCustomerClusterListByThanaId(event.value);
    }
  }



  generateBrandDropdownListData(){
    this.dealerService.getBrandDropDownList().subscribe(res=>{
      console.log('BrandDropList-dropdownlist-',res);
      this.BrandList=res;

    });
  }

  getAttachmentTypeDropdownListData(){
    this.dealerService.getAttachmentTypeDropList().subscribe(res=>{
      console.log('Attachment-dropdownlist-',res);
      this.attachmentList=res;
    });
  }

  onChange(event:any){
    //  console.log(event.source.value);
    // const brandNames = <FormArray>this.brandInformationForm.get('brandName') as FormArray;

    // if(event.checked) {
    //   brandNames.push(new FormControl(event.source.value))
    // } else {
    //   const i = brandNames.controls.findIndex(x => x.value === event.source.value);
    //   brandNames.removeAt(i);
    // }
  }

  onDocumentChange(event:any){
    console.log(event);
  }

  onSubmit() {
    if (this.actionMode == OBJ_EDIT) {


    } else {
      //console.log(this.brandInformationForm.value.brandName);
      let obj = {
        "customerName": this.partnerForm.value.customerName,
        "emailAddress": this.partnerForm.value.emailAddress,
        "phoneNo": this.partnerForm.value.phoneNo,
        "status": true,
        "districtId": this.customerAddressForm.value.districtId,
        "thanaId": this.customerAddressForm.value.thanaId,
        "clusterId": this.customerAddressForm.value.clusterId,
        "area": this.customerAddressForm.value.area,
        "address": this.customerAddressForm.value.address,
        "latitude": this.customerAddressForm.value.latitude,
        "longitude": this.customerAddressForm.value.longitude,
        "password": "12345678",
        "confirmPassword": "12345678",
        "workingZone" : this.customerAddressForm.value.workingZone,
        "mobileBankId": this.walletInformationForm.value.mobileBankId,
        "mobileWalletNo": this.walletInformationForm.value.mobileWalletNo,
        "nomineeName": this.nomineInformationForm.value.nomineeName,
        "nomineePhone": this.nomineInformationForm.value.nomineePhone,
        "nomineeRelationShip": this.nomineInformationForm.value.nomineeRelationShip,
        "attachments":[
          {
              "attachmentTypeId":this.documentInformationForm.value.documentTypeId,
              "attachmentLink": this.fileUploadUrl
          }
      ]
      };

      console.log(obj);

      this.partnerService.createParner(obj).subscribe(
        (res) => {
          this.utilService.showSnackBarMessage(
            "Partner Created Successfully",
            this.utilService.TYPE_MESSAGE.SUCCESS_TYPE
          );
          this.router.navigate(["/pages/partner-management/partners"]);
        },
        (error) => {
          this.utilService.showSnackBarMessage(
            error.message,
            this.utilService.TYPE_MESSAGE.ERROR_TYPE
          );
        }
      );
    }
  }

  goToList() {
    this.router.navigate(["/pages/partner-management/partners"]);
  }


  checkDisable() {
    return !(this.customerAddressForm.valid && this.partnerForm.valid && this.walletInformationForm.valid && this.nomineInformationForm.valid);
  }

  //partner
  get customerName() {
    return this.partnerForm.get("customerName");
  }
  get emailAddress() {
    return this.partnerForm.get("emailAddress");
  }
  get phoneNo() {
    return this.partnerForm.get("phoneNo");
  }

  get shopName() {
    return this.partnerForm.get("shopName");
  }

  ///customerAddressForm

  get area() {
    return this.customerAddressForm.get("area");
  }

  get latitude() {
    return this.customerAddressForm.get("latitude");
  }

  get longitude() {
    return this.customerAddressForm.get("longitude");
  }

  get workingZone(){
    return this.customerAddressForm.get("workingZone");
  }

  get address() {
    return this.customerAddressForm.get("address");
  }

  //nomineInformationForm
  get nomineeName() {
    return this.nomineInformationForm.get("nomineeName");
  }
  get nomineePhone() {
    return this.nomineInformationForm.get("nomineePhone");
  }

  get nomineeRelationShip(){
    return this.nomineInformationForm.get("nomineeRelationShip");
  }

  get mobileBankId() {
    return this.walletInformationForm.get("mobileBankId");
  }

  get mobileWalletNo(){
    return this.walletInformationForm.get("mobileWalletNo");
  }

}

