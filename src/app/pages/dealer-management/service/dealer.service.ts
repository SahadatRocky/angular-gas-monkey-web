import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { ApiEndpoints } from "../../../api-endpoints";


@Injectable({
  providedIn: 'root'
})
export class DealerService {

  HTTPOptions:Object = {
    observe: 'response',
    responseType: 'text'
 }

  private apiEndpoints: ApiEndpoints = new ApiEndpoints();
  constructor(private http: HttpClient) { }

  getDealerListTableData(page: number, size: number): Observable<any> {
    let url = this.apiEndpoints.DEALER_URL;
    const fullUrl = `${url}?page=${page}&size=${size}`;
    return this.http.get<any>(fullUrl).pipe(
      map(value => value)
      );
  }


  getDealerById(id:any): Observable<any>{
    let url = this.apiEndpoints.DEALER_URL + '/' + id;
    return this.http.get<any>(url).pipe(
      map(value => value)
      );
  }

  dealerApproved(id:any, obj:any): Observable<any>{
    let url = this.apiEndpoints.Dealer_APPROVED_BASE_URL + '/' + id;
    return this.http.put<any>(url,obj).pipe(
      map(value => value)
    );
  }

  getCustomerDistrictDropList(): Observable<any> {
    let url = this.apiEndpoints.CUSTOMER_DISTRICT_DROPDOWN_LIST;
    return this.http.get<any>(url).pipe(
      map(value => value)
    );
  }

  getCustomerThanaDropList(districtId:any): Observable<any> {
    let url = this.apiEndpoints.CUSTOMER_THANA_DROPDOWN_LIST + '/' + districtId;
    return this.http.get<any>(url).pipe(
      map(value => value)
    );
  }

  getCustomerClusterDropList(thanaId:any): Observable<any> {
    let url = this.apiEndpoints.CUSTOMER_CLUSTER_DROPDOWN_LIST + '/' + thanaId;
    return this.http.get<any>(url).pipe(
      map(value => value)
    );
  }

  getAttachmentTypeDropList(): Observable<any> {
    let url = this.apiEndpoints.ATTACHMENT_TYPE_DROPDOWN_LIST ;
    return this.http.get<any>(url).pipe(
      map(value => value)
    );
  }


  getBrandDropDownList(){
    let url = this.apiEndpoints.BRAND_BASE_URL+this.apiEndpoints.DROPDOWN_LIST_API_URL;
    return this.http.get<any>(url).pipe(
      map(value => value)
    );
  }

  createDealer(Obj: any): Observable<any>{
    let url = this.apiEndpoints.DEALER_URL;
    return this.http.post<any>(url, Obj).pipe(map(value => value))
  }

  // file upload
  fileUpload(formData: any): Observable<any>{
    let url = this.apiEndpoints.FILE_UPLOAD_URL;
    return this.http.post<any>(url, formData).pipe(map(value => value))
  }



}
