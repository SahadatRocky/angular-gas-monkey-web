import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DealerManagementRoutingModule } from './dealer-management-routing.module';
import { DealerComponent } from './dealer/dealer.component';
import { DealerListComponent } from './dealer-list/dealer-list.component';
import { SharedModule } from '../shared/shared.module';
import { DealerEditComponent } from './dealer-edit/dealer-edit.component';
import { DealerViewComponent } from './dealer-view/dealer-view.component';


@NgModule({
  declarations: [
    DealerComponent,
    DealerListComponent,
    DealerEditComponent,
    DealerViewComponent
  ],
  imports: [
    CommonModule,
    DealerManagementRoutingModule,
    SharedModule
  ]
})
export class DealerManagementModule { }
