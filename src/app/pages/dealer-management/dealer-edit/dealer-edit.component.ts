
import { Component, EventEmitter, Input, OnInit } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { OBJ_EDIT, OBJ_NEW } from "../../../constants/gas-monkey-constants";
import { FileUploader } from "ng2-file-upload";
import { UtilService } from "../../shared/service/util.service";
import { DealerService } from "../service/dealer.service";

@Component({
  selector: "app-dealer-edit",
  templateUrl: "./dealer-edit.component.html",
  styleUrls: ["./dealer-edit.component.scss"],
})
export class DealerEditComponent implements OnInit {
  @Input() selectedDealerObj: any;
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$";
  dealerForm: FormGroup;
  customerAddressForm: FormGroup;
  brandInformationForm: FormGroup;
  nomineInformationForm: FormGroup;
  documentInformationForm: FormGroup;
  buttonText: string;
  dealerId: any;
  actionMode: string;
  file: File[];
  selectedFiles: any;
  districtList: any;
  thanaList: any;
  clusterList: any;
  BrandList:any;
  attachmentList:any;
  fileUploadUrl:any;

  // floorList: any = [
  //   { id: "1", value: "Level-1" },
  //   { id: "2", value: "Level-2" },
  //   { id: "3", value: "Level-3" },
  //   { id: "4", value: "Level-4" },
  //   { id: "5", value: "Level-5" },
  //   { id: "6", value: "Level-6" },
  //   { id: "7", value: "Level-7" },
  //   { id: "8", value: "Level-8" },
  //   { id: "9", value: "Level-9" },
  //   { id: "10", value: "Level-10" },
  //   { id: "11", value: "Level-11" },
  //   { id: "12", value: "Level-12" },
  //   { id: "13", value: "Level-13" },
  //   { id: "14", value: "Level-14" },
  //   { id: "15", value: "Level-15" },
  //   { id: "16", value: "Level-16" },
  //   { id: "17", value: "Level-17" },
  //   { id: "18", value: "Level-18" },
  //   { id: "19", value: "Level-19" },
  //   { id: "20", value: "Level-20" },
  // ];

  // ELEMENT_DATA = {
  //   firstNameEng: "Sahadat",
  //   firstNameBan: "সাহাদাত",
  //   lastNameEng: "Hossain",
  //   lastNameBan: "হোসেন",
  //   latitute: "23.346656",
  //   longitude: "90.56785",
  //   lift: true,
  //   floorId: "5",
  //   phoneNumber: "01686953176",
  //   email: "sahadat@batworld.com",
  //   address: "Uttara, Sector-13, Road-13, Dhaka",
  //   area: "Sector-13",
  //   thanaId: "1",
  //   districtId: "1",
  //   company: "Gas monkey",
  //   customerTypeId: "1",
  // };

  // CUSTOMER_ADDRESS_DATA = {
  //   customerLongitude: "23.346656",
  //   customerLatitute: "90.56785",
  //   customerArea: "Uttara, Sector-13, Road-13, Dhaka",
  //   customerHouse: "House-41",
  //   customerThanaId: "1",
  //   customerDistrictId: "1",
  //   customerPeferedWaitingArea: "Uttara, Sector-13, Road-13, Dhaka",
  // };

  // WALLET_DATA = {
  //   mobileBankingName: "1",
  //   walletNumber: "01811217231",
  // };

  // NOMINE_DATA = {
  //   nomineName: "Sahadat",
  //   nominePhoneNumber: "01811217231",
  //   nomineRelationShip: "Brother",
  // };

  // DOCUMENT_DATA = {
  //   documentTypeId: "1",
  // };

  // VERIFICATION_DATA = {
  //   verificationNote: "Delivery before 12pm.",
  //   verificationStatus: "1",
  // };

  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private dealerService: DealerService,
    private utilService: UtilService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      this.dealerId = params.id;
      this.buttonText = this.dealerId != "0" ? "Approved" : "SAVE";
      this.actionMode = this.dealerId != "0" ? OBJ_EDIT : OBJ_NEW;
    });
    this.createDealerFormGroup();
    this.customerAddressGroup();
    this.brandInformationGroup();
    this.nomineInformationGroup();
    this.documentInformationGroup();
    this.getCustomerDistrictList();
    this.generateBrandDropdownListData();
    this.getAttachmentTypeDropdownListData();

    if (this.actionMode == OBJ_EDIT) {

    } else {

    }
  }
  // [Validators.required, Validators.pattern(this.emailPattern)]
  createDealerFormGroup() {
    this.dealerForm = this.fb.group({
      customerName: ["", [Validators.required]],
      phoneNo: ["",  [Validators.required, Validators.pattern("[0-9 ]{11}")]],
      shopName: ["", [Validators.required]],
      emailAddress: ["",[Validators.required, Validators.pattern(this.emailPattern)]],
      status: [false],
    });
  }

  customerAddressGroup() {
    this.customerAddressForm = this.fb.group({
      districtId: [""],
      thanaId: [""],
      clusterId: [""],
      area: ["", [Validators.required]],
      latitude: ["", [Validators.required]],
      longitude: ["", [Validators.required]],
      address: ["", [Validators.required]],
    });
  }

  brandInformationGroup() {
    this.brandInformationForm = this.fb.group({
      brandName: this.formBuilder.array([])

    });
  }

  nomineInformationGroup() {
    this.nomineInformationForm = this.fb.group({
      keyContactName: ["",[Validators.required]],
      keyContactPhone: ["",[Validators.required]],
    });
  }

  documentInformationGroup() {
    this.documentInformationForm = this.fb.group({
      documentTypeId: [""],
    });
  }

  uploader: FileUploader = new FileUploader({
    disableMultipart: false,
    method: "post",
    itemAlias: "attachment",
    allowedFileType: ["image", "video"],
  });

  onFileSelected(event: EventEmitter<File[]>) {
    const file: any = event;
    const formData = new FormData();
    formData.append('file', file[0]);
    //console.log(formData.get('file'));

    this.dealerService.fileUpload(formData).subscribe(res=>{
      console.log('----------------------file-upload-------------',res);
      this.fileUploadUrl = res.path;
      console.log('---------------------this.fileUploadUrl-------------',this.fileUploadUrl);
      this.utilService.showSnackBarMessage('File Uploaded Successfully',this.utilService.TYPE_MESSAGE.SUCCESS_TYPE);
      });


  }


  // getPartnerById() {
  //   this.populateData(this.ELEMENT_DATA);
  //   // this.isShowLoading = true;
  //   // this.productService.getProductCategoryById(this.productCategoryId).subscribe(res => {
  //   //   console.log(res);
  //   //   this.isShowLoading = false;
  //   //   this.populateData(res);
  //   // })
  // }

  // populateData(myData) {
  //   this.dealerForm.patchValue(myData);
  //   // this.userForm.get('languageId').setValue(myData.languageBean.id);
  //   // this.userForm.get('groupId').setValue(myData.groupInfoBean.id);
  //   // this.userForm.controls['email'].disable();
  // }

  // getCustomerAddressById() {
  //   this.populateCustomerAddressData(this.CUSTOMER_ADDRESS_DATA);
  //   // this.isShowLoading = true;
  //   // this.productService.getProductCategoryById(this.productCategoryId).subscribe(res => {
  //   //   console.log(res);
  //   //   this.isShowLoading = false;
  //   //   this.populateData(res);
  //   // })
  // }

  // populateCustomerAddressData(myData) {
  //   this.customerAddressForm.patchValue(myData);
  //   // this.userForm.get('languageId').setValue(myData.languageBean.id);
  //   // this.userForm.get('groupId').setValue(myData.groupInfoBean.id);
  //   // this.userForm.controls['email'].disable();
  // }


  // populateBrandData(myData) {
  //   //this.beandInformationForm.patchValue(myData);
  //   // this.userForm.get('languageId').setValue(myData.languageBean.id);
  //   // this.userForm.get('groupId').setValue(myData.groupInfoBean.id);
  //   // this.userForm.controls['email'].disable();
  // }

  // getNomineInfoById() {
  //   this.populateNominetData(this.NOMINE_DATA);
  //   // this.isShowLoading = true;
  //   // this.productService.getProductCategoryById(this.productCategoryId).subscribe(res => {
  //   //   console.log(res);
  //   //   this.isShowLoading = false;
  //   //   this.populateData(res);
  //   // })
  // }

  // populateNominetData(myData) {
  //   this.nomineInformationForm.patchValue(myData);
  //   // this.userForm.get('languageId').setValue(myData.languageBean.id);
  //   // this.userForm.get('groupId').setValue(myData.groupInfoBean.id);
  //   // this.userForm.controls['email'].disable();
  // }

  // getDocumentInfoById() {
  //   this.populateDocumentData(this.DOCUMENT_DATA);
  //   // this.isShowLoading = true;
  //   // this.productService.getProductCategoryById(this.productCategoryId).subscribe(res => {
  //   //   console.log(res);
  //   //   this.isShowLoading = false;
  //   //   this.populateData(res);
  //   // })
  // }

  // populateDocumentData(myData) {
  //   this.documentInformationForm.patchValue(myData);
  //   // this.userForm.get('languageId').setValue(myData.languageBean.id);
  //   // this.userForm.get('groupId').setValue(myData.groupInfoBean.id);
  //   // this.userForm.controls['email'].disable();
  // }


  getCustomerDistrictList() {
    this.dealerService.getCustomerDistrictDropList().subscribe((res) => {
      this.districtList = res;
    });
  }

  getCustomerThanaListByDistrictId(id: any) {
    this.dealerService.getCustomerThanaDropList(id).subscribe((res) => {
      this.thanaList = res;
    });
  }

  getCustomerClusterListByThanaId(id: any) {
    this.dealerService.getCustomerClusterDropList(id).subscribe((res) => {
      this.clusterList = res;
    });
  }

  onDistrictChange(event) {
    if (event.value == "") {
      this.customerAddressForm.get("customerThanaId").setValue("");
      this.customerAddressForm.get("customerClusterId").setValue("");
    } else {
      this.getCustomerThanaListByDistrictId(event.value);
    }
  }

  onThanaChange(event) {
    if (event.value == "") {
      this.customerAddressForm.get("customerThanaId").setValue("");
    } else {
      this.getCustomerClusterListByThanaId(event.value);
    }
  }

  generateBrandDropdownListData(){
    this.dealerService.getBrandDropDownList().subscribe(res=>{
      console.log('BrandDropList-dropdownlist-',res);
      this.BrandList=res;

    });
  }

  getAttachmentTypeDropdownListData(){
    this.dealerService.getAttachmentTypeDropList().subscribe(res=>{
      console.log('Attachment-dropdownlist-',res);
      this.attachmentList=res;
    });
  }

  onChange(event:any){
     console.log(event.source.value);
    const brandNames = <FormArray>this.brandInformationForm.get('brandName') as FormArray;

    if(event.checked) {
      brandNames.push(new FormControl(event.source.value))
    } else {
      const i = brandNames.controls.findIndex(x => x.value === event.source.value);
      brandNames.removeAt(i);
    }
  }

  onDocumentChange(event:any){
    console.log(event);

  }

  onSubmit() {
    if (this.actionMode == OBJ_EDIT) {

    } else {

      console.log(this.brandInformationForm.value.brandName);

      let obj = {
        "customerName": this.dealerForm.value.customerName,
        "emailAddress": this.dealerForm.value.emailAddress,
        "phoneNo": this.dealerForm.value.phoneNo,
        "shopName": this.dealerForm.value.shopName,
        "status": true,
        "districtId": this.customerAddressForm.value.districtId,
        "thanaId": this.customerAddressForm.value.thanaId,
        "clusterId": this.customerAddressForm.value.clusterId,
        "area": this.customerAddressForm.value.area,
        "address": this.customerAddressForm.value.address,
        "latitude": this.customerAddressForm.value.latitude,
        "longitude": this.customerAddressForm.value.longitude,
        "password": "12345678",
        "confirmPassword": "12345678",
        "keyContactName": this.nomineInformationForm.value.keyContactName,
        "keyContactPhone": this.nomineInformationForm.value.keyContactPhone,
        "brandIds":this.brandInformationForm.value.brandName,
        "attachments":[
          {
              "attachmentTypeId":this.documentInformationForm.value.documentTypeId,
              "attachmentLink": this.fileUploadUrl
          }
      ]
      };

      console.log(obj);

      this.dealerService.createDealer(obj).subscribe(
        (res) => {
          this.utilService.showSnackBarMessage(
            "Dealer Created Successfully",
            this.utilService.TYPE_MESSAGE.SUCCESS_TYPE
          );
          this.router.navigate(["/pages/dealer-management/dealers"]);
        },
        (error) => {
          this.utilService.showSnackBarMessage(
            error.message,
            this.utilService.TYPE_MESSAGE.ERROR_TYPE
          );
        }
      );
    }
  }

  goToList() {
    this.router.navigate(["/pages/dealer-management/dealers"]);
  }


  checkDisable() {
    return !(this.customerAddressForm.valid && this.dealerForm.valid && this.brandInformationForm.valid && this.nomineInformationForm.valid);
  }

  //partner
  get customerName() {
    return this.dealerForm.get("customerName");
  }
  get emailAddress() {
    return this.dealerForm.get("emailAddress");
  }
  get phoneNo() {
    return this.dealerForm.get("phoneNo");
  }

  get shopName() {
    return this.dealerForm.get("shopName");
  }

  ///customerAddressForm

  get area() {
    return this.customerAddressForm.get("area");
  }

  get latitude() {
    return this.customerAddressForm.get("latitude");
  }

  get longitude() {
    return this.customerAddressForm.get("longitude");
  }


  get address() {
    return this.customerAddressForm.get("address");
  }

  //nomineInformationForm
  get keyContactName() {
    return this.nomineInformationForm.get("keyContactName");
  }
  get keyContactPhone() {
    return this.nomineInformationForm.get("keyContactPhone");
  }

}
