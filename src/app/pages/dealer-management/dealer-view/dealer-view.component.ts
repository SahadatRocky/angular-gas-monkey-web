import { Component, EventEmitter, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilService } from '../../shared/service/util.service';
import { FileUploader } from 'ng2-file-upload';
import { ProductService } from '../../product-management/service/product.service';
import { DealerService } from '../service/dealer.service';
import { MatDialog } from '@angular/material/dialog';
import { DialogImageViewerComponent } from '../../shared/components/dialog-image-viewer/dialog-image-viewer.component';

@Component({
  selector: 'app-dealer-view',
  templateUrl: './dealer-view.component.html',
  styleUrls: ['./dealer-view.component.scss']
})
export class DealerViewComponent implements OnInit {
  dealerId:any;
  orderViewData:any;
  buttonText: string;
  dealerViewData:any;
  isApproved:boolean;
  verificationData:any;
  DocumentImageList = [];
  attachmentBeanArray=[];

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private dealerService: DealerService,
    private productService: ProductService,
    private utilService: UtilService,
    public dialog: MatDialog

  ){}
  ngOnInit() {
    this.activatedRoute.params.subscribe(
      params => {
        this.dealerId = params.id;
        // console.log(this.storeId);
        this.buttonText = 'Approved';
      });

      this.getDealerById();

  }


  getDealerById(){
    this.dealerService.getDealerById(this.dealerId).subscribe(res => {
      this.dealerViewData = res;
      console.log("=======>>>>>>>>>>",this.dealerViewData);

       this.attachmentBeanArray = this.dealerViewData.attachmentBeans;
      //  this.dealerViewData.attachmentBeans[0].attachmentTypeBean.attachmentName
      console.log("=======>>>>>>>>>>",this.attachmentBeanArray);

      this.isApproved = this.dealerViewData.customerInfoBean.approved;
      //console.log('dealer-apprived-status====>>>>>',this.isApproved);
      //this.verificationStatus=this.dealerViewData.customerInfoBean.approved;
      if(this.isApproved){
        return this.verificationData='Verified';
      }else{
        return this.verificationData='Not Verified';
      }
    })
  }



  uploader: FileUploader = new FileUploader({
    //url: URL,
    disableMultipart: false,
    // autoUpload: true,
    method: "post",
    itemAlias: "attachment",
    allowedFileType: ["image", "video"],
  });

  onFileSelected(event: EventEmitter<File>) {
    const file: any = event;
    const title = file[0].name;
    const formData = new FormData();
    formData.append("file", file[0]);
    //console.log(formData.get("file"));
    this.productService.uploadProductImage(formData).subscribe((res) => {
      //console.log("----------------------img-response-------------", res);
      if (res.status == 200) {
        //console.log(title);
        let obj = {
          imageLink: res.body,
          imageLinkId: "",
          status: true,
          // brandId: this.brandId,
          title: title,
        };
        // this.productService.createBrandImage(obj).subscribe((res) => {
        //   console.log("Brand image created", res);
        //  // this.getBrandById();
        // });
      }
    });
  }

  onSubmit(){
    let obj = {
      // "id": this.brandId,
    };

    console.log(this.dealerId);
    this.dealerService.dealerApproved(this.dealerId, obj).subscribe(
      (res) => {
        //console.log(res);
        this.utilService.showSnackBarMessage(
          "Dealer Approved Successfully",
          this.utilService.TYPE_MESSAGE.SUCCESS_TYPE
        );
        this.router.navigate(["/pages/dealer-management/dealers"]);
      },
      (error) => {
        this.utilService.showSnackBarMessage(
          error.message,
          this.utilService.TYPE_MESSAGE.ERROR_TYPE
        );
      }
    );
  }

  openImageDialog(event){
    console.log(event)
    const dialogRef = this.dialog.open(DialogImageViewerComponent, {

      // disableClose: true,
      width: '550px',
      height: '550px',
      data: {
        title: 'Image',
        obj: event.attachmentLink
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('image closed');
    });

  }

  goToList(){
    this.router.navigate(['/pages/dealer-management/dealers']);
  }


}
