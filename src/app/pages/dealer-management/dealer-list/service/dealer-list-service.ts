import {Injectable} from '@angular/core';
import { MasterListService } from '../../../../containers/core/master-component/master-service/master-list-service';
import { TextData } from '../../../shared/model/common/text-data';

@Injectable({
  providedIn: 'root'
})
export class DealerListService extends MasterListService {
  constructor() {
    super();
    this.fieldInfo = {
      colWidth: 4,
      listTitleEn: 'Dealer',
      listTitleBn: 'তালিকা',
      fields: [
        new TextData({
          key: 'customerInfoBean.customerName',
          fieldWidth: 4,
          name: 'customerName',
          id: 'customerName',
          label: 'Name',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 1
        }),
        new TextData({
          key: 'customerInfoBean.photoLink',
          fieldWidth: 4,
          name: 'photoLink',
          id: 'photoLink',
          label: 'Photo',
          type: 'img',
          canShow: true,
          canSort: true,
          order: 2
        }),
        new TextData({
          key: 'shopName',
          fieldWidth: 4,
          name: 'shopName',
          id: 'shopName',
          label: 'Shop Name',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 3
        }),
        new TextData({
          key: 'customerInfoBean.cluster.name',
          fieldWidth: 4,
          name: 'location',
          id: 'location',
          label: 'Shop Location',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 4
        }),
        new TextData({
          key: 'customerInfoBean.phoneNo',
          fieldWidth: 4,
          name: 'contactNumber',
          id: 'contactNumber',
          label: 'Contact Number',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 5
        }),
        new TextData({
          key: 'depositBalance',
          fieldWidth: 4,
          name: 'depositBalance',
          id: 'depositBalance',
          label: 'Deposit Balance',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 6
        }),
        new TextData({
          key: 'approved',
          fieldWidth: 4,
          name: 'approved',
          id: 'approved',
          label: 'Approval Status',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 7
        }),
        new TextData({
          key: 'action',
          fieldWidth: 4,
          name: 'অ্যাকশন',
          id: 'action',
          label: 'Action',
          // type: 'text',
          canShow: true,
          canSort: true,
          order: 8
        })
      ]
    };
  }
}
