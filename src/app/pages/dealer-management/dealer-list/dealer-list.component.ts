import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ServiceListControlWrapper } from '../../../containers/core/master-component/master-model/service-list-control-wrapper';
import { MasterListService } from '../../../containers/core/master-component/master-service/master-list-service';
import DataUtils from '../../shared/utils/data-utils';
import { DealerService } from '../service/dealer.service';
import { DealerButtonService } from './service/dealer-button-service';
import { DealerListService } from './service/dealer-list-service';

@Component({
  selector: 'app-dealer-list',
  templateUrl: './dealer-list.component.html',
  styleUrls: ['./dealer-list.component.scss']
})

export class DealerListComponent implements OnInit {

  // ELEMENT_DATA: any = [
  //   { name: 'Sahadat', locationName: 'Uttara', walletName: 'Bikas', mobile:'01711213148',depositBalance:15000, status: 'verified'}
  // ];

  data: any;
  isShowLoading = false;
  dataSource = new MatTableDataSource();
  dataSize = 10;
  serviceListControlWrapper: ServiceListControlWrapper<
    DealerListService,
    DealerButtonService
 >;
  constructor(private router: Router,
    private dealerListService: DealerListService,
    private dealerButtonService: DealerButtonService,
    private dealerService: DealerService) { }


    ngOnInit(): void {
      this.loadList(0,this.dataSize);
     }

     loadList(offset, limit) {
       this.dealerService.getDealerListTableData(offset,limit).subscribe((res) =>{
        console.log(res.content);
        this.data = DataUtils.flattenData(res.content);

         this.serviceListControlWrapper = new ServiceListControlWrapper<
         DealerListService,
         DealerButtonService
           >(
             this.dealerListService,
             this.data,
             this.dealerButtonService,
             res.totalElements,
             { pageSize: res.size, pageIndex: res.number }
           );
       });
     }

     onPageChange(event) {
      this.loadList(event.pageInfo.offset, event.pageInfo.limit);
    }

    execute(event: { element: any; rawData?: any; method: string; service?: MasterListService; methodKey?: string }) {
     // console.log(event);
     switch (event.method) {
       case 'add':
         this.router.navigate(['/pages/dealer-management/dealer-edit/0']);
         break;
       case 'edit':
        //  this.router.navigate(['/pages/dealer-management/dealer-edit/'+ event.element.id]);
         break;
       case 'view':
        this.router.navigate(['/pages/dealer-management/dealer-view/'+ event.element['customerInfoBean.id']]);
         break;
     }
   }
}
