import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DealerEditComponent } from './dealer-edit/dealer-edit.component';
import { DealerListComponent } from './dealer-list/dealer-list.component';
import { DealerViewComponent } from './dealer-view/dealer-view.component';
import { DealerComponent } from './dealer/dealer.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'dealer-management'
    },
    children: [
      {
        path: '',
        redirectTo: 'dealers'
      },
      {
        path: 'dealers',
        component: DealerComponent,
      },
      {
        path: 'dealer-list',
        component: DealerListComponent,
      },
      {
        path: 'dealer-edit/:id',
        component: DealerEditComponent
      },
      {
        path: 'dealer-view/:id',
        component: DealerViewComponent
      },



    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DealerManagementRoutingModule { }
