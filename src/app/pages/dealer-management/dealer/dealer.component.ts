import { Component, OnInit } from '@angular/core';
import { OBJ_EDIT, OBJ_LIST, OBJ_NEW } from '../../../constants/gas-monkey-constants';
import { EmitterService } from '../../shared/service/emitter.service';

@Component({
  selector: 'app-dealer',
  templateUrl: './dealer.component.html',
  styleUrls: ['./dealer.component.scss']
})
export class DealerComponent implements OnInit {

  mode: string;
  selectedDealerObj: any;

  constructor(private emitterService: EmitterService) { }
  ngOnInit() {
    this.mode = OBJ_LIST;
    this.listenEmittedApiObj();
  }

  listenEmittedApiObj() {
    this.emitterService.selectedDealerObj.subscribe(data => {
      if (data) {
        if (data.actionMode === OBJ_EDIT) {
          this.mode = OBJ_EDIT;
        } else if (data.actionMode === OBJ_NEW) {
          this.mode = OBJ_NEW;
        }  else {
          this.mode = OBJ_LIST;
        }
        this.selectedDealerObj = data;
      } else {
        this.mode = OBJ_LIST;
      }
    });
  }
}
