import { Component, OnInit, ViewChild } from '@angular/core';
import { PageEvent, MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { UtilService } from '../../../shared/service/util.service';
import { ProductService } from '../../service/product.service';

@Component({
  selector: 'app-product-price-list',
  templateUrl: './product-price-list.component.html',
  styleUrls: ['./product-price-list.component.scss']
})
export class ProductPriceListComponent implements OnInit {
  // ELEMENT_DATA: any = [
  //   { code : 'JMA', productDefaultPrice: 500 ,specialAmount: '150' , amount: '100', startDate: '22-03-10',endDate: '25-03-10',returnPrice:'200' }
  // ];

  public pageSizeOptions = [10,20,100,500];
  displayedColumns: string[];
  paginatorLength: number;
  pageEvent: PageEvent;
  dataSize = 10;
  isShowLoading = false;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  dataSource = new MatTableDataSource();

  constructor(
    private utilService: UtilService,
    private productService: ProductService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.setApiColumns();
    this.getProductPriceList(0, this.dataSize);
  }
  setApiColumns() {
    this.displayedColumns = ['productPriceCode' ,'defaultPrice',  'productPriceAmount', 'productPriceSpecialAmount','productReturnPrice', 'productPriceSpecialEndDate','productPriceSpecialStDate', 'action'];
  }

  getProductPriceList(page: number, size: number) {
    this.isShowLoading = true;
    this.productService.getProductPriceTableData(page,size).subscribe((res) =>{
      this.isShowLoading = false;
      console.log("----",res);
      this.setTableData(res.content);
      this.setPagination(res.totalPages, res.totalElements);
    });
  }

  setTableData(list: any) {
    this.dataSource.data = list;
  }

  setPagination(totalPages: number, totalElements: number) {
    this.paginatorLength = totalElements;
  }

  onPaginateChange(pageEvent: PageEvent) {
    this.getProductPriceList(pageEvent.pageIndex, pageEvent.pageSize);
  }

  createNewProductPrice(){
    this.router.navigate(['/pages/product-management/product-price/0']);
  }

  sendRowData(event) {
    console.log(event);
    this.router.navigate(['/pages/product-management/product-price/' + event.id]);
  }

  applyFilter(filterValue: string) {

    if(filterValue.length >= 3){
    //  this.apiBankHelperService.getClientTableDataForSearch(0, this.dataSize, filterValue).subscribe(
    //    res => {
    //      this.isShowLoading = false;
    //      this.setDashboardData(res.data.content);
    //      this.setPagination(res.data.totalPages, res.data.totalElements);
    //    },
    //    error => console.log(error)
    //  );
   }else if(filterValue.length == 0){
    //  this.getClientList(0, this.dataSize);
   }
 }



}
