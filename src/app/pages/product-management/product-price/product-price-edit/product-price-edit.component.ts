import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { OBJ_EDIT, OBJ_NEW } from '../../../../constants/gas-monkey-constants';
import { UtilService } from '../../../shared/service/util.service';
import { ProductService } from '../../service/product.service';

@Component({
  selector: 'app-product-price-edit',
  templateUrl: './product-price-edit.component.html',
  styleUrls: ['./product-price-edit.component.scss']
})
export class ProductPriceEditComponent implements OnInit {
  @Input() selectedProductPriceObj: any;
  userId:any;
  buttonText: string;
  actionMode: string;
  isShowLoading = false;
  productPriceForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private productService: ProductService,
    private utilService: UtilService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit(): void {

    this.activatedRoute.params.subscribe(
      params => {
        this.userId = params.id;
        console.log(this.userId);
        this.buttonText = this.userId != '0' ? 'UPDATE' : 'SAVE';
        this.actionMode = this.userId != '0' ? OBJ_EDIT : OBJ_NEW;
      });

      if(this.actionMode == OBJ_EDIT){
       // this.getDataUserById();
        //this.userForm.controls['email'].disable();
      }else{
        // this.isShowLoading = false;
        // this.userForm.controls['userId'].enable();
      }
      this.createProductPriceFormGroup();
  }

  createProductPriceFormGroup(){
    this.productPriceForm = this.fb.group({
      productPriceCode:['',[Validators.required]],
      defaultPrice:['',[Validators.required]],
      productPriceAmount:['',[Validators.required]],
      productPriceSpecialAmount:['',[Validators.required]],
      productPriceSpecialStDate:[''],
      productPriceSpecialEndDate:[''],
      productReturnPrice:['',[Validators.required]]
    } );
  }

  onSubmit(){
    if(this.actionMode == OBJ_EDIT){

      // this.storeService.storeUpdate(this.storeId,obj).subscribe(res =>{
      //   //console.log(res);
      //   this.populateData(res);
      //   this.utilService.showSnackBarMessage('Store Update Successfully',this.utilService.TYPE_MESSAGE.SUCCESS_TYPE);
      // },error => {
      //   this.utilService.showSnackBarMessage(error.message,this.utilService.TYPE_MESSAGE.ERROR_TYPE);
      // });

    }else{
    //   let obj = {
    //     "productPriceCode": this.productPriceForm.value.productPriceCode,
    //     "defaultPrice": this.productPriceForm.value.defaultPrice,
    //     "productPriceAmount": this.productPriceForm.value.productPriceAmount,
    //     "productPriceSpecialAmount": this.productPriceForm.value.productPriceSpecialAmount,
    //     "productPriceSpecialStDate": this.productPriceForm.value.productPriceSpecialStDate,
    //     "productPriceSpecialEndDate": this.productPriceForm.value.productPriceSpecialEndDate,
    //     "productReturnPrice": this.productPriceForm.value.productReturnPrice,

    // }
    // this.productService.producPriceCreate(obj).subscribe(res=>{
    //     this.utilService.showSnackBarMessage('Create Product price Successfully',this.utilService.TYPE_MESSAGE.SUCCESS_TYPE);
    //     this.router.navigate(['/pages/product-management/product-price']);
    // },error => {
    //   this.utilService.showSnackBarMessage(error.message,this.utilService.TYPE_MESSAGE.ERROR_TYPE);
    // });
    }

  }

  goToList(){
    this.router.navigate(['/pages/product-management/product-price']);
  }

  get productPriceCode() {
    return this.productPriceForm.get('productPriceCode');
  }
  get productPriceSpecialAmount() {
    return this.productPriceForm.get('productPriceSpecialAmount');
  }
  get productPriceAmount() {
    return this.productPriceForm.get('productPriceAmount');
  }
  get productPriceSpecialStDate() {
    return this.productPriceForm.get('productPriceSpecialStDate');
  }
  get productPriceSpecialEndDate() {
    return this.productPriceForm.get('productPriceSpecialEndDate');
  }
  get productReturnPrice() {
    return this.productPriceForm.get('productReturnPrice');
  }

}
