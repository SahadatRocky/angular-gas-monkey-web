import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FileUploader } from 'ng2-file-upload';
import { OBJ_EDIT, OBJ_NEW } from '../../../../constants/gas-monkey-constants';
import { UtilService } from '../../../shared/service/util.service';
import { ProductService } from '../../service/product.service';



@Component({
  selector: 'app-product-size-edit',
  templateUrl: './product-size-edit.component.html',
  styleUrls: ['./product-size-edit.component.scss']
})
export class ProductSizeEditComponent implements OnInit {
  @Input() selectedProductSizeObj: any;
  userId:any;
  buttonText: string;
  actionMode: string;
  isShowLoading = false;
  productSizeForm: FormGroup;

  merchantList: any = [
    {id: '1', value: 'CT'},
    {id: '2', value: 'HBR'}

  ];

  language1: any = [
    {id: '1', value: 'English'},
    {id: '2', value: 'Bangla'}

  ];
  code: any = [
    {id: '1', value: 'T11'},
    {id: '2', value: 'T33'}

  ];

  constructor(private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private utilService: UtilService,
    private productService: ProductService,
    private router: Router,) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(
      params => {
        this.userId = params.id;
        console.log(this.userId);
        this.buttonText = this.userId != '0' ? 'UPDATE' : 'SAVE';
        this.actionMode = this.userId != '0' ? OBJ_EDIT : OBJ_NEW;
      });

      if(this.actionMode == OBJ_EDIT){
       // this.getDataUserById();
        //this.userForm.controls['email'].disable();
      }else{
        // this.isShowLoading = false;
        // this.userForm.controls['userId'].enable();
      }

    this.createProductTypeFormGroup();
  }

  createProductTypeFormGroup(){
    this.productSizeForm = this.fb.group({
        name:['',[Validators.required]],
        description:['',[Validators.required]],
        visibleStatus:['']

    } );
  }

  onSubmit(){


  if(this.actionMode == OBJ_EDIT){

    // this.storeService.storeUpdate(this.storeId,obj).subscribe(res =>{
    //   //console.log(res);
    //   this.populateData(res);
    //   this.utilService.showSnackBarMessage('Store Update Successfully',this.utilService.TYPE_MESSAGE.SUCCESS_TYPE);
    // },error => {
    //   this.utilService.showSnackBarMessage(error.message,this.utilService.TYPE_MESSAGE.ERROR_TYPE);
    // });

  }else{
  //   let obj = {
  //     "name": this.productSizeForm.value.name,
  //     "description": this.productSizeForm.value.description,
  //     "visibleStatus":this.productSizeForm.value.visibleStatus
  // }
  // this.productService.productSizeCreate(obj).subscribe(res=>{
  //     this.utilService.showSnackBarMessage('Create Product Size Successfully',this.utilService.TYPE_MESSAGE.SUCCESS_TYPE);
  //     this.router.navigate(['/pages/product-management/product-size']);
  // },error => {
  //   this.utilService.showSnackBarMessage(error.message,this.utilService.TYPE_MESSAGE.ERROR_TYPE);
  // });
  }

  }

  goToList(){
    this.router.navigate(['/pages/product-management/product-size']);
  }

  get name() {
    return this.productSizeForm.get('name');
  }

  get description() {
    return this.productSizeForm.get('description');
  }

}
