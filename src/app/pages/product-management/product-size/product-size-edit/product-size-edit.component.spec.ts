import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ProductSizeEditComponent } from './product-size-edit.component';

describe('ProductSizeEditComponent', () => {
  let component: ProductSizeEditComponent;
  let fixture: ComponentFixture<ProductSizeEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductSizeEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductSizeEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
