import { Component, OnInit, ViewChild } from '@angular/core';
import { PageEvent, MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { UtilService } from '../../../shared/service/util.service';
import { ProductService } from '../../service/product.service';

@Component({
  selector: 'app-product-size-list',
  templateUrl: './product-size-list.component.html',
  styleUrls: ['./product-size-list.component.scss']
})
export class ProductSizeListComponent implements OnInit {

  ELEMENT_DATA: any = [
    {  name: '12KG', description: "it's a Regular product size.", status: 'active'},
    {  name: '24KG', description: "it's a Medium product size.", status: 'active'},
    {  name: '35KG', description: "it's a Big product size.", status: 'active'}
  ];
  public pageSizeOptions = [10,20,100,500];
  displayedColumns: string[];
  paginatorLength: number;
  pageEvent: PageEvent;
  dataSize = 10;
  isShowLoading = false;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  dataSource = new MatTableDataSource();

  constructor(
    private utilService: UtilService,
    private productService: ProductService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.setApiColumns();
    this.getProductSizeList(0, this.dataSize);
  }

  setApiColumns() {
    this.displayedColumns = ['name' ,'description', 'status' ,'action'];
  }

  getProductSizeList(page: number, size: number) {
    this.setTableData(this.ELEMENT_DATA);
    //  this.isShowLoading = true;
    // this.productService.getProductSizeTableData(page,size).subscribe((res) =>{
    //   this.isShowLoading = false;
    //   console.log("----",res);
    //   this.setTableData(res.content);
    //   this.setPagination(res.totalPages, res.totalElements);
    // });
  }

  setTableData(list: any) {
    this.dataSource.data = list;
  }

  setPagination(totalPages: number, totalElements: number) {
    this.paginatorLength = totalElements;
  }

  onPaginateChange(pageEvent: PageEvent) {
    this.getProductSizeList(pageEvent.pageIndex, pageEvent.pageSize);
  }

  createNewProductSize(){
    this.router.navigate(['/pages/product-management/product-size/0']);
  }

  sendRowData(event) {
    console.log(event);
    this.router.navigate(['/pages/product-management/product-size/' + event.id]);
  }

  applyFilter(filterValue: string) {

    if(filterValue.length >= 3){
    //  this.apiBankHelperService.getClientTableDataForSearch(0, this.dataSize, filterValue).subscribe(
    //    res => {
    //      this.isShowLoading = false;
    //      this.setDashboardData(res.data.content);
    //      this.setPagination(res.data.totalPages, res.data.totalElements);
    //    },
    //    error => console.log(error)
    //  );
   }else if(filterValue.length == 0){
    //  this.getClientList(0, this.dataSize);
   }
 }

}
