import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BrandEditComponent } from './brand/brand-edit/brand-edit.component';
import { BrandListComponent } from './brand/brand-list/brand-list.component';
import { BrandComponent } from './brand/brand.component';
import { ProductEditComponent } from './product/product-edit/product-edit.component';
import { ProductListComponent } from './product/product-list/product-list.component';
import { ProductComponent } from './product/product.component';

const routes: Routes = [
  {
    path:'',
    data:{
      title: 'product-management'
    },
    children:[
      // {
      //   path: '',
      //    redirectTo: 'product'
      // },
      {
        path:'product',
        component: ProductComponent,
        data:{
          title: 'product-list'
        },
      },
      {
        path: 'product-list',
        component: ProductListComponent,
        data:{
          title: 'product-list'
        },
      },
      {
        path: 'product/:id',
        component: ProductEditComponent,
        data:{
          title: 'product-create-or-edit'
        },
      },
      {
        path:'brand',
        component: BrandComponent,
        data:{
          title: 'brand-list'
        },
      },
      {
        path: 'brand-list',
        component: BrandListComponent,
        data:{
          title: 'brand-list'
        },
      },
      {
        path: 'brand/:id',
        component: BrandEditComponent,
        data:{
          title: 'brand-edit'
        },
      },
      // {
      //   path:'product-type',
      //   component: ProductTypeComponent,
      //   data:{
      //     title: 'product-type-list'
      //   },
      // },
      // {
      //   path: 'product-type-list',
      //   component: ProductTypeListComponent,
      //   data:{
      //     title: 'product-type-list'
      //   },
      // },
      // {
      //   path: 'product-type/:id',
      //   component: ProductTypeEditComponent,
      //   data:{
      //     title: 'product-type-create-or-edit'
      //   },
      // },
      // {
      //   path:'product-price',
      //   component: ProductPriceComponent,
      //   data:{
      //     title: 'product-price-list'
      //   },
      // },
      // {
      //   path: 'product-price-list',
      //   component: ProductPriceListComponent,
      //   data:{
      //     title: 'product-price-list'
      //   },
      // },
      // {
      //   path: 'product-price/:id',
      //   component: ProductPriceEditComponent,
      //   data:{
      //     title: 'product-price-create-or-edit'
      //   },
      // },
      // {
      //   path:'product-size',
      //   component: ProductSizeComponent,
      //   data:{
      //     title: 'product-size-list'
      //   },
      // },
      // {
      //   path: 'product-size-list',
      //   component: ProductSizeListComponent,
      //   data:{
      //     title: 'product-size-list'
      //   },
      // },
      // {
      //   path: 'product-size/:id',
      //   component: ProductSizeEditComponent,
      //   data:{
      //     title: 'product-size-create-or-edit'
      //   },
      // },
      // {
      //   path:'product-image',
      //   component: ProductImageComponent,
      //   data:{
      //     title: 'product-image-list'
      //   },
      // },
      // {
      //   path: 'product-image-list',
      //   component: ProductImageListComponent,
      //   data:{
      //     title: 'product-image-list'
      //   },
      // },
      // {
      //   path: 'product-upload-image',
      //   component: ProductUploadImagesComponent,
      //   data:{
      //     title: 'product-image-upload'
      //   },
      // }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductManagementRoutingModule { }
