import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ProductUploadImagesComponent } from '../product-upload-images/product-upload-images.component';

@Component({
  selector: 'app-product-image-list',
  templateUrl: './product-image-list.component.html',
  styleUrls: ['./product-image-list.component.scss']
})
export class ProductImageListComponent implements OnInit {

  ELEMENT_DATA: any = [
    { name: "image1", size: "480", defaultImage: "inactive"}
  ];
  public pageSizeOptions = [10,20,100,500];
  displayedColumns: string[];
  paginatorLength: number;
  pageEvent: PageEvent;
  dataSize = 10;
  isShowLoading = false;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  dataSource = new MatTableDataSource();
  constructor(private router: Router,
    public dialog: MatDialog) { }

  ngOnInit(): void {
    this.setApiColumns();
    this.setTableData(this.ELEMENT_DATA);

  }

  setApiColumns() {
    this.displayedColumns = ['name','size' ,'defaultImage' ,'action'];
  }

  setTableData(list: any) {
    this.dataSource.data = list;
  }

  onPaginateChange(pageEvent: PageEvent) {
    // this.getClientList(pageEvent.pageIndex, pageEvent.pageSize);
  }

  applyFilter(filterValue: string) {

    if(filterValue.length >= 3){
    //  this.apiBankHelperService.getClientTableDataForSearch(0, this.dataSize, filterValue).subscribe(
    //    res => {
    //      this.isShowLoading = false;
    //      this.setDashboardData(res.data.content);
    //      this.setPagination(res.data.totalPages, res.data.totalElements);
    //    },
    //    error => console.log(error)
    //  );
   }else if(filterValue.length == 0){
    //  this.getClientList(0, this.dataSize);
   }
 }



  gotoImageUploadDialogmode(){
    const dialogRef = this.dialog.open(ProductUploadImagesComponent, {

      disableClose: true,
      width: '500px',
      height: '300px',
      data: {
        type: 'product_image_upload'
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      //this.populateApiData(0, this.dataSize, this.apiBankoId);
    });
  }

}
