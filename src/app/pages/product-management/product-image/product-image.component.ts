import { Component, OnInit } from '@angular/core';
import { OBJ_EDIT, OBJ_LIST, OBJ_NEW } from '../../../constants/gas-monkey-constants';
import { EmitterService } from '../../shared/service/emitter.service';


@Component({
  selector: 'app-product-image',
  templateUrl: './product-image.component.html',
  styleUrls: ['./product-image.component.scss']
})
export class ProductImageComponent implements OnInit {

  mode: any;
  selectedProductImageObj: any;

  constructor(private emitterService: EmitterService) { }

  ngOnInit(): void {
    this.mode = OBJ_LIST;
    this.listenEmittedApiObj();
  }

  listenEmittedApiObj(){
    this.emitterService.selectedProductImageObj.subscribe(data=>{
      if(data){
        if(data.actionMode == OBJ_EDIT){
          this.mode = OBJ_EDIT;
        }else if(data.actionMode == OBJ_NEW){
           this.mode = OBJ_NEW;
        }else{
          this.mode = OBJ_LIST;
        }
        this.selectedProductImageObj = data;
      }else{
        this.mode = OBJ_LIST;
      }
    })
  }

}
