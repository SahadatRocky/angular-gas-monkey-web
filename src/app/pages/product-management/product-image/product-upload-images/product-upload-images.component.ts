import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FileUploadService } from '../../service/file-upload.service';

@Component({
  selector: 'app-product-upload-images',
  templateUrl: './product-upload-images.component.html',
  styleUrls: ['./product-upload-images.component.scss']
})
export class ProductUploadImagesComponent implements OnInit {
  imageUploadForm:FormGroup;
  imageTypeList: any = [
    {id: '1', value: 'image-type-1'},
    {id: '2', value: 'image-type-2'}

  ];
  selectedFiles?: FileList;
  previews: string[] = [];
  constructor(private uploadService: FileUploadService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<ProductUploadImagesComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {}

  ngOnInit(): void {
    this.createProductTypeFormGroup();
  }

  createProductTypeFormGroup(){
    this.imageUploadForm = this.fb.group({
      imageTypeId:['']
    } );
  }



  selectFiles(event: any): void {
    this.selectedFiles = event.target.files;

    this.previews = [];
    if (this.selectedFiles && this.selectedFiles[0]) {
      const numberOfFiles = this.selectedFiles.length;
      for (let i = 0; i < numberOfFiles; i++) {
        const reader = new FileReader();

        reader.onload = (e: any) => {
          this.previews.push(e.target.result);
        };

        reader.readAsDataURL(this.selectedFiles[i]);
      }
    }
  }

  upload(idx: number, file: File): void {
    if (file) {
      // this.uploadService.upload(file).subscribe(
      //   (event: any) => {
      //     if (event instanceof HttpResponse) {
      //       const msg = 'Uploaded the file successfully: ' + file.name;
      //     }
      //   },
      //   (err: any) => {
      //     const msg = 'Could not upload the file: ' + file.name;
      //   }
      // );
    }
  }

  uploadFiles(): void {
    console.log(this.imageUploadForm.value);
    if (this.selectedFiles) {
      console.log(this.selectedFiles);
      for (let i = 0; i < this.selectedFiles.length; i++) {
        this.upload(i, this.selectedFiles[i]);
      }
    }
  }

  cancelBtn(){
    this.dialogRef.close();
  }

}
