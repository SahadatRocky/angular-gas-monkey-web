import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductComponent } from './product/product.component';
import { ProductListComponent } from './product/product-list/product-list.component';
import { ProductEditComponent } from './product/product-edit/product-edit.component';
import { ProductTypeComponent } from './product-type/product-type.component';
import { ProductTypeListComponent } from './product-type/product-type-list/product-type-list.component';
import { ProductTypeEditComponent } from './product-type/product-type-edit/product-type-edit.component';
import { ProductPriceComponent } from './product-price/product-price.component';
import { ProductPriceListComponent } from './product-price/product-price-list/product-price-list.component';
import { ProductPriceEditComponent } from './product-price/product-price-edit/product-price-edit.component';
import { ProductSizeComponent } from './product-size/product-size.component';
import { ProductSizeListComponent } from './product-size/product-size-list/product-size-list.component';
import { ProductSizeEditComponent } from './product-size/product-size-edit/product-size-edit.component';
import { ProductManagementRoutingModule } from './product-management-routing.module';
import { SharedModule } from '../shared/shared.module';
import { ProductImageComponent } from './product-image/product-image.component';
import { ProductImageListComponent } from './product-image/product-image-list/product-image-list.component';
import { ProductUploadImagesComponent } from './product-image/product-upload-images/product-upload-images.component';
import { BrandComponent } from './brand/brand.component';
import { BrandEditComponent } from './brand/brand-edit/brand-edit.component';
import { BrandListComponent } from './brand/brand-list/brand-list.component';




@NgModule({
  declarations: [
    ProductListComponent,
    ProductComponent,
    ProductEditComponent,
    BrandComponent,
    BrandListComponent,
    BrandEditComponent,
    ProductTypeComponent,
    ProductTypeListComponent,
    ProductTypeEditComponent,
    ProductPriceComponent,
    ProductPriceListComponent,
    ProductPriceEditComponent,
    ProductSizeComponent,
    ProductSizeListComponent,
    ProductSizeEditComponent,
    ProductImageComponent,
    ProductImageListComponent,
    ProductUploadImagesComponent,

  ],
  imports: [
    CommonModule,
    ProductManagementRoutingModule,
    SharedModule

  ]
})
export class ProductManagementModule { }
