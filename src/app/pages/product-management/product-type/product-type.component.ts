import { Component, OnInit } from '@angular/core';
import { OBJ_EDIT, OBJ_LIST, OBJ_NEW } from '../../../constants/gas-monkey-constants';
import { EmitterService } from '../../shared/service/emitter.service';

@Component({
  selector: 'app-product-type',
  templateUrl: './product-type.component.html',
  styleUrls: ['./product-type.component.scss']
})
export class ProductTypeComponent implements OnInit {
  mode: string;
  selectedProductTypeObj: any;

  constructor(private emitterService: EmitterService) { }

  ngOnInit(): void {
    this.mode = OBJ_LIST;
    this.listenEmittedApiObj();
  }
  listenEmittedApiObj() {
    this.emitterService.selectedProductTypeObj.subscribe(data => {
      if (data) {
        if (data.actionMode === OBJ_EDIT) {
          this.mode = OBJ_EDIT;
        } else if (data.actionMode === OBJ_NEW) {
          this.mode = OBJ_NEW;
        }  else {
          this.mode = OBJ_LIST;
        }
        this.selectedProductTypeObj = data;
      } else {
        this.mode = OBJ_LIST;
      }
    });
  }

}
