import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FileUploader } from 'ng2-file-upload';
import { OBJ_EDIT, OBJ_NEW } from '../../../../constants/gas-monkey-constants';
import { UserService } from '../../../role-management/service/user.service';
import { UtilService } from '../../../shared/service/util.service';
import { StoreService } from '../../../store-management/service/store.service';
import { ProductService } from '../../service/product.service';


@Component({
  selector: 'app-product-type-edit',
  templateUrl: './product-type-edit.component.html',
  styleUrls: ['./product-type-edit.component.scss']
})
export class ProductTypeEditComponent implements OnInit {
  @Input() selectedProductTypeObj: any;
  userId:any;
  buttonText: string;
  actionMode: string;
  isShowLoading = false;
  productTypeForm: FormGroup;
  languageList:any;

  code: any = [
    {id: 'Regular', value: 'Regular'}

  ];
  merchantList: any = [
    {id: '1', value: 'Gas Monkey'},

  ];

  constructor(private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private productService : ProductService,
    private storeService: StoreService,
    private userService: UserService,
    private utilService: UtilService,) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(
      params => {
        this.userId = params.id;
        console.log(this.userId);
        this.buttonText = this.userId != '0' ? 'UPDATE' : 'SAVE';
        this.actionMode = this.userId != '0' ? OBJ_EDIT : OBJ_NEW;
      });

      if(this.actionMode == OBJ_EDIT){
       // this.getDataUserById();
        //this.userForm.controls['email'].disable();
      }else{
        // this.isShowLoading = false;
        // this.userForm.controls['userId'].enable();
      }

    this.createProductTypeFormGroup();
    this.getStoreDropdownListData();
    // this.getLanguageDropdownListData();
  }

  createProductTypeFormGroup(){
    this.productTypeForm = this.fb.group({
        titleEng:['', [Validators.required]],
        titleBan:['', [Validators.required]],
        nameEng:['', [Validators.required]],
        nameBan:['', [Validators.required]],
        descriptionEng:[''],
        descriptionBan:[''],
        productTypeCode:[''],
        merchantId: ['']
    } );
  }

  // getLanguageDropdownListData(){
  //   this.userService.getLanguageDropdownList().subscribe(response =>{
  //     this.languageList = response;
  //      console.log('-->>',response);
  //   });
  // }

  getStoreDropdownListData(){
    // this.storeService.getStoreDropdownList().subscribe(response =>{
    //   this.merchantList = response;
    //   console.log('-->>',response);
    // });
  }

  onSubmit(){
    console.log(this.productTypeForm.value);

    if(this.actionMode == OBJ_EDIT){

      // this.storeService.storeUpdate(this.storeId,obj).subscribe(res =>{
      //   //console.log(res);
      //   this.populateData(res);
      //   this.utilService.showSnackBarMessage('Store Update Successfully',this.utilService.TYPE_MESSAGE.SUCCESS_TYPE);
      // },error => {
      //   this.utilService.showSnackBarMessage(error.message,this.utilService.TYPE_MESSAGE.ERROR_TYPE);
      // });

    }else{

    //   let obj = {
    //     "productTypeCode": this.productTypeForm.value.productTypeCode,
    //     "productTypeVisible": this.productTypeForm.value.productTypeVisible,
    //     "productTypeStatus": this.productTypeForm.value.productTypeStatus,
    //     "sortOrder": 1,
    //     "merchantId": this.productTypeForm.value.merchantId,
    //     "languageId":  this.productTypeForm.value.languageId,
    //     "parentId": null,
    //     "name": this.productTypeForm.value.name,
    //     "description": this.productTypeForm.value.description,
    //     "title": this.productTypeForm.value.title
    // }
    //   this.productService.productTypeCreate(obj).subscribe(res=>{
    //     this.utilService.showSnackBarMessage('Create Product Type Successfully',this.utilService.TYPE_MESSAGE.SUCCESS_TYPE);
    //     this.router.navigate(['/pages/product-management/product-type']);
    // },error => {
    //   this.utilService.showSnackBarMessage(error.message,this.utilService.TYPE_MESSAGE.ERROR_TYPE);
    // });
    }
  }

  goToList(){
    this.router.navigate(['/pages/product-management/product-type']);
  }


  get titleEng() {
    return this.productTypeForm.get('titleEng');
  }

  get titleBan() {
    return this.productTypeForm.get('titleBan');
  }

  get nameEng() {
    return this.productTypeForm.get('nameEng');
  }

  get nameBan() {
    return this.productTypeForm.get('nameBan');
  }

  get productTypeCode() {
    return this.productTypeForm.get('productTypeCode');
  }

  get merchantId() {
    return this.productTypeForm.get('merchantId');
  }

  get descriptionEng() {
    return this.productTypeForm.get('descriptionEng');
  }
  get descriptionBan() {
    return this.productTypeForm.get('descriptionBan');
  }




}
