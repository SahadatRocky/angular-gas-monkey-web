import { Component, OnInit, ViewChild } from '@angular/core';
import { PageEvent, MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { UtilService } from '../../../shared/service/util.service';
import { ProductService } from '../../service/product.service';

@Component({
  selector: 'app-product-type-list',
  templateUrl: './product-type-list.component.html',
  styleUrls: ['./product-type-list.component.scss']
})
export class ProductTypeListComponent implements OnInit {
  ELEMENT_DATA: any = [
    { typeName: 'Regular', code : 'JM001',merchantName: 'Gas monkey'},
    { typeName: 'Medium', code : 'JM002',merchantName: 'Gas monkey'},
    { typeName: 'Big', code : 'JM003',merchantName: 'Gas monkey'}
  ];

  public pageSizeOptions = [10,20,100,500];
  displayedColumns: string[];
  paginatorLength: number;
  pageEvent: PageEvent;
  dataSize = 10;
  isShowLoading = false;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  dataSource = new MatTableDataSource();

  constructor(
    private utilService: UtilService,
    private router: Router,
    private productService : ProductService
  ) { }

  ngOnInit(): void {
    this.setApiColumns();
    this.getProductTypeList(0, this.dataSize);
  }

  setApiColumns() {
    this.displayedColumns = ['typeName' ,'code','merchantName', 'action'];
  }

  getProductTypeList(page: number, size: number) {
    this.setTableData(this.ELEMENT_DATA);
    // this.isShowLoading = true;
    // this.productService.getProductTypeTableData(page,size).subscribe((res) =>{
    //   this.isShowLoading = false;
    //   console.log("----",res);
    //   this.setTableData(res.content);
    //   this.setPagination(res.totalPages, res.totalElements);
    // });
  }

  setTableData(list: any) {
    this.dataSource.data = list;
  }

  setPagination(totalPages: number, totalElements: number) {
    this.paginatorLength = totalElements;
  }

  onPaginateChange(pageEvent: PageEvent) {
    this.getProductTypeList(pageEvent.pageIndex, pageEvent.pageSize);
  }

  createNewProductType(){
    this.router.navigate(['/pages/product-management/product-type/0']);
  }

  sendRowData(event) {
    console.log(event);
    this.router.navigate(['/pages/product-management/product-type/' + event.id]);
  }

  applyFilter(filterValue: string) {

    if(filterValue.length >= 3){
    //  this.apiBankHelperService.getClientTableDataForSearch(0, this.dataSize, filterValue).subscribe(
    //    res => {
    //      this.isShowLoading = false;
    //      this.setDashboardData(res.data.content);
    //      this.setPagination(res.data.totalPages, res.data.totalElements);
    //    },
    //    error => console.log(error)
    //  );
   }else if(filterValue.length == 0){
    //  this.getClientList(0, this.dataSize);
   }
 }

}
