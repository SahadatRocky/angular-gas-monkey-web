import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiEndpoints } from '../../../api-endpoints';

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {
  //private baseUrl = 'http://localhost:8080';
  private apiEndpoints: ApiEndpoints = new ApiEndpoints();
  constructor(private http: HttpClient) { }

  // upload(file: File): Observable<HttpEvent<any>> {
  //   const formData: FormData = new FormData();

  //   formData.append('file', file);

  //   const req = new HttpRequest('POST', `${this.apiEndpoints.PRODUCT_IMAGE_UPLOAD}/upload`, formData, {
  //     reportProgress: true,
  //     responseType: 'json'
  //   });

  //   return this.http.request(req);
  // }

  // getProductImageUploadFiles(): Observable<any> {
  //   return this.http.get(`${this.apiEndpoints.PRODUCT_IMAGE_UPLOAD}/files`);
  // }
}
