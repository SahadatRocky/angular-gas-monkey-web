import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { ApiEndpoints } from "../../../api-endpoints";


@Injectable({
  providedIn: 'root'
})
export class ProductService {

  HTTPOptions:Object = {
    observe: 'response',
    responseType: 'text'
 }

  private apiEndpoints: ApiEndpoints = new ApiEndpoints();
  constructor(private http: HttpClient) { }

  // PRODUCT CATEGORY
  brandCreate(Obj: any): Observable<any>{
    let url = this.apiEndpoints.BRAND_BASE_URL;
    return this.http.post<any>(url, Obj).pipe(map(value => value))
  }

  getBrandListTableData(page: number, size: number): Observable<any> {
    let url = this.apiEndpoints.BRAND_BASE_URL;
    const fullUrl = `${url}?page=${page}&size=${size}`;
    return this.http.get<any>(fullUrl).pipe(
      map(value => value)
      );
  }

  getBrandById(id:any): Observable<any>{
    let url = this.apiEndpoints.BRAND_BASE_URL + '/' + id;
    return this.http.get<any>(url).pipe(
      map(value => value)
      );
  }

  brandUpdate(id:any, obj:any): Observable<any>{
    let url = this.apiEndpoints.BRAND_BASE_URL + '/' + id;
    return this.http.put<any>(url,obj).pipe(
      map(value => value)
    );
  }

  // PRODUCT
  getProductSizeDropDownList(){
    let url = this.apiEndpoints.PRODUCT_SIZE_BASE_URL+this.apiEndpoints.DROPDOWN_LIST_API_URL;
    return this.http.get<any>(url).pipe(
      map(value => value)
    );
  }

  getProductValveSizeDropDownList(){
    let url = this.apiEndpoints.PRODUCT_VALVE_SIZE_BASE_URL+this.apiEndpoints.DROPDOWN_LIST_API_URL;
    return this.http.get<any>(url).pipe(
      map(value => value)
    );
  }

  getProductBrandDropDownList(){
    let url = this.apiEndpoints.BRAND_BASE_URL+this.apiEndpoints.DROPDOWN_LIST_API_URL;
    return this.http.get<any>(url).pipe(
      map(value => value)
    );
  }

  productCreate(Obj: any): Observable<any>{
    let url = this.apiEndpoints.PRODUCT_BASE_URL;
    return this.http.post<any>(url, Obj).pipe(map(value => value))
  }

  getProductListTableData(page: number, size: number): Observable<any> {
    let url = this.apiEndpoints.PRODUCT_BASE_URL;
    const fullUrl = `${url}?page=${page}&size=${size}`;
    return this.http.get<any>(fullUrl).pipe(
      map(value => value)
    );
  }

  getProductDataById(id:any): Observable<any>{
    let url = this.apiEndpoints.PRODUCT_BASE_URL + '/' + id;
    return this.http.get<any>(url).pipe(
      map(value => value)
      );
  }

  productUpdate(id:any, obj:any): Observable<any>{
    let url = this.apiEndpoints.PRODUCT_BASE_URL + '/' + id;
    return this.http.put<any>(url,obj).pipe(
      map(value => value)
    );
  }

  // Product image

  uploadProductImage(formData: any): Observable<any>{
    let url = this.apiEndpoints.UPLOAD_PRODUCT_IMAGE_URL;
    return this.http.post<any>(url, formData,this.HTTPOptions).pipe(map(value => value))
  }

  createProductImage(obj: any): Observable<any>{
    let url = this.apiEndpoints.CREATE_PRODUCT_IMAGE_URL;
    return this.http.post<any>(url, obj).pipe(map(value => value))
  }

  getFindProductImage(id:any): Observable<any>{
    let url = this.apiEndpoints.FIND_PRODUCT_IMAGE_URL + '/' + id;
    return this.http.get<any>(url).pipe(
      map(value => value)
      );
  }

  updateProductImage(id:any, obj:any): Observable<any> {
    let url = this.apiEndpoints.CREATE_PRODUCT_IMAGE_URL+ '/' + id;
    return this.http.put<any>(url,obj).pipe(
      map(value => value)
      );
  }

  deleteProductImage(id:any): Observable<any> {
    let url = this.apiEndpoints.CREATE_PRODUCT_IMAGE_URL+ '/' + id;
    return this.http.delete<any>(url,this.HTTPOptions).pipe(
      map(value => value)
      );
  }

  updateBrandImage(id:any, obj:any): Observable<any> {
    let url = this.apiEndpoints.CREATE_BRAND_IMAGE_URL+ '/' + id;
    return this.http.put<any>(url,obj).pipe(
      map(value => value)
      );
  }

  deleteBrandImage(id:any): Observable<any> {
    let url = this.apiEndpoints.CREATE_BRAND_IMAGE_URL+ '/' + id;
    return this.http.delete<any>(url,this.HTTPOptions).pipe(
      map(value => value)
      );
  }


  createBrandImage(obj: any): Observable<any>{
    let url = this.apiEndpoints.CREATE_BRAND_IMAGE_URL;
    return this.http.post<any>(url, obj).pipe(map(value => value))
  }


  getProductSizeTableData(page: number, size: number): Observable<any> {
    let url = this.apiEndpoints.PRODUCT_BASE_URL;
    const fullUrl = `${url}?page=${page}&size=${size}`;
    return this.http.get<any>(fullUrl).pipe(
      map(value => value)
      );
  }

  getProductPriceTableData(page: number, size: number): Observable<any> {
    let url = this.apiEndpoints.PRODUCT_BASE_URL;
    const fullUrl = `${url}?page=${page}&size=${size}`;
    return this.http.get<any>(fullUrl).pipe(
      map(value => value)
      );
  }

}
