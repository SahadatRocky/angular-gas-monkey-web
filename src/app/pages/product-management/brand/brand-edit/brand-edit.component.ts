import { DatePipe } from "@angular/common";
import { Component, EventEmitter, Input, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
} from "@angular/forms";
import { MatDialog } from "@angular/material/dialog";
import { ActivatedRoute, Router } from "@angular/router";
import { FileUploader } from "ng2-file-upload";
import { debounceTime, distinctUntilChanged, switchMap } from "rxjs/operators";
import { OBJ_EDIT, OBJ_NEW } from "../../../../constants/gas-monkey-constants";
import { DialogImageViewerComponent } from "../../../shared/components/dialog-image-viewer/dialog-image-viewer.component";
import { UtilService } from "../../../shared/service/util.service";
import { ProductService } from "../../service/product.service";

@Component({
  selector: "app-brand-edit",
  templateUrl: "./brand-edit.component.html",
  styleUrls: ["./brand-edit.component.scss"],
  providers: [DatePipe]
})
export class BrandEditComponent implements OnInit {
  banglaPattern = "/[^\p{Script=Bengali}]+/u";
  @Input() brandObj: any;
  brandId: any;
  buttonText: string;
  actionMode: string;
  isShowLoading = false;
  brnadForm: FormGroup;
  //file = [];
  languageList: any;
  brandImageList = [];
  checkedDiscountEnable:boolean=false;
  // ELEMENT_DATA =
  //   {  nameEng: 'JAMUNA',nameBan: 'যমুনা', descriptionEng : 'Jamuna Gas in one of the largest LP Gas bulk importation, storage, bottling, marketing & distribution, company in Bangladesh. It is the first Govt Approved private LP Gas plant to be engaged in this sector. It has developed immensely in both internally and externally from the time of its commencement. It is the fastest in growing network of interrelated business in LP gas sector. We continue to implement and search for new technologies and applications for the benefit of our product and also for the customers.',descriptionBan: 'যমুনা গ্যাস বাংলাদেশের অন্যতম বৃহত্তম এলপি গ্যাস বাল্ক আমদানি, স্টোরেজ, বোতলজাতকরণ, বিপণন ও বিতরণ, কোম্পানি। এটি এই খাতে নিযুক্ত হওয়া প্রথম সরকার অনুমোদিত বেসরকারি এলপি গ্যাস প্ল্যান্ট। এটি শুরু হওয়ার সময় থেকে অভ্যন্তরীণ এবং বাহ্যিক উভয় ক্ষেত্রেই ব্যাপকভাবে বিকশিত হয়েছে। এটি এলপি গ্যাস সেক্টরে আন্তঃসম্পর্কিত ব্যবসার সবচেয়ে দ্রুত বর্ধনশীল নেটওয়ার্ক। আমরা আমাদের পণ্যের সুবিধার জন্য এবং গ্রাহকদের জন্য নতুন প্রযুক্তি এবং অ্যাপ্লিকেশনগুলি বাস্তবায়ন এবং অনুসন্ধান চালিয়ে যাচ্ছি।'};
  discountList=[
    {id:'PERCENT', value:'PERCENT'},
    {id: 'AMOUNT', value:'AMOUNT'}
  ];

  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private productService: ProductService,
    private utilService: UtilService,
    private datePipe: DatePipe,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      this.brandId = params.id;
      //console.log(this.brandId);
      this.buttonText = this.brandId != "0" ? "UPDATE" : "SAVE";
      this.actionMode = this.brandId != "0" ? OBJ_EDIT : OBJ_NEW;
    });

    this.createBrandFormGroup();

    if (this.actionMode == OBJ_EDIT) {
      this.getBrandById();
      //this.userForm.controls['email'].disable();
    } else {
      this.isShowLoading = false;
      // this.userForm.controls['userId'].enable();
    }
  }

  createBrandFormGroup() {
    this.brnadForm = this.fb.group({
      companyName: ["", Validators.required],
      nameEn: ["", Validators.required],
      nameBn: ["", [Validators.required]],
      status: [false],
      discountEnabled: [false],
      discountType:[''],
      discountValue:[''],
      discountStartDate:[''],
      discountEndDate:['']
    });
  }

  getBrandById() {
    //console.log("brand-id", this.brandId);
    //this.populateData(this.ELEMENT_DATA);
    this.isShowLoading = true;
    this.productService.getBrandById(this.brandId).subscribe((res) => {
      console.log(res);
      this.brandImageList = res.brandImages;
      //console.log('brnad-image-list',this.brandImageList);
      this.isShowLoading = false;
      this.populateData(res);
    });
  }

  populateData(myData) {
    this.brnadForm.patchValue(myData);
    this.checkedDiscountEnable = this.brnadForm.controls.discountEnabled.value;
  }

  uploader: FileUploader = new FileUploader({
    //url: URL,
    disableMultipart: false,
    // autoUpload: true,
    method: "post",
    itemAlias: "attachment",
    allowedFileType: ["image", "video"],
  });

  onFileSelected(event: EventEmitter<File>) {
    const file: any = event;
    const title = file[0].name;
    const formData = new FormData();
    formData.append("file", file[0]);
    //console.log(formData.get("file"));
    this.productService.uploadProductImage(formData).subscribe((res) => {
      //console.log("----------------------img-response-------------", res);
      if (res.status == 200) {
        //console.log(title);
        let obj = {
          imageLink: res.body,
          imageLinkId: "",
          status: true,
          brandId: this.brandId,
          title: title,
        };
        this.productService.createBrandImage(obj).subscribe((res) => {
          console.log("Brand image created", res);
          this.getBrandById();
        });
      }
    });
  }

  onSubmit() {
    if (this.actionMode == OBJ_EDIT) {
      let obj = {
        "id": this.brandId,
        "companyName": this.brnadForm.value.companyName,
        "nameEn": this.brnadForm.value.nameEn,
        "nameBn": this.brnadForm.value.nameBn,
        "status": this.brnadForm.value.status,
        "discountValue":this.brnadForm.value.discountValue,
        "discountType":this.brnadForm.value.discountType,
        "discountStartDate":this.datePipe.transform(this.brnadForm.value.discountStartDate,'yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\''),
        "discountEndDate":this.datePipe.transform(this.brnadForm.value.discountEndDate,'yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\''),
        "discountEnabled": this.brnadForm.value.discountEnabled,
      };
      // console.log(obj);
      this.productService.brandUpdate(this.brandId, obj).subscribe(
        (res) => {
          //console.log(res);
          this.utilService.showSnackBarMessage(
            "Brand Updated Successfully",
            this.utilService.TYPE_MESSAGE.SUCCESS_TYPE
          );
          this.router.navigate(["/pages/product-management/brand"]);
        },
        (error) => {
          this.utilService.showSnackBarMessage(
            error.message,
            this.utilService.TYPE_MESSAGE.ERROR_TYPE
          );
        }
      );
    } else {
      let obj = {
        "companyName": this.brnadForm.value.companyName,
        "nameEn": this.brnadForm.value.nameEn,
        "nameBn": this.brnadForm.value.nameBn,
        "status": true,
      };
      this.productService.brandCreate(obj).subscribe(
        (res) => {
          this.utilService.showSnackBarMessage(
            "Brand Created Successfully",
            this.utilService.TYPE_MESSAGE.SUCCESS_TYPE
          );
          this.router.navigate(["/pages/product-management/brand"]);
        },
        (error) => {
          this.utilService.showSnackBarMessage(
            error.message,
            this.utilService.TYPE_MESSAGE.ERROR_TYPE
          );
        }
      );
    }
  }

  onTextChangeOnlyBangla(event){
    const reg = new RegExp('[\p{Script=Bengali}]+');
  const engNumberSpecReg = new RegExp("^[ A-Za-z0-9_!@#$%^&*()={}:;<>+`~|'-/\"\]*$");

        let test = engNumberSpecReg.test(event.target.value);
        let output = reg.test(event.target.value);

        if(test || event.target.value == "\\" || event.target.value == "[" || event.target.value == "]" || event.target.value == "?"){
          this.brnadForm.get('nameBn').setValue('');
        }
        if( !test &&  output){
          this.brnadForm.get('nameBn').setValue(event.target.value);
        }
  }

  onDiscountTypeChange(event){
    console.log(event);
  }

  onchangeDiscountEnable(event){
    this.checkedDiscountEnable = event.checked;
    this.brnadForm.get('discountEnabled').setValue(event.checked);
}

checkDisable(){
  //console.log("check------");
  if(this.actionMode == 'edit'){
    if(this.brnadForm.value.discountEnabled){
      //console.log("offer-product-true");
      this.brnadForm.get('discountEnabled').setValidators(Validators.requiredTrue);
      this.brnadForm.get('discountValue').setValidators(Validators.required);
      this.brnadForm.get('discountType').setValidators(Validators.required);
      this.brnadForm.get('discountStartDate').setValidators(Validators.required);
      this.brnadForm.get('discountEndDate').setValidators(Validators.required);
      this.brnadForm.get('discountEnabled').updateValueAndValidity();
      this.brnadForm.get('discountValue').updateValueAndValidity();
      this.brnadForm.get('discountType').updateValueAndValidity();
      this.brnadForm.get('discountStartDate').updateValueAndValidity();
      this.brnadForm.get('discountEndDate').updateValueAndValidity();
    }
    if(!this.brnadForm.value.discountEnabled){
      //console.log("offer-product-false");
      this.brnadForm.get('discountEnabled').clearValidators();;
      this.brnadForm.get('discountValue').clearValidators();;
      this.brnadForm.get('discountType').clearValidators();;
      this.brnadForm.get('discountStartDate').clearValidators();;
      this.brnadForm.get('discountEndDate').clearValidators();;
      this.brnadForm.get('discountEnabled').updateValueAndValidity();
      this.brnadForm.get('discountValue').updateValueAndValidity();
      this.brnadForm.get('discountType').updateValueAndValidity();
      this.brnadForm.get('discountStartDate').updateValueAndValidity();
      this.brnadForm.get('discountEndDate').updateValueAndValidity();
    }
    return !(this.brnadForm.valid);
  }else{
    return !this.brnadForm.valid;
  }
}


  goToList() {
    this.router.navigate(["/pages/product-management/brand"]);
  }

  imageUpdate(id:any , obj:any){
    this.productService.updateBrandImage(id,obj).subscribe(res =>{
      this.getBrandById();
      this.utilService.showSnackBarMessage(' Brand Image Update Successfully',this.utilService.TYPE_MESSAGE.SUCCESS_TYPE);
    });
  }

  onToggleChnage(item:any,checked:any){
    console.log(item);
    //console.log('-----------------checked',checked);
    let obj = {
      imageLink: item.imageLink,
      imageLinkId: "",
      status: checked,
      title: item.title,
    };
    this.imageUpdate(item.id, obj);
  }

  onBrandImageDelete(item:any){
    this.productService.deleteBrandImage(item.id).subscribe(res=>{
      if(res){
        this.getBrandById();
        this.utilService.showSnackBarMessage(' Brand Image Delete Successfully',this.utilService.TYPE_MESSAGE.SUCCESS_TYPE);
      }
    });
}

openImageDialog(event){
  console.log(event.srcElement.src);
  const dialogRef = this.dialog.open(DialogImageViewerComponent, {

    // disableClose: true,
    width: '550px',
    height: '550px',
    data: {
      title: 'Image',
      obj: event.srcElement.src
    },
  });

  dialogRef.afterClosed().subscribe(result => {
    console.log('image closed');
  });

}

  get companyName() {
    return this.brnadForm.get("companyName");
  }

  get nameEn() {
    return this.brnadForm.get("nameEn");
  }

  get nameBn() {
    return this.brnadForm.get("nameBn");
  }

  get categoryStatus() {
    return this.brnadForm.get("categoryStatus");
  }

  get discountValue() {
    return this.brnadForm.get("discountValue");
  }

  get discountEnabled(){
    return this.brnadForm.get('discountEnabled');
  }

  get discountEndDate(){
    return this.brnadForm.get('discountEndDate');
  }

  get discountStartDate(){
    return this.brnadForm.get('discountStartDate');
  }
}
