import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ServiceListControlWrapper } from '../../../../containers/core/master-component/master-model/service-list-control-wrapper';
import { MasterListService } from '../../../../containers/core/master-component/master-service/master-list-service';
import DataUtils from '../../../shared/utils/data-utils';
import { ProductService } from '../../service/product.service';
import { BrandButtonService } from './service/brand-button-service';
import { BrandListService } from './service/brand-list-service';

@Component({
  selector: 'app-brand-list',
  templateUrl: './brand-list.component.html',
  styleUrls: ['./brand-list.component.scss']
})
export class BrandListComponent implements OnInit {

  // ELEMENT_DATA: any = [
  //   {  name: 'JAMUNA', desc : 'Jamuna Gas in one of the largest LP Gas bulk importation, storage, bottling, marketing & distribution, company in Bangladesh. It is the first Govt Approved private LP Gas plant to be engaged in this sector. It has developed immensely in both internally and externally from the time of its commencement. It is the fastest in growing network of interrelated business in LP gas sector. We continue to implement and search for new technologies and applications for the benefit of our product and also for the customers.', status: 'active',}
  // ];

  data: any;
  isShowLoading = false;
  dataSource = new MatTableDataSource();
  dataSize = 10;
  serviceListControlWrapper: ServiceListControlWrapper<
    BrandListService,
    BrandButtonService
    >;

  constructor(
    private router: Router,
    private  brandButtonService: BrandButtonService,
    private brandListService: BrandListService,
    private productService : ProductService
    ) { }

  ngOnInit(): void {
    this.loadList(0,this.dataSize);
  }

  loadList(offset, limit) {
    this.productService.getBrandListTableData(offset, limit).subscribe((res) =>{
      console.log(res.content);
      this.data = DataUtils.flattenData(res.content);
      this.serviceListControlWrapper = new ServiceListControlWrapper<
      BrandListService,
      BrandButtonService
        >(
          this.brandListService,
          this.data,
          this.brandButtonService,
          res.totalElements,
          { pageSize: res.size, pageIndex: res.number }
        );
    });
  }

  onPageChange(event) {
    this.loadList(event.pageInfo.offset, event.pageInfo.limit);
  }

  execute(event: { element: any; rawData?: any; method: string; service?: MasterListService; methodKey?: string }) {
    // console.log(event);
    switch (event.method) {
      case 'add':
        this.router.navigate(['/pages/product-management/brand/0']);
        break;
      case 'edit':
        console.log('edit',event);
        this.router.navigate(['/pages/product-management/brand/' + event.element.id]);
        break;
      case 'view':
        break;
    }
  }
}
