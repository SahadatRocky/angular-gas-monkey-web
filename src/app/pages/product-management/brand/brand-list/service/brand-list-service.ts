import {Injectable} from '@angular/core';
import { MasterListService } from '../../../../../containers/core/master-component/master-service/master-list-service';
import { TextData } from '../../../../shared/model/common/text-data';


@Injectable({
  providedIn: 'root'
})
export class BrandListService extends MasterListService {
  constructor() {
    super();
    this.fieldInfo = {
      colWidth: 4,
      listTitleEn: 'Brand',
      listTitleBn: 'ব্র্যান্ড তালিকা',
      fields: [

        new TextData({
          key: 'companyName',
          fieldWidth: 4,
          name: 'companyName',
          id: 'companyName',
          label: 'Company Name',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 1
        }),
        new TextData({
          key: 'imageLink',
          fieldWidth: 4,
          name: 'imageLink',
          id: 'imageLink',
          label: 'Photo',
          type: 'img',
          canShow: true,
          canSort: true,
          order: 2
        }),
        new TextData({
          key: 'nameEn',
          fieldWidth: 4,
          name: 'nameEn',
          id: 'nameEn',
          label: 'Brand Name',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 3
        }),

        new TextData({
          key: 'discountType',
          fieldWidth: 4,
          name: 'discountType',
          id: 'discountType',
          label: 'Discount Type',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 4
        }),
        new TextData({
          key: 'discountValue',
          fieldWidth: 4,
          name: 'discountValue',
          id: 'discountValue',
          label: 'Discount Value',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 5
        }),
        new TextData({
          key: 'discountEnabled',
          fieldWidth: 4,
          name: 'discountEnabled',
          id: 'discountEnabled',
          label: 'Discount Enable',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 6
        }),
        new TextData({
          key: 'discountStartDate',
          fieldWidth: 4,
          name: 'discountStartDate',
          id: 'discountStartDate',
          label: 'Discount Start Time',
          type: 'date',
          canShow: true,
          canSort: true,
          order: 7
        }),

        new TextData({
          key: 'discountEndDate',
          fieldWidth: 4,
          name: 'discountEndDate',
          id: 'discountEndDate',
          label: 'Discount End Time',
          type: 'date',
          canShow: true,
          canSort: true,
          order: 8
        }),

        new TextData({
          key: 'status',
          fieldWidth: 4,
          name: 'status',
          id: 'status',
          label: 'Status',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 9
        }),
        new TextData({
          key: 'action',
          fieldWidth: 4,
          name: 'অ্যাকশন',
          id: 'action',
          label: 'Action',
          // type: 'text',
          canShow: true,
          canSort: true,
          order: 10
        })
      ]
    };
  }
}
