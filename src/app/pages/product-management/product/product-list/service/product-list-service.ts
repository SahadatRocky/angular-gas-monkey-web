import {Injectable} from '@angular/core';
import { MasterListService } from '../../../../../containers/core/master-component/master-service/master-list-service';
import { TextData } from '../../../../shared/model/common/text-data';


@Injectable({
  providedIn: 'root'
})
export class ProductListService extends MasterListService {
  constructor() {
    super();
    this.fieldInfo = {
      colWidth: 4,
      listTitleEn: 'Product',
      listTitleBn: 'তালিকা',
      fields: [
        new TextData({
          key: 'productCode',
          fieldWidth: 4,
          name: 'code',
          id: 'code',
          label: 'Code',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 1
        }),
        new TextData({
          key: 'productPhoto',
          fieldWidth: 4,
          name: 'productPhoto',
          id: 'productPhoto',
          label: 'Photo',
          type: 'img',
          canShow: true,
          canSort: true,
          order: 2
        }),
        new TextData({
          key: 'companyName',
          fieldWidth: 4,
          name: 'companyName',
          id: 'companyName',
          label: 'Company Name',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 3
        }),
        new TextData({
          key: 'brandNameEn',
          fieldWidth: 4,
          name: 'brandName',
          id: 'brandName',
          label: 'Brand Name',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 4
        }),
        new TextData({
          key: 'productSizeEn',
          fieldWidth: 4,
          name: 'size',
          id: 'size',
          label: 'Size',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 5
        }),
        new TextData({
          key: 'valveSizeEn',
          fieldWidth: 4,
          name: 'valveSize',
          id: 'valveSize',
          label: 'Valve Size',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 6
        }),
        new TextData({
          key: 'refillPrice',
          fieldWidth: 4,
          name: 'refillPrice',
          id: 'refillPrice',
          label: 'Refill Price',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 8
        }),
        new TextData({
          key: 'emptyCylinderPrice',
          fieldWidth: 4,
          name: 'emptyCylinderPrice',
          id: 'emptyCylinderPrice',
          label: 'Empty Cylinder Price',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 7
        }),
        new TextData({
          key: 'packagePrice',
          fieldWidth: 4,
          name: 'packagePrice',
          id: 'packagePrice',
          label: 'Package Price',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 8
        }),
        new TextData({
          key: 'offerProduct',
          fieldWidth: 4,
          name: 'offerProduct',
          id: 'offerProduct',
          label: 'Offer Product',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 9
        }),
        new TextData({
          key: 'featureProduct',
          fieldWidth: 4,
          name: 'featureProduct',
          id: 'featureProduct',
          label: 'Feature Product',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 10
        }),
        new TextData({
          key: 'status',
          fieldWidth: 4,
          name: 'status',
          id: 'status',
          label: 'Status',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 11
        }),

        new TextData({
          key: 'action',
          fieldWidth: 4,
          name: 'অ্যাকশন',
          id: 'action',
          label: 'Action',
          // type: 'text',
          canShow: true,
          canSort: true,
          order: 11
        })
      ]
    };
  }
}
