import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ServiceListControlWrapper } from '../../../../containers/core/master-component/master-model/service-list-control-wrapper';
import { MasterListService } from '../../../../containers/core/master-component/master-service/master-list-service';
import { UtilService } from '../../../shared/service/util.service';
import DataUtils from '../../../shared/utils/data-utils';
import { ProductService } from '../../service/product.service';
import { ProductButtonService } from './service/product-button-service';
import { ProductListService } from './service/product-list-service';
@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
  data: any;
  isShowLoading = false;
  dataSource = new MatTableDataSource();
  dataSize = 10;
  serviceListControlWrapper: ServiceListControlWrapper<
    ProductListService,
    ProductButtonService
    >;
  constructor(private router: Router,
    private utilService: UtilService,
    private productListService: ProductListService,
    private productButtonService: ProductButtonService,
    private productService : ProductService) { }

    ngOnInit(): void {
      this.loadList(0,this.dataSize);
    }

    loadList(offset, limit) {
      this.productService.getProductListTableData(offset,limit).subscribe((res) =>{
        // console.log("------------product-list-data------------",res);
        this.data = DataUtils.flattenData(res.content);
        this.serviceListControlWrapper = new ServiceListControlWrapper<
        ProductListService,
        ProductButtonService
          >(
            this.productListService,
            this.data,
            this.productButtonService,
            res.totalElements,
            { pageSize: res.size, pageIndex: res.number }
          );
      });
    }

    execute(event: { element: any; rawData?: any; method: string; service?: MasterListService; methodKey?: string }) {
      console.log(event);
      switch (event.method) {
        case 'add':
          this.router.navigate(['/pages/product-management/product/0']);
          break;
        case 'edit':
          console.log('edit',event);
          this.router.navigate(['/pages/product-management/product/' + event.element.id]);
          break;
        case 'view':
          break;
      }
    }

    onPageChange(event) {
      this.loadList(event.pageInfo.offset, event.pageInfo.limit);
    }
}
