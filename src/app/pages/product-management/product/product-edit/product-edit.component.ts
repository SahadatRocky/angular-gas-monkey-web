import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { OBJ_EDIT, OBJ_NEW } from '../../../../constants/gas-monkey-constants';
import { FileUploader } from 'ng2-file-upload';
import { UtilService } from '../../../shared/service/util.service';
import { StoreService } from '../../../store-management/service/store.service';
import { ProductService } from '../../service/product.service';
import { DatePipe } from '@angular/common';
import { DialogImageViewerComponent } from '../../../shared/components/dialog-image-viewer/dialog-image-viewer.component';
import { MatDialog } from '@angular/material/dialog';
@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.scss'],
  providers: [DatePipe]
})
export class ProductEditComponent implements OnInit {

  @Input() selectedProductObj: any;
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  productImageList = [];
  productForm: FormGroup;
  seoDetailsForm: FormGroup;
  productPriceForm:FormGroup;
  productImageForm:FormGroup;
  displayedColumns: string[];
  dataSource = new MatTableDataSource();
  buttonText: string;
  actionMode: string;
  isShowLoading = false;
  productId:any;
  languageList:any;
  selectedFiles:any;
  checkedSpecialPrice:boolean=false;
  productSizeList:any;
  BulbTypeList: any;
  BrandList: any;
  formData:any;
  productPriceId:any;
  productPriceStatus:any;
  productShortCode:string="";
  productSizeCode:string="";
  productValveCode:string="";
  productCodeValue:string="";
  selectedBrandId:string;

  constructor(private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private productService : ProductService,
    private storeService: StoreService,
    private utilService: UtilService,
    private datePipe: DatePipe,
    public dialog: MatDialog
        ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(
      params => {
        this.productId = params.id;
        this.buttonText = this.productId != '0' ? 'UPDATE' : 'SAVE';
        this.actionMode = this.productId != '0' ? OBJ_EDIT : OBJ_NEW;
      });

      this.createProductDefinationFormGroup();
      this.createproductPriceFormGroup();
      this.createProductImageForm();
      this.generateProductSizeDropdownListData();
      this.generateValveSizeDropdownListData();
      this.generateBrandDropdownListData();
      // this.checkedSpecialPrice =  this.productPriceForm.controls.specialPriceChecked.value;

      if(this.actionMode == OBJ_EDIT){
        this.getProductDataById();
        //this.userForm.controls['email'].disable();
      }else{
        // this.isShowLoading = false;
        // this.userForm.controls['userId'].enable();
      }

  }

  generateProductSizeDropdownListData(){
    //console.log("===========called==========");
    this.productService.getProductSizeDropDownList().subscribe(res=>{
     // console.log('productsize-dropdownlist-',res);
      this.productSizeList=res;

    });
  }

  generateValveSizeDropdownListData(){
    this.productService.getProductValveSizeDropDownList().subscribe(res=>{
      console.log('vulveSize-dropdownlist-',res);
      this.BulbTypeList=res;

    });
  }

  generateBrandDropdownListData(){
    this.productService.getProductBrandDropDownList().subscribe(res=>{
      //console.log('BrandDropList-dropdownlist-',res);
      this.BrandList=res;

    });
  }



  getProductDataById(){
   //this.populateData(this.ELEMENT_DATA);
    this.isShowLoading = true;
    this.productService.getProductDataById(this.productId).subscribe(res => {
      console.log(res);
      this.populateData(res);
      this.populatePriceData(res);
      this.populateImageDataData(res);
      this.isShowLoading = false;
    })
  }


  createproductPriceFormGroup(){
    this.productPriceForm = this.fb.group({
      refillPrice: ['', Validators.required],
      emptyCylinderPrice: ['', Validators.required],
      packagePrice:['',Validators.required]
    } );
  }

  createProductImageForm(){
    this.productImageForm = this.fb.group({
      featureProduct: [false],
      offerProduct: [false],
    });
  }


  createProductDefinationFormGroup(){
    this.productForm = this.fb.group({
      companyName: [''],
      brandId: ['', Validators.required],
      productSizeId: ['', Validators.required],
      productValveSizeId: ['', Validators.required],
      code:['', Validators.required],
      status:[false]

    });
  }

  populateData(myData) {
    //console.log("-------------------product-form-data--------------",myData);
    this.productForm.patchValue(myData);
    this.productForm.get('companyName').setValue(myData.brand.companyName);
    this.productForm.get('brandId').setValue(myData.brand.id);
    this.productForm.get('productSizeId').setValue(myData.productSize.id);
    this.productForm.get('productValveSizeId').setValue(myData.productValveSize.id);
  }

  populatePriceData(myData) {

    if(myData){
      this.productPriceId = myData.productPrice?.id ? myData.productPrice?.id : "";
      this.productPriceStatus = myData.productPrice?.status ? myData.productPrice?.status : false;
      this.productPriceForm.patchValue(myData.productPrice);
    }
  }

  populateImageDataData(myData) {
    if(myData){
      this.productImageList = myData.productImageList;
      this.productImageForm.get('featureProduct').setValue(myData.featureProduct);
      this.productImageForm.get('offerProduct').setValue(myData.offerProduct);
    }
  }

  imageUpdate(id:any , obj:any){
    this.productService.updateProductImage(id,obj).subscribe(res =>{
      this.getProductDataById();
      this.utilService.showSnackBarMessage(' Product Image Update Successfully',this.utilService.TYPE_MESSAGE.SUCCESS_TYPE);
    });
  }

  onToggleChnage(item:any,checked:any){
    console.log(item);
    console.log(checked);
    let obj = {
      imageLink: item.imageLink,
      imageLinkId: "",
      status: checked,
      title: item.title,
    };
    this.imageUpdate(item.id, obj);
  }


  uploader: FileUploader = new FileUploader({
    disableMultipart : false,
    method: 'post',
    itemAlias: 'attachment',
    allowedFileType: ['image', 'video']
    });

  onFileSelected(event: EventEmitter<File>) {
    const file: any = event;
    const title = file[0].name;
    const formData = new FormData();
    formData.append('file', file[0]);
    //console.log(formData.get('file'));
    this.productService.uploadProductImage(formData).subscribe(res=>{
      //console.log('----------------------img-response-------------',res);
        if(res.status == 200){
              console.log(title);
              let obj ={
                "imageLink":res.body ,
                "imageLinkId": "",
                "status": true,
                "productId": this.productId,
                "title": title
            };
            this.productService.createProductImage(obj).subscribe(res=> {
              console.log("create product image", res);
              this.getProductDataById();
            });
        }
      });
  }

  onSubmit(){

    if(this.actionMode == OBJ_EDIT){
      let obj = {
        "id": this.productId,
        "companyName": this.productForm.value.companyName,
        "brandId": this.productForm.value.brandId,
        "productSizeId": this.productForm.value.productSizeId,
        "productValveSizeId": this.productForm.value.productValveSizeId,
        "code": this.productForm.value.code,
        "status": this.productForm.value.status,
        "productPriceBean": {
            "refillPrice": this.productPriceForm.value.refillPrice,
            "emptyCylinderPrice":this.productPriceForm.value.emptyCylinderPrice,
            "packagePrice":this.productPriceForm.value.packagePrice,
            "status":this.productPriceStatus,
            "productId": this.productId
        },
        "featureProduct": this.productImageForm.value.featureProduct? this.productImageForm.value.featureProduct : false,
        "offerProduct": this.productImageForm.value.offerProduct ? this.productImageForm.value.offerProduct : false
    }

    if(this.productPriceId){
      obj.productPriceBean['id'] = this.productPriceId;
    }
      this.productService.productUpdate(this.productId,obj).subscribe(res =>{
        console.log("-------------product-update-data-------------",res);
        this.utilService.showSnackBarMessage('Product Updated Successfully',this.utilService.TYPE_MESSAGE.SUCCESS_TYPE);
        this.router.navigate(['/pages/product-management/product']);
      },error => {
        this.utilService.showSnackBarMessage(error.message,this.utilService.TYPE_MESSAGE.ERROR_TYPE);
      });

    }else{
      let obj = {
        "companyName": this.productForm.value.companyName,
        "brandId": this.productForm.value.brandId,
        "productSizeId": this.productForm.value.productSizeId,
        "productValveSizeId": this.productForm.value.productValveSizeId,
        "code": this.productForm.value.code,
        "status": true
      }
      this.productService.productCreate(obj).subscribe(res=>{
        console.log("-------------product-create-data-------------",res);
        this.utilService.showSnackBarMessage(' Product Created Successfully',this.utilService.TYPE_MESSAGE.SUCCESS_TYPE);
        this.router.navigate(['/pages/product-management/product']);
    },error => {
      this.utilService.showSnackBarMessage(error.message,this.utilService.TYPE_MESSAGE.ERROR_TYPE);
    });
    }
  }

  setTableData(list: any) {
    this.dataSource.data = list;
  }

  // onchangeSpicalPrice(event){
  //     this.checkedSpecialPrice = event.checked;
  //     this.productPriceForm.get('isSpecialPrice').setValue(event.checked);
  // }

  goToList(){
    this.router.navigate(['/pages/product-management/product']);
  }

  onProductValveSizeChange(event){
    // console.log(event);
     let valve = this.BulbTypeList.find(o => o.id === event.value);
     this.productValveCode =valve.value.split(" ")[0];
     this.productCodeValue=this.productShortCode+this.productSizeCode+this.productValveCode;
     this.productForm.patchValue({
       code: this.productCodeValue
     });
  }

  onProductSizeChange(event){
    //console.log(event);
    let valve = this.productSizeList.find(o => o.id === event.value);
    this.productSizeCode =valve.value.split(" ")[0];
    this.productCodeValue=this.productShortCode+this.productSizeCode+this.productValveCode;
    this.productForm.patchValue({
      code: this.productCodeValue
    });
  }

  checkDisable(){
    //console.log("check------");
    if(this.actionMode == 'edit'){
      return !(this.productForm.valid && this.productPriceForm.valid);
    }else{
      return !this.productForm.valid;
    }
  }

  onBrandChange(event){
    //brand-set-company
    this.productService.getBrandById(event.value).subscribe(res =>{
      console.log("-------------brand-data-------------",res);
      this.productShortCode =res.shortName;
      this.productCodeValue=this.productShortCode+this.productSizeCode+this.productValveCode;
      this.productForm.patchValue({
        companyName: res.companyName,
        code: this.productCodeValue
      });
    },error => {
      this.utilService.showSnackBarMessage(error.message,this.utilService.TYPE_MESSAGE.ERROR_TYPE);
    });
  }

  onProductImageDelete(item:any){
      this.productService.deleteProductImage(item.id).subscribe(res=>{
        if(res){
          this.getProductDataById();
        this.utilService.showSnackBarMessage(' Product Image Delete Successfully',this.utilService.TYPE_MESSAGE.SUCCESS_TYPE);
        }
      });
  }

  openImageDialog(event){
    console.log(event.srcElement.src);
    const dialogRef = this.dialog.open(DialogImageViewerComponent, {

      // disableClose: true,
      width: '550px',
      height: '550px',
      data: {
        title: 'Image',
        obj: event.srcElement.src
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('image closed');
    });

  }

  get companyName() {
    return this.productForm.get('companyName');
  }

  get nameEn() {
    return this.productForm.get('nameEn');
  }
  get nameBn() {
    return this.productForm.get('nameBn');
  }

  get descriptionEn() {
    return this.productForm.get('descriptionEn');
  }

  get descriptionBn() {
    return this.productForm.get('descriptionBn');
  }

  get code() {
    return this.productForm.get('code');
  }

  get brandId() {
    return this.productForm.get('brandId');
  }

  get productSizeId() {
    return this.productForm.get('productSizeId');
  }

  get productValveSizeId() {
    return this.productForm.get('productValveSizeId');
  }

  get featureProduct(){
    return this.productImageForm.get('featureProduct');
  }

  get offerProduct(){
    return this.productImageForm.get('offerProduct');
  }

  get refillPrice(){
    return this.productPriceForm.get('refillPrice');
  }

  get emptyCylinderPrice(){
    return this.productPriceForm.get('emptyCylinderPrice');
  }

  get isSpecialPrice(){
    return this.productPriceForm.get('isSpecialPrice');
  }

  get specialPrice(){
    return this.productPriceForm.get('specialPrice');
  }

  get packagePrice(){
    return this.productPriceForm.get('packagePrice');
  }

  get specialPriceStartDate(){
    return this.productPriceForm.get('specialPriceStartDate');
  }

  get specialPriceEndDate(){
    return this.productPriceForm.get('specialPriceEndDate');
  }

}
