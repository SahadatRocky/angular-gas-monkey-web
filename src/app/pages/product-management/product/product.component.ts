import { Component, OnInit } from '@angular/core';
import { OBJ_EDIT, OBJ_LIST, OBJ_NEW } from '../../../constants/gas-monkey-constants';
import { EmitterService } from '../../shared/service/emitter.service';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  mode: any;
  selectedProductObj: any;

  constructor(private emitterService: EmitterService,
    ) { }

  ngOnInit(): void {
    this.mode = OBJ_LIST;
    this.listenEmittedApiObj();
  }

  listenEmittedApiObj(){
    this.emitterService.selectedProductObj.subscribe(data=>{
      if(data){
        if(data.actionMode == OBJ_EDIT){
          this.mode = OBJ_EDIT;
        }else if(data.actionMode == OBJ_NEW){
           this.mode = OBJ_NEW;
        }else{
          this.mode = OBJ_LIST;
        }
        this.selectedProductObj = data;
      }else{
        this.mode = OBJ_LIST;
      }
    })
  }

}
