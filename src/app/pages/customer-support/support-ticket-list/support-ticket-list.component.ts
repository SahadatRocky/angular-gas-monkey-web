import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ServiceListControlWrapper } from '../../../containers/core/master-component/master-model/service-list-control-wrapper';
import { MasterListService } from '../../../containers/core/master-component/master-service/master-list-service';
import { UtilService } from '../../shared/service/util.service';
import DataUtils from '../../shared/utils/data-utils';
import { ReplyComponent } from '../reply/reply.component';
import { SupportTicketService } from '../support-ticket.service';
import { SupportTicketButtonService } from './service/support-ticket-button.service';
import { SupportTicketListService } from './service/support-ticket-list.service';

@Component({
  selector: 'app-support-ticket-list',
  templateUrl: './support-ticket-list.component.html',
  styleUrls: ['./support-ticket-list.component.scss']
})
export class SupportTicketListComponent implements OnInit {

  // ELEMENT_DATA: any = [
  //   {  customerId: '1',issueTypeId: '1',details: '1', note : ' ngladesh. It is the first Govt Approved private LP Gas plant to be .', issueStatus: '0',status: 'active',}
  // ];

  data: any;
  isShowLoading = false;
  dataSource = new MatTableDataSource();
  dataSize = 10;
  serviceListControlWrapper: ServiceListControlWrapper<
  SupportTicketListService,
  SupportTicketButtonService
    >;

  constructor(
    private supportTicketService: SupportTicketService,
    private utilService: UtilService,
    private supportTicketListService: SupportTicketListService,
    private supportTicketButtonService: SupportTicketButtonService,
    public dialog: MatDialog,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.loadList(0,this.dataSize);
   }

   loadList(offset, limit) {
    this.supportTicketService.getSupportTicketTableData(offset,limit).subscribe((res) =>{
      console.log('***',res.content);
       this.data = DataUtils.flattenData(res.content);
       this.serviceListControlWrapper = new ServiceListControlWrapper<
       SupportTicketListService,
      SupportTicketButtonService
         >(
           this.supportTicketListService,
           this.data,
           this.supportTicketButtonService,
           res.totalElements,
          { pageSize: res.size, pageIndex: res.number }
         );
     });
   }

  onPageChange(event) {
    this.loadList(event.pageInfo.offset, event.pageInfo.limit);
  }

  execute(event: { element: any; rawData?: any; method: string; service?: MasterListService; methodKey?: string }) {
   console.log(event);
   switch (event.method) {
     case 'reply':
      this.replayDialog(event.element);
       break;
   }
 }

 replayDialog(data) {
  console.log(data);
  const dialogRef = this.dialog.open(ReplyComponent, {
    width: '800px',
    height: '400px',
    data: {
       title:'reply',
       obj: data
    },
  });
  dialogRef.afterClosed().subscribe(result => {
    this.loadList(0,this.dataSize);
    console.log('The dialog was closed');
  });
}


}
