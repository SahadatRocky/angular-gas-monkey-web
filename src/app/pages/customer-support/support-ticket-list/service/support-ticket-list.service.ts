import { Injectable } from '@angular/core';
import { MasterListService } from '../../../../containers/core/master-component/master-service/master-list-service';
import { TextData } from '../../../shared/model/common/text-data';

@Injectable({
  providedIn: 'root'
})
export class SupportTicketListService extends MasterListService {
  constructor() {
    super();
    this.fieldInfo = {
      colWidth: 4,
      listTitleEn: 'Report Issue List',
      listTitleBn: 'উজারের তালিকা',
      fields: [
        new TextData({
          key: 'customer.customerName',
          fieldWidth: 4,
          name: 'customerName',
          id: 'customerName',
          label: 'Customer Name',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 1
        }),
        new TextData({
          key: 'issueType.title',
          fieldWidth: 4,
          name: 'title',
          id: 'title',
          label: 'Issue Type title',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 2
        }),
        new TextData({
          key: 'details',
          fieldWidth: 4,
          name: 'details',
          id: 'details',
          label: 'Details',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 3
        }),
        new TextData({
          key: 'note',
          fieldWidth: 4,
          name: 'note',
          id: 'note',
          label: 'Note',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 4
        }),
        new TextData({
          key: 'issueStatus',
          fieldWidth: 4,
          name: 'issueStatus',
          id: 'issueStatus',
          label: 'Issue Status',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 5
        }),
        new TextData({
          key: 'status',
          fieldWidth: 4,
          name: 'status',
          id: 'status',
          label: 'Status',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 6
        }),
        new TextData({
          key: 'action',
          fieldWidth: 4,
          name: 'অ্যাকশন',
          id: 'action',
          label: 'Action',
          // type: 'text',
          canShow: true,
          canSort: true,
          order: 7
        })
      ]
    };
  }
}
