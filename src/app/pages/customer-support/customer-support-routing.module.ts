import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SupportTicketListComponent } from './support-ticket-list/support-ticket-list.component';


const routes: Routes = [
  {
    path: "",
    data: {
      title: "customer-support",
    },
    children: [
      {
        path: "",
        redirectTo: "support-ticket-list",
      },
      {
        path: "support-ticket-list",
        component: SupportTicketListComponent,
        data: {
          title: "support-ticket-list",
        },
      },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerSupportRoutingModule { }
