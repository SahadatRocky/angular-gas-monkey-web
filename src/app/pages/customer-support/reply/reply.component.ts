import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilService } from '../../shared/service/util.service';
import { SupportTicketService } from '../support-ticket.service';

@Component({
  selector: 'app-reply',
  templateUrl: './reply.component.html',
  styleUrls: ['./reply.component.scss']
})
export class ReplyComponent implements OnInit {

  replayForm: FormGroup;
  userId:any;

  constructor(private activatedRoute: ActivatedRoute,
    private supportTicketService: SupportTicketService,
    private fb: FormBuilder,
    private router: Router,
    public dialogRef: MatDialogRef<ReplyComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private utilService: UtilService
    ) { }

  ngOnInit(): void {
    this.createFormGroup();
    console.log('Reply--',this.data.obj);
    this.userId = this.data.obj.id;
  }

  createFormGroup(){
    this.replayForm = this.fb.group({
      note: ["", [Validators.required]],
    });
  }

  onNoClick(){
    this.dialogRef.close();
  }

 onSubmit(){

    let obj = {
      "id": this.data.obj.id,
      "note": this.replayForm.value.note
    }
    console.log('submit--',obj);
    this.supportTicketService.updateSupportTicket(this.data.obj.id,obj).subscribe(
      (res) => {
        this.utilService.showSnackBarMessage(
          "Reply update Successfully",
          this.utilService.TYPE_MESSAGE.SUCCESS_TYPE
        );
      this.dialogRef.close();
      },
      (error) => {
        this.utilService.showSnackBarMessage(
          error.message,
          this.utilService.TYPE_MESSAGE.ERROR_TYPE
        );
      }
    );
 }

 goToList(){
  this.router.navigate(['/pages/customer-support/support-ticket-list']);
}

get note() {
  return this.replayForm.get("note");
 }

}
