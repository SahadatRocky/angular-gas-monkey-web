import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomerSupportRoutingModule } from './customer-support-routing.module';
import { SupportTicketListComponent } from './support-ticket-list/support-ticket-list.component';
import { ReplyComponent } from './reply/reply.component';
import { SharedModule } from '../shared/shared.module';




@NgModule({
  declarations: [SupportTicketListComponent, ReplyComponent],
  imports: [
    CommonModule,
    CustomerSupportRoutingModule,
    SharedModule
  ]
})
export class CustomerSupportModule { }
