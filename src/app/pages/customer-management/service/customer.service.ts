import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiEndpoints } from '../../../api-endpoints';
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  HTTPOptions:Object = {
    observe: 'response',
    responseType: 'text'
 }


  private apiEndpoints: ApiEndpoints = new ApiEndpoints();

  constructor(private http: HttpClient) { }

  getCustomerListTableData(page: number, size: number): Observable<any> {
    let url = this.apiEndpoints.CUSTOMER_LIST_URL;
    const fullUrl = `${url}?page=${page}&size=${size}`;
    return this.http.get<any>(fullUrl).pipe(
      map(value => value)
    );
  }

  getCustomerTypeList(): Observable<any> {
    let url = this.apiEndpoints.CUSTOMER_TYPE_DROPDOWN_LIST;
    return this.http.get<any>(url).pipe(
      map(value => value)
    );
  }

  getCustomerById(id:any): Observable<any> {
    let url = this.apiEndpoints.CUSTOMER_LIST_URL + '/' + id;
    return this.http.get<any>(url).pipe(
      map(value => value)
    );
  }

  customerCreate(Obj: any): Observable<any>{
    let url = this.apiEndpoints.CUSTOMER_LIST_URL;
    return this.http.post<any>(url, Obj).pipe(map(value => value))
  }

  customerUpdate(id:any, obj:any): Observable<any>{
    let url = this.apiEndpoints.CUSTOMER_LIST_URL + '/' + id;
    return this.http.put<any>(url,obj).pipe(
      map(value => value)
    );
  }

  getCustomerDistrictDropList(): Observable<any> {
    let url = this.apiEndpoints.CUSTOMER_DISTRICT_DROPDOWN_LIST;
    return this.http.get<any>(url).pipe(
      map(value => value)
    );
  }

  getCustomerThanaDropList(districtId:any): Observable<any> {
    let url = this.apiEndpoints.CUSTOMER_THANA_DROPDOWN_LIST + '/' + districtId;
    return this.http.get<any>(url).pipe(
      map(value => value)
    );
  }

  getCustomerClusterDropList(thanaId:any): Observable<any> {
    let url = this.apiEndpoints.CUSTOMER_CLUSTER_DROPDOWN_LIST + '/' + thanaId;
    return this.http.get<any>(url).pipe(
      map(value => value)
    );
  }

}
