import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { OBJ_EDIT, OBJ_NEW } from "../../../constants/gas-monkey-constants";
import { UtilService } from "../../shared/service/util.service";
import { CustomerService } from "../service/customer.service";

@Component({
  selector: "app-customer-edit",
  templateUrl: "./customer-edit.component.html",
  styleUrls: ["./customer-edit.component.scss"],
})
export class CustomerEditComponent implements OnInit {
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$";
  customerForm: FormGroup;
  buttonText: string;
  customerId: any;
  actionMode: string;
  customerTypeList: any;
  districtList: any;
  thanaList: any;
  clusterList: any;
  floorList: any = [
    { id: "1", value: "Level-1" },
    { id: "2", value: "Level-2" },
    { id: "3", value: "Level-3" },
    { id: "4", value: "Level-4" },
    { id: "5", value: "Level-5" },
    { id: "6", value: "Level-6" },
    { id: "7", value: "Level-7" },
    { id: "8", value: "Level-8" },
    { id: "9", value: "Level-9" },
    { id: "10", value: "Level-10" },
    { id: "11", value: "Level-11" },
    { id: "12", value: "Level-12" },
    { id: "13", value: "Level-13" },
    { id: "14", value: "Level-14" },
    { id: "15", value: "Level-15" },
    { id: "16", value: "Level-16" },
    { id: "17", value: "Level-17" },
    { id: "18", value: "Level-18" },
    { id: "19", value: "Level-19" },
    { id: "20", value: "Level-20" },
  ];

  // ELEMENT_DATA = {
  //   firstNameEng: "Sahadat",
  //   firstNameBan: "সাহাদাত",
  //   lastNameEng: "Hossain",
  //   lastNameBan: "হোসেন",
  //   latitute: "23.346656",
  //   longitude: "90.56785",
  //   lift: true,
  //   floorId: "Level-5",
  //   phoneNumber: "01686953176",
  //   email: "sahadat@batworld.com",
  //   address: "Uttara, Sector-13, Road-13, Dhaka",
  //   area: "Sector-13",
  //   thanaId: "1",
  //   districtId: "1",
  //   company: "Gas monkey",
  //   customerTypeId: "1",
  // };

  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private customerService: CustomerService,
    private utilService: UtilService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      this.customerId = params.id;
      // console.log(this.storeId);
      this.buttonText = this.customerId != "0" ? "UPDATE" : "SAVE";
      this.actionMode = this.customerId != "0" ? OBJ_EDIT : OBJ_NEW;
    });
    this.createCustomerFormGroup();
    this.getCustomerTypeList();
    this.getCustomerDistrictList();

    if (this.actionMode == OBJ_EDIT) {
      this.getCustomerById();

      //this.userForm.controls['email'].disable();
    } else {
      // this.isShowLoading = false;
      // this.userForm.controls['userId'].enable();
    }

  }

  createCustomerFormGroup() {
    this.customerForm = this.fb.group({
      customerName: ["", [Validators.required]],
      customerTypeId: ["",[Validators.required]],
      companyName: [""],
      districtId: ["", Validators.required],
      thanaId: ["", Validators.required],
      clusterId: ["", Validators.required],
      area: ["",[Validators.required]],
      address: ["",[Validators.required]],
      emailAddress: ["", [Validators.required, Validators.pattern(this.emailPattern)]],
      phoneNo: ["", [Validators.required,Validators.pattern("[0-9 ]{11}")]],
      floorNo: ["",[Validators.required]],
      liftAllowed: [false],
      latitude: ["", [Validators.required]],
      longitude: ["", [Validators.required]],
      status: [false],
    });
  }

  getCustomerById() {
   // this.populateData(this.ELEMENT_DATA);
    this.customerService.getCustomerById(this.customerId).subscribe(res => {
      console.log(res);
      this.populateData(res);
    })
  }

  getCustomerTypeList() {
    this.customerService.getCustomerTypeList().subscribe((res) => {
      this.customerTypeList = res;
    });
  }

  getCustomerDistrictList() {
    this.customerService.getCustomerDistrictDropList().subscribe((res) => {
      this.districtList = res;
    });
  }

  getCustomerThanaListByDistrictId(id: any) {
    this.customerService.getCustomerThanaDropList(id).subscribe((res) => {
      this.thanaList = res;
    });
  }

  getCustomerClusterListByThanaId(id: any) {
    this.customerService.getCustomerClusterDropList(id).subscribe((res) => {
      this.clusterList = res;
    });
  }

  populateData(myData) {
    this.customerForm.patchValue(myData);
  }

  onSubmit() {
    console.log("clicked"+ this.customerForm.valid);
      if (this.actionMode == OBJ_EDIT) {
        let obj = {
          "id": this.customerId,
          "customerName": this.customerForm.value.customerName,
          "emailAddress": this.customerForm.value.emailAddress,
          "phoneNo": this.customerForm.value.phoneNo,
          "customerTypeId" : this.customerForm.value.customerTypeId,
          "companyName": this.customerForm.value.companyName,
          "status": this.customerForm.value.status,
          "districtId": this.customerForm.value.districtId,
          "thanaId": this.customerForm.value.thanaId,
          "clusterId" :this.customerForm.value.clusterId,
          "area" : this.customerForm.value.area,
          "address": this.customerForm.value.address,
          "latitude": this.customerForm.value.latitude,
          "longitude": this.customerForm.value.longitude,
          "floorNo": this.customerForm.value.floorNo,
          "liftAllowed": this.customerForm.value.liftAllowed
        };
        this.customerService.customerUpdate(this.customerId,obj).subscribe(res =>{
            console.log("-------------customer-update-data-------------",res);
            this.utilService.showSnackBarMessage('Customer Updated Successfully',this.utilService.TYPE_MESSAGE.SUCCESS_TYPE);
            this.router.navigate(['/pages/customer-management/customers']);
          },error => {
            this.utilService.showSnackBarMessage(error.message,this.utilService.TYPE_MESSAGE.ERROR_TYPE);
          });
      } else {
        let obj = {
          "customerName": this.customerForm.value.customerName,
          "emailAddress": this.customerForm.value.emailAddress,
          "phoneNo": this.customerForm.value.phoneNo,
          "customerTypeId" : this.customerForm.value.customerTypeId,
          "companyName": this.customerForm.value.companyName,
          "status": this.customerForm.value.status,
          "districtId": this.customerForm.value.districtId,
          "thanaId": this.customerForm.value.thanaId,
          "clusterId" :this.customerForm.value.clusterId,
          "area" : this.customerForm.value.area,
          "address": this.customerForm.value.address,
          "latitude": this.customerForm.value.latitude,
          "longitude": this.customerForm.value.longitude,
          "floorNo": this.customerForm.value.floorNo,
          "liftAllowed": this.customerForm.value.liftAllowed,
        };

        //console.log("obj--customer", obj);
        this.customerService.customerCreate(obj).subscribe(
          (res) => {
            this.utilService.showSnackBarMessage(
              "Customer Created Successfully",
              this.utilService.TYPE_MESSAGE.SUCCESS_TYPE
            );
            this.router.navigate(["/pages/customer-management/customers"]);
          },
          (error) => {
            this.utilService.showSnackBarMessage(
              error.message,
              this.utilService.TYPE_MESSAGE.ERROR_TYPE
            );
          }
      );
    }
  }

  goToList() {
    this.router.navigate(["/pages/customer-management/customers"]);
  }

  onDistrictChange(event) {
    if(event.value == ""){
      this.customerForm.get('thanaId').setValue("");
      this.customerForm.get('clusterId').setValue("");
    }else{
      this.getCustomerThanaListByDistrictId(event.value);
    }
  }

  onThanaChange(event) {
    if(event.value == ""){
      this.customerForm.get('clusterId').setValue("");
    }else{
      this.getCustomerClusterListByThanaId(event.value);
    }
  }

  get name() {
    return this.customerForm.get("customerName");
  }
  get companyName() {
    return this.customerForm.get("companyName");
  }
  get streetAddress() {
    return this.customerForm.get("streetAddress");
  }
  get city() {
    return this.customerForm.get("city");
  }
  get stateProvince() {
    return this.customerForm.get("stateProvince");
  }
  get postalCode() {
    return this.customerForm.get("postalCode");
  }
  get email() {
    return this.customerForm.get("emailAddress");
  }
  get phoneNumber() {
    return this.customerForm.get("phoneNo");
  }
  get address() {
    return this.customerForm.get("address");
  }
  get latitude() {
    return this.customerForm.get("latitude");
  }
  get longitude() {
    return this.customerForm.get("longitude");
  }

  get thana() {
    return this.customerForm.get("thanaId");
  }

  get cluster() {
    return this.customerForm.get("clusterId");
  }

  get district() {
    return this.customerForm.get("districtId");
  }
  get  customerTypeId() {
    return this.customerForm.get("customerTypeId");
  }
  get  floorNo() {
    return this.customerForm.get("floorNo");
  }
  get  area() {
    return this.customerForm.get("area");
  }

}
