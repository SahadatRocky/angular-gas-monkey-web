import { Component, OnInit } from "@angular/core";
import { MatTableDataSource } from "@angular/material/table";
import { Router } from "@angular/router";
import { ServiceListControlWrapper } from "../../../containers/core/master-component/master-model/service-list-control-wrapper";
import { MasterListService } from "../../../containers/core/master-component/master-service/master-list-service";
import DataUtils from "../../shared/utils/data-utils";
import { CustomerButtonService } from "./service/customer-button-service";
import { CustomerListService } from "./service/customer-list-service";
import { UtilService } from "../../shared/service/util.service";
import { CustomerService } from "../service/customer.service";
import { MatDialog } from "@angular/material/dialog";

@Component({
  selector: "app-customer-list",
  templateUrl: "./customer-list.component.html",
  styleUrls: ["./customer-list.component.scss"],
})
export class CustomerListComponent implements OnInit {
  data: any;
  isShowLoading = false;
  dataSource = new MatTableDataSource();
  dataSize = 10;
  serviceListControlWrapper: ServiceListControlWrapper<
    CustomerListService,
    CustomerButtonService
  >;
  constructor(
    private utilService: UtilService,
    private router: Router,
    private customerService: CustomerService,
    private customerListService: CustomerListService,
    private customerButtonService: CustomerButtonService,
    public dialog: MatDialog,
  ) {}

  ngOnInit(): void {
    this.loadList(0,this.dataSize);
  }

  loadList(offset, limit) {
    this.customerService
      .getCustomerListTableData(offset, limit)
      .subscribe((res) => {
         console.log('--------------customer-list-call-------------',res.content);
        this.data = DataUtils.flattenData(res.content);

        this.serviceListControlWrapper = new ServiceListControlWrapper<
          CustomerListService,
          CustomerButtonService
        >(this.customerListService,
          this.data,
          this.customerButtonService,
          res.totalElements,
          { pageSize: res.size, pageIndex: res.number }
          );
      });
  }

  onPageChange(event) {
    this.loadList(event.pageInfo.offset, event.pageInfo.limit);
  }

  execute(event: {
    element: any;
    rawData?: any;
    method: string;
    service?: MasterListService;
    methodKey?: string;
  }) {
    console.log('>>>',event);
    switch (event.method) {
      case "add":
        this.router.navigate(["/pages/customer-management/customer-edit/0"]);
        break;
      case "edit":
        // console.log('edit',event);
        this.router.navigate(["/pages/customer-management/customer-edit/"+ event.element['id']]);
        break;

    }
  }
}
