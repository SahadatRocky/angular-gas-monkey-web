import {Injectable} from '@angular/core';
import { MasterListService } from '../../../../containers/core/master-component/master-service/master-list-service';
import { TextData } from '../../../shared/model/common/text-data';

@Injectable({
  providedIn: 'root'
})
export class CustomerListService extends MasterListService {
  constructor() {
    super();
    this.fieldInfo = {
      colWidth: 4,
      listTitleEn: 'Customer',
      listTitleBn: 'তালিকা',
      fields: [

        new TextData({
          key: 'slNo',
          fieldWidth: 4,
          name: 'slNo',
          id: 'slNo',
          label: 'SlNo',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 1
        }),
        new TextData({
          key: 'customerName',
          fieldWidth: 4,
          name: 'customerName',
          id: 'customerName',
          label: 'Customer Name',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 2
        }),
        new TextData({
          key: 'customerType',
          fieldWidth: 4,
          name: 'customerType',
          id: 'customerType',
          label: 'Customer Type',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 3
        }),
        new TextData({
          key: 'phoneNo',
          fieldWidth: 4,
          name: 'mobile',
          id: 'mobile',
          label: 'Mobile',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 4
        }),
        new TextData({
          key: 'emailAddress',
          fieldWidth: 4,
          name: 'email',
          id: 'email',
          label: 'Email',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 5
        }),
        new TextData({
          key: 'district',
          fieldWidth: 4,
          name: 'district',
          id: 'district',
          label: 'District ',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 6
        }),

        new TextData({
          key: 'thana',
          fieldWidth: 4,
          name: 'thana',
          id: 'thana',
          label: 'Thana ',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 7
        }),
        new TextData({
          key: 'address',
          fieldWidth: 4,
          name: 'address',
          id: 'address',
          label: 'Address ',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 8
        }),

        new TextData({
          key: 'totalOrderNo',
          fieldWidth: 4,
          name: 'totalOrderNo',
          id: 'totalOrderNo',
          label: 'Total Order',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 9
        }),
        new TextData({
          key: 'lastOrder',
          fieldWidth: 4,
          name: 'lastOrder',
          id: 'lastOrder',
          label: 'Last Order',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 10
        }),
        new TextData({
          key: 'purchaseHistory',
          fieldWidth: 4,
          name: 'purchaseHistory',
          id: 'purchaseHistory',
          label: 'Purchase History',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 11
        }),
        new TextData({
          key: 'rewardPoint',
          fieldWidth: 4,
          name: 'rewardPoint',
          id: 'rewardPoint',
          label: 'Reward Point',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 12
        }),
        new TextData({
          key: 'status',
          fieldWidth: 4,
          name: 'status',
          id: 'status',
          label: 'Status',
          type: 'text',
          canShow: true,
          canSort: true,
          order: 13
        })
      ]
    };
  }
}
