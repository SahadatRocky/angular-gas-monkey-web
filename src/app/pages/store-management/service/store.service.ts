import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { ApiEndpoints } from "../../../api-endpoints";


@Injectable({
  providedIn: 'root'
})
export class StoreService {

  HTTPOptions:Object = {
    observe: 'response',
    responseType: 'text'
 }

  private apiEndpoints: ApiEndpoints = new ApiEndpoints();
  constructor(private http: HttpClient) { }

  storeCreate(Obj: any): Observable<any>{
    let url = this.apiEndpoints.STORE_REQUEST_API;
    return this.http.post<any>(url, Obj,this.HTTPOptions).pipe(map(value => value))
  }

  getStoreTableData(page: number, size: number): Observable<any> {
    let url = this.apiEndpoints.STORE_REQUEST_API;
    const fullUrl = `${url}?page=${page}&size=${size}`;
    return this.http.get<any>(fullUrl).pipe(
      map(value => value)
      );
  }

  getStoreDropdownList(): Observable<any>{
    let url = this.apiEndpoints.STORE_REQUEST_API+"/dropdown-list";
    return this.http.get<any>(url).pipe(
      map(value => value)
    );
  }

  getStoreById(id:any): Observable<any>{
    let url = this.apiEndpoints.STORE_REQUEST_API + '/' + id;
    return this.http.get<any>(url).pipe(
      map(value => value)
      );
  }

  storeUpdate(id:any, obj:any): Observable<any>{
    let url = this.apiEndpoints.STORE_REQUEST_API + '/' + id;
    return this.http.put<any>(url,obj).pipe(
      map(value => value)
    );
  }


}
