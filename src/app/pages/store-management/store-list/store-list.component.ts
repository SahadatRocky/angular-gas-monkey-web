import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { UtilService } from '../../shared/service/util.service';
import { StoreService } from '../service/store.service';

@Component({
  selector: 'app-store-list',
  templateUrl: './store-list.component.html',
  styleUrls: ['./store-list.component.scss']
})
export class StoreListComponent implements OnInit {

  ELEMENT_DATA: any = [
    // { name: 'Cris', email: 'cris@gmail.com', status: 'active'},
    // { name: 'Ponting', email: 'p@gmail.com', status: 'inactive'}
  ];

  public pageSizeOptions = [10,20,100,500];
  displayedColumns: string[];
  paginatorLength: number;
  pageEvent: PageEvent;
  dataSize = 10;
  isShowLoading = false;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  dataSource = new MatTableDataSource();
  constructor(
    private storeService: StoreService,
    private utilService: UtilService,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.setApiColumns();
    this.getStoreList(0, this.dataSize);
  }

  setApiColumns() {
    this.displayedColumns = [ 'storeName' , 'storeEmail', 'storeCode', 'action'];
  }

  getStoreList(page: number, size: number) {
    this.isShowLoading = true;
    this.storeService.getStoreTableData(page,size).subscribe((res) =>{
      this.isShowLoading = false;
      // console.log("store----",res);
      this.setTableData(res.content);
      this.setPagination(res.totalPages, res.totalElements);
    });
  }

  setTableData(list: any) {
    this.dataSource.data = list;
  }

  setPagination(totalPages: number, totalElements: number) {
    this.paginatorLength = totalElements;
  }

  onPaginateChange(pageEvent: PageEvent) {
    this.getStoreList(pageEvent.pageIndex, pageEvent.pageSize);
  }

  sendRowData(event) {
    console.log(event);
    this.router.navigate(['/pages/store-management/store/' + event.id]);
  }

  createNewStore(){
    this.router.navigate(['/pages/store-management/store/0']);
  }

  applyFilter(filterValue: string) {

    if(filterValue.length >= 3){
    //  this.apiBankHelperService.getClientTableDataForSearch(0, this.dataSize, filterValue).subscribe(
    //    res => {
    //      this.isShowLoading = false;
    //      this.setDashboardData(res.data.content);
    //      this.setPagination(res.data.totalPages, res.data.totalElements);
    //    },
    //    error => console.log(error)
    //  );
   }else if(filterValue.length == 0){
    //  this.getClientList(0, this.dataSize);
   }
 }

}
