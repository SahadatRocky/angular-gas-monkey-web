import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundComponent } from '../shared/components/not-found/not-found.component';
import { StoreEditComponent } from './store-edit/store-edit.component';
import { StoreListComponent } from './store-list/store-list.component';
import { StoreComponent } from './store/store.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'merchant-management'
    },
    children: [
      {
        path: 'store',
        component: StoreComponent,
        data: {
          title: 'merchant-list'
        },
      },
      {
        path: 'store-list',
        component: StoreListComponent,
        data: {
          title: 'merchant-list'
        },
      },
      {
        path: 'store/:id',
        component: StoreEditComponent,
        data: {
          title: 'merchant-create-or-edit'
        },
      },
      {
        path: '**',
        component: NotFoundComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StoreManagementRoutingModule { }
