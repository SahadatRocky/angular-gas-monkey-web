import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StoreManagementRoutingModule } from './store-management-routing.module';
import { StoreComponent } from './store/store.component';
import { StoreListComponent } from './store-list/store-list.component';
import { StoreEditComponent } from './store-edit/store-edit.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    StoreComponent,
    StoreListComponent,
    StoreEditComponent
  ],
  imports: [
    CommonModule,
    StoreManagementRoutingModule,
    SharedModule
  ]
})
export class StoreManagementModule { }
