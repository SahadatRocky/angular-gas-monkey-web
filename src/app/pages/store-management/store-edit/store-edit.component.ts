import { DatePipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { OBJ_EDIT, OBJ_NEW } from '../../../constants/gas-monkey-constants';
import { UserService } from '../../role-management/service/user.service';
import { UtilService } from '../../shared/service/util.service';
import { StoreService } from '../service/store.service';


@Component({
  selector: 'app-store-edit',
  templateUrl: './store-edit.component.html',
  styleUrls: ['./store-edit.component.scss']
})
export class StoreEditComponent implements OnInit {

  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  @Input() selectedStoreObj: any;
  storeForm: FormGroup;
  storeId:any;
  storeData:any;
  buttonText: string;
  actionMode: string;
  isShowLoading = false;
  languageList:any;
  merchantList:any;

  weightUnitCode: any = [
    {id: 'LB', value: 'LB'},
    {id: 'KG', value: 'KG'}
  ];
  sizeUnitCode: any = [
    {id: 'IN', value: 'IN'},
    {id: 'CM', value: 'CM'}
  ];

  constructor(
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private storeService: StoreService,
    private userService: UserService,
    private utilService: UtilService,
    private datePipe: DatePipe,
    private router: Router) {
      // this.controlArray[0].setValue(false);
     }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(
      params => {
        this.storeId = params.id;
        // console.log(this.storeId);
        this.buttonText = this.storeId != '0' ? 'UPDATE' : 'SAVE';
        this.actionMode = this.storeId != '0' ? OBJ_EDIT : OBJ_NEW;
      });

      if(this.actionMode == OBJ_EDIT){
        this.getDataStoreById();

        //this.userForm.controls['email'].disable();
      }else{
        // this.isShowLoading = false;
        // this.userForm.controls['userId'].enable();

      }

    this.createFormGroup();
    this.getLanguageDropdownListData();

  }

  createFormGroup(){
    this.storeForm = this.fb.group({
      merchantFirstNameBan: ['',  [Validators.required]],
      merchantFirstNameEng: ['',  [Validators.required]],
      storeCode: ['',  [Validators.required]],
      storeEmail: ['',[Validators.required, Validators.pattern(this.emailPattern)]],
      storePhone:['', [Validators.required]],
      sizeUnitCode: [''],
      languageId:[''],
      storeAddress: ['', [Validators.required]],
      useCache: [false],
      weightUnitCode: [''],
      inBusinessSince:['']
    }
    );
  }

  getDataStoreById(){
    this.isShowLoading = true;
    this.storeService.getStoreById(this.storeId).subscribe(res => {
      console.log(res);
      this.storeData = res;
      this.isShowLoading = false;
      this.populateData(res);
    })
  }

  getLanguageDropdownListData(){
    this.userService.getLanguageDropdownList().subscribe(response =>{
      this.languageList = response;
       //console.log('-->>',response);
    });
  }



  populateData(myData) {
    this.storeForm.patchValue(myData);
    this.storeForm.get('languageId').setValue(myData.language.id);
  }

  onSubmit(): void {

    this.storeForm.value.inBusinessSince = this.datePipe.transform(this.storeForm.value.inBusinessSince, "yyyy-MM-dd");
      let obj = {
        "storeName": this.storeForm.value.storeName,
        "storeCode": this.storeForm.value.storeCode,
        "inBusinessSince": this.storeForm.value.inBusinessSince ,
        "sizeUnitCode": this.storeForm.value.sizeUnitCode,
        "storeEmail": this.storeForm.value.storeEmail,
        "storeAddress": this.storeForm.value.storeAddress,
        "useCache": this.storeForm.value.useCache,
        "weightUnitCode": this.storeForm.value.weightUnitCode,
        "storePhone": this.storeForm.value.storePhone,
        "language": {
            "id":  this.storeForm.get('languageId').value,
            "title": "",
            "code": "",
            "sortOrder": 1
        }
    }

    if(this.actionMode == OBJ_EDIT){

      this.storeService.storeUpdate(this.storeId,obj).subscribe(res =>{
        //console.log(res);
        this.populateData(res);
        this.utilService.showSnackBarMessage('Store Update Successfully',this.utilService.TYPE_MESSAGE.SUCCESS_TYPE);
      },error => {
        this.utilService.showSnackBarMessage(error.message,this.utilService.TYPE_MESSAGE.ERROR_TYPE);
      });

    }else{
    this.storeService.storeCreate(obj).subscribe(res=>{
        this.utilService.showSnackBarMessage('Create Store Successfully',this.utilService.TYPE_MESSAGE.SUCCESS_TYPE);
        this.router.navigate(['/pages/store-management/store']);
    },error => {
      this.utilService.showSnackBarMessage(error.message,this.utilService.TYPE_MESSAGE.ERROR_TYPE);
    });
    }
  }

goToList(){
    this.router.navigate(['/pages/store-management/store']);
}

get merchantFirstNameBan() {
  return this.storeForm.get('merchantFirstNameBan');
}
get merchantFirstNameEng() {
  return this.storeForm.get('merchantFirstNameEng');
}

get storeCode() {
  return this.storeForm.get('storeCode');
}

get storeEmail(){
  return this.storeForm.get('storeEmail');
}

get storePhone(){
  return this.storeForm.get('storePhone');
}

get storeAddress(){
  return this.storeForm.get('storeAddress');
}

}
