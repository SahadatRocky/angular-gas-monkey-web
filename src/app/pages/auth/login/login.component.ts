import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { LocalStorageService } from "../../shared/service/local-storage-service";
import { StorageService } from "../../shared/service/storage.service";
import { AuthService } from "../service/auth.service";
import { AngularFireMessaging } from "@angular/fire/messaging";
import { UtilService } from "../../shared/service/util.service";

@Component({
  selector: "app-dashboard",
  templateUrl: "login.component.html",
})
export class LoginComponent implements OnInit {
  public notificationCount: number = 0;
  notifications = [];
  message: any;
  formGroup: FormGroup;
  firbaseToken: string;
  firebaseToken1: string;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private localStorageService: LocalStorageService,
    public messaging: AngularFireMessaging,
    private utilService: UtilService
  ) {}

  ngOnInit(): void {
    this.createFormGroup();
    this.requestPermission();
  }

  getfirebaseToken() {
    this.firbaseToken = this.localStorageService.getFirebaseToken();
    //console.log('///////////////////////',firbaseToken);
  }

  requestPermission() {
    this.messaging.requestToken.subscribe(
      (token) => {
        console.log("Permission granted!");
        console.log(token);
        this.localStorageService.setFirebaseToken(token);
        this.listen();
      },
      (error) => {
        //alert("Please allow notifications");
        // console.error(error)
        this.utilService.showAlertSnackBarMessage(
          "Please allow notifications.",
          this.utilService.TYPE_MESSAGE.SUCCESS_TYPE
        );
      }
    );
  }

  listen() {
    this.messaging.messages.subscribe((message) => {
      console.log(message);
      if (message["notification"]["body"]) {
        this.notificationCount++;
      }
      this.notifications.unshift(message["notification"]["body"]);
    });
  }

  createFormGroup() {
    this.formGroup = this.fb.group({
      username: ["", [Validators.required]],
      password: ["", Validators.required],
    });
  }

  userLogin() {
    const firbaseToken = this.localStorageService.getFirebaseToken();
    if (!firbaseToken) {
      this.utilService.showAlertSnackBarMessage(
        "Please allow notifications.",
        this.utilService.TYPE_MESSAGE.SUCCESS_TYPE
      );
      return;
    }
    const username = this.formGroup.get("username").value;
    const password = this.formGroup.get("password").value;
    const loginBody = {
      username: username,
      password: password,
      deviceType: "WEB",
      token: firbaseToken,
    };

    this.authService.login(loginBody).subscribe(
      (response) => {
        if (response) {
          this.localStorageService.setIsAuthenticated(true);
          this.localStorageService.setJWTToken(response.token);
          this.localStorageService.setJWTRefreshToken(response.refreshToken);
          this.authService.setCurrentUserData(response);

          this.router.navigate(["/pages/home"]);
        } else {
          this.showLoginFailed();
        }
      },
      (error) => {
        console.log(error);
        this.showLoginFailed();
      }
    );
  }

  showLoginFailed() {
    this.utilService.showAlertSnackBarMessage(
      "Please allow notifications.",
      this.utilService.TYPE_MESSAGE.SUCCESS_TYPE
    );
  }

  userForgotPassword() {
    this.router.navigate(["/auth/forgot-password"]);
  }

  get username() {
    return this.formGroup.get("username");
  }

  get password() {
    return this.formGroup.get("password");
  }
}
