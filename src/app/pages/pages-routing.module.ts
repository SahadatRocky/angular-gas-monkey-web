import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PagesComponent } from './pages.component';
import { NotFoundComponent } from './shared/components/not-found/not-found.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'home',
      loadChildren:  () => import('./home/home.module').then(m => m.HomeModule)
    },
    {
        path: 'role-management',
        loadChildren: () => import('./role-management/role-management.module').then(m => m.RoleManagementModule)
    },
    {
      path: 'customer-management',
      loadChildren: () => import('./customer-management/customer-management.module').then(m => m.CustomerManagementModule)
    },
    {
      path: 'product-management',
      loadChildren: ()=> import('./product-management/product-management.module').then(e=>e.ProductManagementModule)
    },
    {
      path: 'store-management',
      loadChildren: () => import('./store-management/store-management.module').then(m => m.StoreManagementModule)
    },
    {
      path: 'order-management',
      loadChildren: () => import('./order-management/order-management.module').then(m => m.OrderManagementModule)
    },
    {
      path: 'configuration-management',
      loadChildren: () => import('./configuration-management/configuration-management.module').then(m => m.ConfigurationManagementModule)
    },
    {
      path: 'partner-management',
      loadChildren: () => import('./partner-management/partner-management.module').then(m => m.PartnerManagementModule)
    },
    {
      path: 'dealer-management',
      loadChildren: () => import('./dealer-management/dealer-management.module').then(m => m.DealerManagementModule)
    },
    {
      path: 'customer-support',
      loadChildren: () => import('./customer-support/customer-support.module').then(m => m.CustomerSupportModule)
    },
    {
      path: '',
      redirectTo: 'home',
      pathMatch: 'full'
    },
    {
      path: '**',
      component: NotFoundComponent
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
