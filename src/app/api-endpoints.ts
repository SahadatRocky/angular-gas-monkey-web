export class ApiEndpoints {
  ROOT_URL = 'https://testapi-k8s.oss.net.bd/gas-monkey'; //UAT
  // ROOT_URL = 'http://192.168.30.83:8801'; // for DEV SERVER
  PRODUCT_IMAGE_UPLOAD = 'http://localhost:8080';

  //End-Point

  LOGIN_URL = `${this.ROOT_URL}/api/v1/auth/login`;
  REFRESH_TOKEN = `${this.ROOT_URL}/api/v1/auth/refresh-token`;
  GROUP_DROPDOWN_LIST = `${this.ROOT_URL}/api/v1/admin/group-info/dropdown-list`;
  LANGUAGE_DROPDOWN_LIST = `${this.ROOT_URL}/api/v1/admin/language/dropdown-list`;
  USER_REGISTRATION = `${this.ROOT_URL}/api/v1/admin/user-registration`;
  USERS_REQUEST_API = `${this.ROOT_URL}/api/v1/admin/users`;
  DESIGNATION_DROPDOWN_LIST = `${this.ROOT_URL}/api/v1/admin/designation/dropdown-list`;
  DEPARTMENT_DROPDOWN_LIST = `${this.ROOT_URL}/api/v1/admin/department/dropdown-list`;

  STORE_REQUEST_API =    `${this.ROOT_URL}/api/v1/admin/stores`;
  BRAND_BASE_URL = `${this.ROOT_URL}/api/v1/admin/brand`;
  PRODUCT_BASE_URL = `${this.ROOT_URL}/api/v1/admin/product`;
  PRODUCT_SIZE_BASE_URL = `${this.ROOT_URL}/api/v1/admin/product-size`;
  PRODUCT_VALVE_SIZE_BASE_URL = `${this.ROOT_URL}/api/v1/admin/product-valve-size`;
  UPLOAD_PRODUCT_IMAGE_URL = `${this.ROOT_URL}/api/v1/admin/upload/product-image`;
  CREATE_PRODUCT_IMAGE_URL = `${this.ROOT_URL}/api/v1/admin/product-image`;
  FIND_PRODUCT_IMAGE_URL = `${this.ROOT_URL}/apapi/v1/admin/product`;
  CUSTOMER_LIST_URL = `${this.ROOT_URL}/api/v1/admin/customer`;
  CUSTOMER_TYPE_DROPDOWN_LIST =`${this.ROOT_URL}/api/v1/admin/customer-type/dropdown-list`;
  CUSTOMER_DISTRICT_DROPDOWN_LIST =`${this.ROOT_URL}/api/v1/admin/district`;
  CUSTOMER_THANA_DROPDOWN_LIST =`${this.ROOT_URL}/api/v1/admin/thana`;
  CUSTOMER_CLUSTER_DROPDOWN_LIST =`${this.ROOT_URL}/api/v1/admin/cluster`;
  CREATE_BRAND_IMAGE_URL = `${this.ROOT_URL}/api/v1/admin/brand-image`;
  CONFIG_DATA_URL = `${this.ROOT_URL}/api/v1/admin/config-data`;
  ORDER_LIST_URL = `${this.ROOT_URL}/api/v1/admin/order-list`;
  ORDER_GET_BY_ID_URL = `${this.ROOT_URL}/api/v1/admin/get-order-by-id`;
  RESET_PASSWORD_URL =  `${this.ROOT_URL}/api/v1/admin/reset-password`;

  SLIDER_IMAGE_LIST_URL = `${this.ROOT_URL}/api/v1/admin/slider-image`;
  COUPON_LIST_URL = `${this.ROOT_URL}/api/v1/admin/coupon`;
  DISCOUNT_DATA_URL = `${this.ROOT_URL}/api/v1/admin/service-discount`;
  SEND_NOTIFICATION_URL = `${this.ROOT_URL}/api/v1/admin/send-notification`;
  ATTACHMENT_URL = `${this.ROOT_URL}/api/v1/admin/attachment-type`;
  ATTACHMENT_TYPE_DROPDOWN_LIST =`${this.ROOT_URL}/api/v1/admin/attachment-type/dropdown-list`

  PARTNER_URL = `${this.ROOT_URL}/api/v1/admin/partner`;
  DEALER_URL = `${this.ROOT_URL}/api/v1/admin/dealer`;

  PARTNER_DROPDOWN_LIST_BY_AREA_ID =`${this.ROOT_URL}/api/v1/admin/partner/dropdown-list`;
  ORDER_FORWARD_BASE_URL = `${this.ROOT_URL}/api/v1/admin/forward-order`;
  MOBILE_BANKING_URL = `${this.ROOT_URL}/api/v1/admin/mobile-banking-info`;
  MOBILE_BANKING_DROPDOWN_URL = `${this.ROOT_URL}/api/v1/admin/mobile-banking-info/dropdown-list`;
  ISSUE_TYPE_URL = `${this.ROOT_URL}/api/v1/admin/issue-type`;
  CANCEL_REASON_URL = `${this.ROOT_URL}/api/v1/admin/cancel-reason`;
  TEMPLATE_URL = `${this.ROOT_URL}/api/v1/admin/template`;

  PARTNER_APPROVED_BASE_URL = `${this.ROOT_URL}/api/v1/admin/partner/approved`;
  Dealer_APPROVED_BASE_URL = `${this.ROOT_URL}/api/v1/admin/dealer/approved`;
  FILE_UPLOAD_URL = `${this.ROOT_URL}/api/v1/file/upload`;

 //
 DROPDOWN_LIST_API_URL="/dropdown-list";
 NOTIFICATION_LIST_URL = `${this.ROOT_URL}/api/v1/admin/notification-list`;
 SUPPORT_TICKET_LIST_URL = `${this.ROOT_URL}/api/v1/admin/support-ticket`;
 PUSH_NOTIFICATION_URL = `${this.ROOT_URL}/api/v1/admin/push-notification`;


 API_ROUTE_DIRECTION_URL = "https://api.openrouteservice.org/v2/directions/driving-car";
 ORDER_TRACE_URL = `${this.ROOT_URL}/api/v1/admin/order-trace`;

 DEPARTMENT_URL = `${this.ROOT_URL}/api/v1/admin/department`;
 DESIGNATION_URL = `${this.ROOT_URL}/api/v1/admin/designation`;
 PARTNER_PAYMENT_URL = `${this.ROOT_URL}/api/v1/admin/partner/payment`;
 TRANSECTION_PAYMENT_URL = `${this.ROOT_URL}/api/v1/admin/transaction/payment`;
 APP_MENU_URL = `${this.ROOT_URL}/api/v1/admin/app-menu`;
 ASSIGN_ROLE_MENU_URL = `${this.ROOT_URL}/api/v1/admin/assign-role-menu`;
}
