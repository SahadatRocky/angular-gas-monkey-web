import {Injectable} from '@angular/core';
import {MasterButtonField} from '../master-model/master-button-field';

@Injectable(
  {
    providedIn: 'root',
  }
)
export class MasterButtonService {
  buttonFields: MasterButtonField[];

  constructor() {
  }

  getButtonField() {
    return this.buttonFields;
  }
}
