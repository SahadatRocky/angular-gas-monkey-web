import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { TextData } from '../../../../pages/shared/model/common/text-data';
import { ListDefinition } from '../master-model/list-definition';

@Injectable(
  {
    providedIn: 'root',
  }
)
export class MasterListService {
  fieldInfo: ListDefinition<any>;
  constructor(){}

  getFields(listServiceData: ListDefinition<any>): TextData[] {
    return listServiceData.fields.sort((a, b) => a.order - b.order);
  }
}
