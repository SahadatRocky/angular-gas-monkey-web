
export class MasterButtonField {
  icon: string;
  visible: boolean;
  controlType: string;
  label: string;

  constructor(options: Partial<MasterButtonField> = {}) {
    this.icon = options.icon;
    this.visible = options.hasOwnProperty('visible') === false ? true : options.visible;
    this.controlType = options.controlType;
    this.label = options.label || '';
  }
}
