import { TextData } from '../../../../pages/shared/model/common/text-data';

export class ListDefinition<T> {
  fields: TextData[];
  colWidth: number;
  listTitleEn?: string;
  listTitleBn?: string;
  isActionColumnVisible?: boolean;

  constructor(options: Partial<ListDefinition<any>> = {}) {
    this.fields = options.fields || [];
    this.colWidth = options.colWidth;
    this.listTitleEn = options.listTitleEn;
    this.listTitleBn = options.listTitleBn;
    this.isActionColumnVisible = options.isActionColumnVisible || true;
  }
}
