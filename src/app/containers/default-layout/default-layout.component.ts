import {Component, OnInit} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { version } from '../../constants/gas-monkey-constants';
import { AuthService } from '../../pages/auth/service/auth.service';
import { ChangePasswordComponent } from '../../pages/role-management/change-password/change-password.component';
import { LocalStorageService } from '../../pages/shared/service/local-storage-service';
import { NavData, navItems } from '../../_nav';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { notification_status } from '../../constants/notification-status';
import { UtilService } from '../../pages/shared/service/util.service';
import { NotificationService } from '../core/notification-service/notification.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent implements OnInit{
  version = version;
  public sidebarMinimized = false;
  public navItems = navItems;
  userName:any;
  jwttoken: string;
  message:any;
  public notificationCount: number = 0;
  notifications = [];

  constructor(private authService: AuthService,
    public dialog: MatDialog,
    private localStorageService: LocalStorageService,
    public messaging: AngularFireMessaging,
    private utilService: UtilService,
    private notificationService: NotificationService,
    private router: Router){
      this.setNavData();

  }

  ngOnInit(): void {
    this.requestPermission();
  }

  requestPermission() {
    this.messaging.requestToken.subscribe(token => {
      console.log('Permission granted!')
      console.log(token)
      this.listen()
    }
      , error => {
        //alert("Please allow notifications");
       // console.error(error)
        this.utilService.showAlertSnackBarMessage(
          "Please allow notifications.",
          this.utilService.TYPE_MESSAGE.SUCCESS_TYPE
        );
      })
  }

  listen() {
    this.messaging.messages.subscribe(message => {
      console.log(message)
      if (message['notification']['body']) {
        this.notificationCount++;
      }
      this.notifications.unshift(message['notification']['body']);
    });
  }

  checkAuthorities(navigationItems: any, rolesArray: string[]): NavData[] {
    return navigationItems.filter(navItem => {
      let canSeeNavMenu = true;
      console.log('-----check-Authorities--',navItem.attributes);
      if (navItem.attributes) {
        if (navItem.attributes.authorities) {
          canSeeNavMenu = navItem.attributes.authorities.some(auth => rolesArray.includes(auth));
        }
        if (navItem.attributes.restricted) {
          canSeeNavMenu = canSeeNavMenu && navItem.attributes.restricted.every(rest => !rolesArray.includes(rest));
        }
      }
      if (canSeeNavMenu && navItem.children) {
        navItem.children = this.checkAuthorities(navItem.children, rolesArray);
      }
      return canSeeNavMenu;
    });
  }

  setNavData() {
    this.navItems = [];
    this.jwttoken = this.localStorageService.getJWTToken();
    let jwtData = this.jwttoken.split('.')[1];
    let decodedJwtJsonData = window.atob(jwtData);
    let decodedJwtData = JSON.parse(decodedJwtJsonData);
    console.log('jwt-data--',decodedJwtData);
    this.userName = decodedJwtData['username'];
    this.localStorageService.setUserName(decodedJwtData['username']);
    this.localStorageService.setAuthorityName(decodedJwtData['authorities']);
    this.localStorageService.setRoleName(decodedJwtData['accessMenus']);
    console.log('accessMenu--',decodedJwtData['accessMenus']);

      if (decodedJwtData['authorities'] == 'SUPER_ADMIN') {
        this.navItems = this.checkAuthorities( navItems, [decodedJwtData['authorities']]);
      }else if (decodedJwtData['accessMenus']) {
        this.navItems = this.checkAuthorities( navItems, decodedJwtData['accessMenus']);
      }
  }

  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }

  dashboard(){
    this.router.navigate(['/pages/home']);
  }

  ResetPassword(){
    const dialogRef = this.dialog.open(ChangePasswordComponent, {
      width: '900px',
      height: '500px',
      data: {
         title:'reset-password'
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  logout(){
    this.dialog.closeAll();
    this.authService.logout();

  }

  clickNtfBar() {
    //this.clicked = true;
    this.getNotifications();
    this.notificationCount = 0;
  }

  goToUrl(ntf: any) {
    // if (ntf.url) {
    //   window.location.replace(ntf.url);
    // }
  }

  getNotifications(){
    console.log("get notification list");
    this.notificationService.getNotificationListData().subscribe(result => {
      console.log('Response***',result);
      if (result != null) {
        this.notifications = result['data'];
      }
    });
  }

  setPointer(ntf: any): string {
    return ntf.url ? "pointer" : "unset";
  }

  setBackground(ntf: any): string {
    return ntf.status !== notification_status.SEEN
      ? "rgb(200, 255, 188)"
      : "white";
  }

}
